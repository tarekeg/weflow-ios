//
//  SecondOnboardingDescriptionViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 09/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit

class DescriptionViewController: UIViewController {
    
    @IBOutlet weak var toLoginButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backButton: UIButton!
    
    var backButtonIsHidden = true
    
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont(name: "Avenir-Heavy", size: 15)!,
        .foregroundColor: K.Color.grayThirtySix,
         .underlineStyle: NSUnderlineStyle.single.rawValue
     ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDesign()
        backButtonIsHidden ? (backButton.isHidden = true) : (backButton.isHidden = false)
    }
    
    func setDesign () {
              
        let attributeString = NSMutableAttributedString(
                string: NSLocalizedString("login", comment: ""),
                attributes: yourAttributes
             )
             toLoginButton.setAttributedTitle(attributeString, for: .normal)
    }

    @IBAction func goBackToPreviousView(_ sender: Any) {
        self.dismiss(animated: true)
    }
    

}
