//
//  InAppLoginViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 10/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Auth0
import SwiftKeychainWrapper
import Firebase
import Alamofire
import SwiftyJSON
import Sentry

class InAppLoginViewController: UIViewController, UIGestureRecognizerDelegate {
    
    
    
    

    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var mailTextField: CustomTextField!
    @IBOutlet weak var toSignInButton: UIButton!
    @IBOutlet weak var resetPasswordButton: UIButton!
    @IBOutlet weak var logInButton: LoadingButton!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    
    
    private let locationManager = CLLocationManager()
    let RF = ReusableFunctions()
    let alertService = AlertService()
    
    
    var checkBoxChecked = false
    let checkedImage = UIImage(named: "ic_check_box")
    let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")
    var iconClick = false
    let imageIcon = UIImageView()
    let eyeColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.00)
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont(name: "Avenir-Heavy", size: 15)!,
        .foregroundColor: K.Color.grayThirtySix,
         .underlineStyle: NSUnderlineStyle.single.rawValue
     ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkFCMToken()
        setDesign()
        checkSavedCredentials()
        alertService.resetDelegate = self
        mailTextField.delegate = self
        passwordTextField.delegate = self
        mailTextField.addTarget(self, action: #selector(InAppLoginViewController.textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(InAppLoginViewController.textFieldDidChange(_:)), for: .editingChanged)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    func checkFCMToken () {
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching FCM registration token: \(error)")
            } else if let token = token {
                print("FCM registration token: \(token)")
                UserDefaults.standard.set(token, forKey: "FCMToken")
            }
        }
    }
    @IBAction func loginButtonTapped(_ sender: Any) {
        logInButton.isEnabled = false
        logInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        logInButton.showLoading()
        auth0Login(mailTextField.text!, passwordTextField.text!)
    }
    @IBAction func saveCredentialsTapped(_ sender: Any) {
        checkBoxChecked = !checkBoxChecked
        if checkBoxChecked {
            checkBoxImageView.image = checkedImage
        } else {
            checkBoxImageView.image = uncheckedImage
        }
    }
    func checkSavedCredentials () {
        if let retrievedMailString = KeychainWrapper.standard.string(forKey: "mailKeychain") {
            mailTextField.text = retrievedMailString
            checkBoxChecked = true
            checkBoxImageView.image = checkedImage
            logInButton.isEnabled = true
            logInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        }
        
    }
    
    @IBAction func resetPasswordTapped(_ sender: Any) {
        let alert = alertService.alertResetPassword()
        self.present(alert, animated: true)
    }
    
    func auth0Login (_ mail : String, _ password : String) {
        
        Auth0
            .authentication()
            .login(
                usernameOrEmail: mail,
                password: password,
                realm: "Username-Password-Authentication",
                audience : "https://preprod.ireby.pro/api",
                scope: "openid profile email offline_access")
            .start { result in
                switch result {
                case .success(let credentials):
                    self.syncTokenProfile(token: credentials.idToken!)
                    UserDefaults.standard.set(credentials.idToken!, forKey: "Token")
                    UserDefaults.standard.set(credentials.accessToken!, forKey: "ireby_access_token")
                    UserDefaults.standard.set(credentials.refreshToken!, forKey: "ireby_refresh_token")
                    if(!SessionManager.shared.store(credentials: credentials)) {
                        print("Failed to store credentials")
                    } else {
                        DispatchQueue.main.async {
                            if self.checkBoxChecked {
                                KeychainWrapper.standard.set(self.mailTextField.text!, forKey: "mailKeychain")
                            } else {
                                KeychainWrapper.standard.removeObject(forKey: "mailKeychain")
                            }
                        }
                        
                        SessionManager.shared.retrieveProfile { error in
                            DispatchQueue.main.async { [self] in
                                guard error == nil else {
                                    print("Failed to retrieve profile: \(String(describing: error))")
                                    return
                                }
                                self.logInButton.hideLoading()
                                if isLocationEnabled() && isInitEnabled() {
                                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "progressDownloadView")
                                    UIApplication.shared.windows.first?.rootViewController = vc
                                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                                    self.present(vc, animated: true)
                                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                                } else if !isLocationEnabled() {
                                    let locationVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "locationPermissionViewController")
                                    self.present(locationVC, animated: true)
                                } else if !isInitEnabled() {
                                    let motionVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "motionActivityPermissionViewController")
                                    self.present(motionVC, animated: true)
                                }
                                    
                                
                                
                            }
                        }
                    }
                case .failure(let error as Auth0.AuthenticationError):
                    SentrySDK.capture(error: error)
                    print("Error code: \(error.code), description:\(error.description), info:\(error.info), statusCode:\(error.statusCode)")
                    DispatchQueue.main.async {
                        if error.statusCode == 403 {
                            self.showPopup(title: NSLocalizedString("error1-popup-sync-title", comment: ""), body: NSLocalizedString("error1-popup-sync-body", comment: ""), buttonTitle: NSLocalizedString("error1-popup-sync-button", comment: ""))
                        } else {
                            self.showPopup(title: NSLocalizedString("error-popup-login-title", comment: ""), body: NSLocalizedString("error-popup-login-body", comment: ""), buttonTitle: NSLocalizedString("error-popup-login-button", comment: ""))
                        }
                    }
                    print("Failed with \(error)")
                case .failure(_):
                    DispatchQueue.main.async {
                        self.showPopup(title: NSLocalizedString("error-popup-login-title", comment: ""), body: NSLocalizedString("error-popup-login-body", comment: ""), buttonTitle: NSLocalizedString("error-popup-login-button", comment: ""))
                    }
                }
            }
    }
    
    func syncTokenProfile (token : String) {
        let parameters = ["user":token]
        let parameters2 = [
            "update_doc":[
                "device_token":UserDefaults.standard.string(forKey: "FCMToken") ?? "unknow",
                "curr_platform":"ios",
                "curr_sync_interval":1800,
                "source":"Transway"
            ],
            "user":token
        ] as [String : Any]
        AF.request(K.Path.baseURL + K.Path.createProfile, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON : \(json)")
                AF.request(K.Path.baseURL + K.Path.updateProfile, method: .post, parameters: parameters2, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        print("JSON : \(json)")
                    case .failure(let error):
                        SentrySDK.capture(error: error)
                        print(error)
                    }
                }
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
    }
    
    func isLocationEnabled () -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            if #available(iOS 14.0, *) {
                switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return false
                }
            } else {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return true
                }
            }
        } else {
            return false
        }

    }
    func isInitEnabled() -> Bool {
        if CMMotionActivityManager.authorizationStatus().rawValue != 3 {
            return false
        } else {
            return true
        }
    }
    

    func showPopup(title : String, body : String, buttonTitle : String) {
           
        let alert = alertService.alert(title: title, body: body, buttonTitle: buttonTitle) { [self] in
            logInButton.isEnabled = true
            logInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
            logInButton.hideLoading()
            
        }
        self.present(alert, animated: true)
    }
    
    func setDesign() {
        logInButton.isEnabled = false
        logInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        imageIcon.image = UIImage(named: "visibility_off")?.imageWithColor(color1: eyeColor)
        let contentView = UIView()
        contentView.addSubview(imageIcon)
        contentView.frame = CGRect(x: 0, y: 0, width: UIImage(named: "visibility_off")!.size.width + 10, height: UIImage(named: "visibility_off")!.size.height + 10 )
        imageIcon.frame = CGRect(x: -10, y: 5, width: UIImage(named: "visibility_off")!.size.width, height: UIImage(named: "visibility_off")!.size.height)
        
        passwordTextField.rightView = contentView
        passwordTextField.rightViewMode = .always
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)
        let attributeResetString = NSMutableAttributedString(
            string: NSLocalizedString("reset-password", comment: ""),
            attributes: yourAttributes
        )
        let attributeString = NSMutableAttributedString(
            string: NSLocalizedString("signIn", comment: ""),
            attributes: yourAttributes
        )
        toSignInButton.setAttributedTitle(attributeString, for: .normal)
        resetPasswordButton.setAttributedTitle(attributeResetString, for: .normal)
        
    }

    @IBAction func goBackToPreviousView(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
        self.dismiss(animated: true)
    }
    
    @objc func imageTapped(tapGestureRecognizer:UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if iconClick {
            iconClick = false
            tappedImage.image = UIImage(named: "visibility")?.imageWithColor(color1: eyeColor)
            passwordTextField.isSecureTextEntry = false
        } else {
            iconClick = true
            tappedImage.image = UIImage(named: "visibility_off")?.imageWithColor(color1: eyeColor)
            passwordTextField.isSecureTextEntry = true
        }
    }
  
    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Avenir-Roman", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}


extension InAppLoginViewController : UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == mailTextField {
            checkBoxChecked = false
            checkBoxImageView.image = uncheckedImage
        }
        if RF.isValidEmail(mailTextField.text!) == true && passwordTextField.text?.isEmpty == false {
            logInButton.isEnabled = true
            logInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        } else {
            logInButton.isEnabled = false
            logInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }

    }
}



extension InAppLoginViewController : AlertResetDelegate {
    func sendResult(bool: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            if bool {
                let alert = alertService.alertResetPasswordSuccess()
                self.present(alert, animated: true)
            } else {
                self.showToast(message: NSLocalizedString("error-toast", comment: ""))
            }
        }
    }
}
