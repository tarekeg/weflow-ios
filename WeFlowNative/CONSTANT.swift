//
//  CONSTANT.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 30/07/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import Foundation

struct K {
    struct Path {
        static let baseURL = Environment.emissionUrl
        static let baseURLWeb = Environment.irebyWebUrl
        static let createProfile = "/profile/create"
        static let updateProfile = "/profile/update"
        static let getByDate = "/timeline/getTripsForTenDays/"
        static let cleverApi  = Environment.wefloApi
        static let supportWeflo = "weflow@transway.fr"
        static let preprodHeroku = "https://weflo.herokuapp.com/"
        static let wefloConfidentialité = "https://www.weflo.fr/politique-de-confidentialité"
        static let wefloWix = "https://www.weflo.fr/"
    }
    struct Color {
        static let primaryColor =  UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1.00)
        static let secondaryColor = UIColor(red: 0.00, green: 0.63, blue: 0.60, alpha: 1.00)
        static let chacoralGrey = UIColor(red: 0.24, green: 0.25, blue: 0.26, alpha: 1.00)
        static let disabledColor = UIColor(red: 0.68, green: 0.68, blue: 0.68, alpha: 1.00)
        static let graySeven = UIColor(red: 0.44, green: 0.44, blue: 0.44, alpha: 1.00)
        static let grayThirtySix = UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00)
        static let CAR = UIColor(red: 0.63, green: 0.22, blue: 0.06, alpha: 1.00)
        static let BICYCLING = UIColor(red: 0.00, green: 0.63, blue: 0.60, alpha: 1.00)
        static let WALKING = UIColor.black
        static let TRAM = UIColor(red: 1.00, green: 0.66, blue: 0.55, alpha: 1.00)
        static let SUBWAY = UIColor(red: 1.00, green: 0.84, blue: 0.78, alpha: 1.00)
        static let TRAIN = UIColor(red: 0.53, green: 0.76, blue: 0.71, alpha: 1.00)
        static let AIR = UIColor(red: 0.73, green: 0.53, blue: 0.45, alpha: 1.00)
        static let BUS = UIColor(red: 0.28, green: 0.43, blue: 0.40, alpha: 1.00)
        static let CARPOOLING = UIColor(red: 0.42, green: 0.60, blue: 0.00, alpha: 1.00)
        static let MOTO = UIColor(red: 0.00, green: 0.27, blue: 0.21, alpha: 1.00)
        static let ESCOOTER = UIColor(red: 0.21, green: 0.30, blue: 0.00, alpha: 1.00)
        static let ERROR = UIColor(red: 0.76, green: 0.00, blue: 0.00, alpha: 1.00)
        static let OTHER = UIColor(red: 0.71, green: 0.45, blue: 0.36, alpha: 1.00)
    }
    struct Constant {
        static let averageWeight = 68.0
        static let irebyBearer = "cAcg-4vNqe8@743C@NQ9p$Jg3k$B*2fmvyNAYZG?Q%85*7&?Tv+J=RhG+5QjpU^Nh9^xzX"
    }
   
}
