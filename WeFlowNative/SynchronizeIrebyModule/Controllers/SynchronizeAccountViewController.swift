//
//  SynchronizeAccountViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 11/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Alamofire
import AudioToolbox
import SwiftKeychainWrapper
import Sentry

class SynchronizeAccountViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var irebyMailTextField: CustomTextField!
    @IBOutlet weak var syncButton: LoadingButton!
    @IBOutlet weak var informativeSyncLabel: UILabel!
    
    private let locationManager = CLLocationManager()
    var params : Parameters?
    let alertService = AlertService()
    let RF = ReusableFunctions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        syncButton.isEnabled = false
        syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        setUpTextFields()
        addTargetTotextFields()
        syncButton.setTitle(NSLocalizedString("synchronize", comment: ""), for: .normal)
        
    }
    
    
    @IBAction func goBackTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func syncButtonTapped(_ sender: Any) {
        if(syncButton.title(for: .normal) == NSLocalizedString("synchronize", comment: "")) {
            syncWithIreby(token: UserDefaults.standard.string(forKey: "Token")!, mail: irebyMailTextField.text!)
        } else if syncButton.title(for: .normal) == NSLocalizedString("lets_go", comment: "") {
            if isLocationEnabled() && isInitEnabled() {
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "tabBarController")
                UIApplication.shared.windows.first?.rootViewController = vc
                UIApplication.shared.windows.first?.makeKeyAndVisible()
                self.present(vc, animated: true)
                self.presentingViewController?.dismiss(animated: true, completion: nil)
            } else if !isLocationEnabled() {
                let locationVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "locationPermissionViewController")
                self.present(locationVC, animated: true)
            } else if !isInitEnabled() {
                let motionVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "motionActivityPermissionViewController")
                self.present(motionVC, animated: true)
            }
            
        }
        
    }
    func isLocationEnabled () -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            if #available(iOS 14.0, *) {
                switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return false
                }
            } else {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return true
                }
            }
        } else {
            return false
        }
        
    }
    func isInitEnabled() -> Bool {
        if CMMotionActivityManager.authorizationStatus().rawValue != 3 {
            return false
        } else {
            return true
        }
    }
    func syncWithIreby (token : String, mail : String) {
        syncButton.showLoading()
        syncButton.isEnabled = false
        syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        params = [
            "email" : irebyMailTextField.text
        ]
        let headers: HTTPHeaders = [
            "token" : UserDefaults.standard.string(forKey: "ireby_access_token")!   ,
            "authorization": "Bearer " + K.Constant.irebyBearer,
            "content-type": "application/json",
            "Accept": "application/json"
        ]
        
        AF.request(K.Path.baseURLWeb + "/api/users/weflo/sync", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseString { [self]
            response in
            switch response.result {
            case .success(let value):
                print(response.response?.statusCode)
                if response.response?.statusCode == 200 {
                    print(value)
                    KeychainWrapper.standard.removeObject(forKey: "irebymail")
                    KeychainWrapper.standard.removeObject(forKey: "irebyfirstname")
                    KeychainWrapper.standard.removeObject(forKey: "irebylastname")
                    KeychainWrapper.standard.removeObject(forKey: "irebypassword")
                    UserDefaults.standard.removeObject(forKey: "hasStartedSignIn")
                    let alert = alertService.alert(title: NSLocalizedString("sync-mail-popup-title", comment: ""), body: NSLocalizedString("sync-mail-popup-body", comment: ""), buttonTitle: NSLocalizedString("sync-mail-popup-button", comment: "")) {
                        syncButton.hideLoading()
                        syncButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                        syncButton.isEnabled = true
                        syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                    }
                    self.tabBarController?.present(alert, animated: true)
                    
                } else if response.response?.statusCode == 409 {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("popup-already-sync-title", comment: ""), body: NSLocalizedString("popup-already-sync-body", comment: ""), ButtonTitle: NSLocalizedString("popup-already-sync-button", comment: ""))
                    syncButton.hideLoading()
                    syncButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                    syncButton.isEnabled = true
                    syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                } else if response.response?.statusCode == 404 {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("error1-mail-popup-title", comment: ""), body: NSLocalizedString("error1-mail-popup-body", comment: ""), ButtonTitle: NSLocalizedString("error1-mail-popup-button", comment: ""))
                    syncButton.hideLoading()
                    syncButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                    syncButton.isEnabled = true
                    syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                }  else if response.response?.statusCode == 422 {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("error-mail-popup-title", comment: ""), body: NSLocalizedString("error-mail-popup-body", comment: ""), ButtonTitle: NSLocalizedString("error-mail-popup-button", comment: ""))
                    syncButton.hideLoading()
                    syncButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                    syncButton.isEnabled = true
                    syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                } else {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("error-popup-sync-title", comment: ""), body: NSLocalizedString("error-popup-sync-body", comment: ""), ButtonTitle: NSLocalizedString("error-popup-sync-button", comment: ""))
                    syncButton.hideLoading()
                    syncButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                    syncButton.isEnabled = true
                    syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                }
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
                syncButton.hideLoading()
                syncButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                syncButton.isEnabled = true
                syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
            }
            
        }
    }
    func resetTextFields () {
        irebyMailTextField.text = ""
        syncButton.hideLoading()
        syncButton.isEnabled = true
        syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
    }
    func showAlert(title : String, body : String, ButtonTitle : String) {
        let alert = alertService.alert(title: title, body: body, buttonTitle: ButtonTitle) {
            
        }
        self.present(alert, animated: true)
    }
    
    func setUpTextFields () {
        irebyMailTextField.delegate = self
        irebyMailTextField.delegate = self
    }
    
    
    func addTargetTotextFields () {
        irebyMailTextField.addTarget(self, action: #selector(SynchronizeAccountViewController.textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        checkEntries()
    }
    

    func checkEntries () {
        if RF.isValidEmail(irebyMailTextField.text!) == true {
            syncButton.isEnabled = true
            syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        } else {
            syncButton.isEnabled = false
            syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
    }
    
}






