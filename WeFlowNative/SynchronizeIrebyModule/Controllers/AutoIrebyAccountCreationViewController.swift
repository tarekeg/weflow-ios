//
//  AutoIrebyAccountCreationViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 11/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Alamofire
import AudioToolbox
import SVGKit
import Kingfisher
import SwiftKeychainWrapper
import Sentry

class AutoIrebyAccountCreationViewController: UIViewController,AlertCompanyDelegate {

    @IBOutlet weak var irebyTcuTextView: UITextView!
    @IBOutlet weak var irebySignInButton: LoadingButton!
    @IBOutlet weak var tcuCheckBoxImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tcuView: UIView!
    @IBOutlet weak var progFidView: UIView!
    @IBOutlet weak var ambassadeurView: UIView!
    @IBOutlet weak var ambassadeurCheckboxImageView: UIImageView!
    @IBOutlet weak var progFidCheckBoxImageView: UIImageView!
    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var successSyncWithCompanyLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var ambassadeurTextView: UITextView!
    @IBOutlet weak var progFidTextView: UITextView!
    
    private let locationManager = CLLocationManager()
    let checkedImage = UIImage(named: "ic_check_box")
    let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")
    var tcuIsConfirmed = false
    var ambassadeurConfirmed = false
    var anotherProgFidConfirmed = false
    var companycode : String?
    
    let hyperlinks = [NSLocalizedString("tcu",comment : "") : K.Path.baseURLWeb + "/cgu",NSLocalizedString("experience-rules",comment : ""):K.Path.baseURLWeb + "/reglement-jeux"]
    
    var params : Parameters?
    let alertService = AlertService()
    
    var firstName : String?
    var lastName : String?
    var password : String?
    var mail : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setParamsForIreby()
        toggleViews(syncSuccessIsshown: false)
        alertService.companyDelegate = self
        ambassadeurTextView.text = NSLocalizedString("textview-description-sync-ambassadeur", comment: "")
        progFidTextView.text = NSLocalizedString("textview-descritpion-sync-another-program", comment: "")
        irebyTcuTextView.text = NSLocalizedString("tcu-ireby-text", comment: "")
        irebySignInButton.isEnabled = false
        irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        irebyTcuTextView.addHyperLinksToText(originalText: irebyTcuTextView.text!, hyperLinks: hyperlinks)
    
    }
    func setParamsForIreby() {
        if let retrievedMailString = KeychainWrapper.standard.string(forKey: "irebymail") {
            mail = retrievedMailString
        }
        if let retrievedFirstNameString = KeychainWrapper.standard.string(forKey: "irebyfirstname") {
            firstName = retrievedFirstNameString
        }
        if let retrievedLastNameString = KeychainWrapper.standard.string(forKey: "irebylastname") {
            lastName = retrievedLastNameString
        }
        if let retrievedPasswordString = KeychainWrapper.standard.string(forKey: "irebypassword") {
            password = retrievedPasswordString
        }
    }
    func toggleViews (syncSuccessIsshown : Bool) {
        if syncSuccessIsshown {
            backButton.isHidden = true
            tcuIsConfirmed = false
            tcuCheckBoxImageView.image = uncheckedImage
            irebySignInButton.isEnabled = false
            irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
            descriptionLabel.isHidden = true
            progFidView.isHidden = true
            ambassadeurView.isHidden = true
            companyImageView.isHidden = false
            companyNameLabel.isHidden = false
            successSyncWithCompanyLabel.isHidden = false
            tcuView.isHidden = false
        } else {
            descriptionLabel.isHidden = false
            progFidView.isHidden = false
            ambassadeurView.isHidden = false
            companyImageView.isHidden = true
            companyNameLabel.isHidden = true
            successSyncWithCompanyLabel.isHidden = true
            tcuView.isHidden = true
        }
    }
    
    func sendCompany(company: String,companyCode : String, img : URL) {
        companycode = companyCode
        DispatchQueue.main.async { [self] in
            let fileExtension = img.pathExtension
            companyNameLabel.text = company
            if fileExtension == "svg" {
                let image = SVGKImage(contentsOf: img)
                companyImageView.image = image?.uiImage
            } else {
                self.companyImageView.kf.indicatorType = .activity
                self.companyImageView.kf.setImage(with: img)
            }
            toggleViews(syncSuccessIsshown: true)
        }
        
    }
    

    @IBAction func goBackTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func irebySignInTapped(_ sender: Any) {
        print("tcuIsConfirmed = ", tcuIsConfirmed, " anotherProgfidIsConfirmed = ",anotherProgFidConfirmed," ambassadeurWefloIsConfirmed = ",ambassadeurConfirmed)
        if tcuIsConfirmed && ambassadeurConfirmed {
            if(irebySignInButton.title(for: .normal) == NSLocalizedString("GO", comment: "")) {
                createUserInIreby(companyCode: "N64C6")
            } else if irebySignInButton.title(for: .normal) == NSLocalizedString("lets_go", comment: "") {
                if isLocationEnabled() && isInitEnabled() {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "tabBarController")
                    UIApplication.shared.windows.first?.rootViewController = vc
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    self.present(vc, animated: true)
                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                } else if !isLocationEnabled() {
                    let locationVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "locationPermissionViewController")
                    self.present(locationVC, animated: true)
                } else if !isInitEnabled() {
                    let motionVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "motionActivityPermissionViewController")
                    self.present(motionVC, animated: true)
                }

                
            }
        } else if anotherProgFidConfirmed && !tcuIsConfirmed  {
            let alert = alertService.alertCompanyCode()
            self.present(alert, animated: true)
        } else if anotherProgFidConfirmed && tcuIsConfirmed {
            if(irebySignInButton.title(for: .normal) == NSLocalizedString("GO", comment: "")) {
                createUserInIreby(companyCode: companycode!)
            } else if irebySignInButton.title(for: .normal) == NSLocalizedString("lets_go", comment: "") {
                if isLocationEnabled() && isInitEnabled() {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "tabBarController")
                    UIApplication.shared.windows.first?.rootViewController = vc
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    self.present(vc, animated: true)
                    self.presentingViewController?.dismiss(animated: true, completion: nil)
                } else if !isLocationEnabled() {
                    let locationVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "locationPermissionViewController")
                    self.present(locationVC, animated: true)
                } else if !isInitEnabled() {
                    let motionVC = UIStoryboard(name: "Permission", bundle: nil).instantiateViewController(withIdentifier: "motionActivityPermissionViewController")
                    self.present(motionVC, animated: true)
                }

            }
        }

    }
    func isLocationEnabled () -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            if #available(iOS 14.0, *) {
                switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return false
                }
            } else {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return true
                }
            }
        } else {
            return false
        }

    }
    func isInitEnabled() -> Bool {
        if CMMotionActivityManager.authorizationStatus().rawValue != 3 {
            return false
        } else {
            return true
        }
    }
    @IBAction func wefloAmbassadeurCheckBoxTapped(_ sender: Any) {
        irebySignInButton.isEnabled = false
        irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        ambassadeurConfirmed = !ambassadeurConfirmed
        if ambassadeurConfirmed {
            
            ambassadeurCheckboxImageView.image = checkedImage
            anotherProgFidConfirmed = false
            progFidCheckBoxImageView.image = uncheckedImage
            tcuView.isHidden = false
        } else {
            
            ambassadeurCheckboxImageView.image = uncheckedImage
            tcuView.isHidden = true
        }
        
    }
    @IBAction func anotherProgFidCheckBoxTapped(_ sender: Any) {
        anotherProgFidConfirmed = !anotherProgFidConfirmed
        if anotherProgFidConfirmed {
            tcuCheckBoxImageView.image = uncheckedImage
            tcuIsConfirmed = false
            tcuView.isHidden = true
            progFidCheckBoxImageView.image = checkedImage
            ambassadeurConfirmed = false
            ambassadeurCheckboxImageView.image = uncheckedImage
            irebySignInButton.isEnabled = true
            irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
            
        } else {
            irebySignInButton.isEnabled = false
            irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
            tcuView.isHidden = true
            progFidCheckBoxImageView.image = uncheckedImage
        }
    }
    @IBAction func tcuTapped(_ sender: Any) {
        if tcuIsConfirmed {
            tcuIsConfirmed = !tcuIsConfirmed
            tcuCheckBoxImageView.image = uncheckedImage
            irebySignInButton.isEnabled = false
            irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        } else {
            tcuIsConfirmed = !tcuIsConfirmed
            tcuCheckBoxImageView.image = checkedImage
            irebySignInButton.isEnabled = true
            irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        }
    }
    
    func createUserInIreby(companyCode : String) {
        let upperCode = companycode?.uppercased()
        irebySignInButton.showLoading()
        irebySignInButton.isEnabled = false
        irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        params = [
    
            "firstname" : firstName,
            "lastname" : lastName,
            "company_code" : upperCode,
            "password" : password
        ]
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + K.Constant.irebyBearer,
            "token" : UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "content-type": "application/json"
        ]
        
        AF.request(K.Path.baseURLWeb + "/api/v2/users/create", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON { [self]
            response in
            switch response.result {
            case .success(let value):
                print(response.response?.statusCode)
                if response.response?.statusCode == 201 {
                    KeychainWrapper.standard.removeObject(forKey: "irebymail")
                    KeychainWrapper.standard.removeObject(forKey: "irebyfirstname")
                    KeychainWrapper.standard.removeObject(forKey: "irebylastname")
                    KeychainWrapper.standard.removeObject(forKey: "irebypassword")
                    UserDefaults.standard.removeObject(forKey: "hasStartedSignIn")
                    print(value)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
                        tcuView.isHidden = true
                        descriptionLabel.font = UIFont(name: "Avenir-Heavy", size: 15.0)
                        descriptionLabel.text = NSLocalizedString("description-created-user", comment: "")
                        successSyncWithCompanyLabel.font = UIFont(name: "Avenir-Heavy", size: 15.0)
                        if companycode == "N64C6" {
                            successSyncWithCompanyLabel.text = NSLocalizedString("description-created-user", comment: "")
                        } else {
                            successSyncWithCompanyLabel.text = NSLocalizedString("description-created-user-anotherprogFid", comment: "")
                        }
                        irebySignInButton.hideLoading()
                        ambassadeurView.isHidden = true
                        progFidView.isHidden = true
                        backButton.isHidden = true
                        irebySignInButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                        irebySignInButton.isEnabled = true
                        irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                    }
                    
                } else if response.response?.statusCode == 409 {
                    print(value)
                    showAlert(title: NSLocalizedString("popup-already-sync-title", comment: ""), body: NSLocalizedString("popup-already-sync-body", comment: ""), ButtonTitle: NSLocalizedString("popup-already-sync-button", comment: ""))
                    irebySignInButton.hideLoading()
                    ambassadeurView.isHidden = true
                    progFidView.isHidden = true
                    tcuView.isHidden = true
                    backButton.isHidden = true
                    irebySignInButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                    irebySignInButton.isEnabled = true
                    irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)

                } else {
                    showAlert(title: NSLocalizedString("error-popup-signIn-title", comment: ""), body: NSLocalizedString("error-popup-signIn-body", comment: ""), ButtonTitle: NSLocalizedString("error-popup-signIn-button", comment: ""))
                    irebySignInButton.hideLoading()
                    irebySignInButton.isEnabled = true
                    irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                }
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
        
        
    }
func showAlert(title : String, body : String, ButtonTitle : String) {
    let alert = alertService.alert(title: title, body: body, buttonTitle: ButtonTitle) {
        
    }
    self.present(alert, animated: true)
}
    
}
