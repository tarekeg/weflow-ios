//
//  PointsSystemViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 09/06/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Kingfisher

class PointsSystemViewController: UIViewController, UIGestureRecognizerDelegate {
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var descriptionModeLabel: UILabel!
    @IBOutlet weak var profileImageButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        profileImageButton.layoutIfNeeded()
        profileImageButton.subviews.first?.contentMode = .scaleAspectFill
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        let resource = ImageResource(downloadURL: URL(string: UserDefaults.standard.string(forKey: "pictureUrl")!)! , cacheKey: "my_avatar")
        profileImageButton.kf.setBackgroundImage(with: resource, for: .normal)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
}
