//
//  HelpViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 22/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import MessageUI
import Kingfisher

class HelpViewController: UIViewController, MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate {
    
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var firstCheckBoxImageView: UIImageView!
    @IBOutlet weak var secondCheckBoxImageView: UIImageView!
    
    
    let checkedImage = UIImage(named: "ic_check_box")
    let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")
    var firstConsentState = false
    var secondConsetState = false
    let alertService = AlertService()
    @IBOutlet weak var navView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        profileImageButton.layoutIfNeeded()
        profileImageButton.subviews.first?.contentMode = .scaleAspectFill
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        let resource = ImageResource(downloadURL: URL(string: UserDefaults.standard.string(forKey: "pictureUrl")!)! , cacheKey: "my_avatar")
        profileImageButton.kf.setBackgroundImage(with: resource, for: .normal)
        firstConsentState = false
        secondConsetState = false
        firstCheckBoxImageView.image = uncheckedImage
        secondCheckBoxImageView.image = uncheckedImage
        confirmButton.isEnabled = false
        confirmButton.backgroundColor = UIColor(displayP3Red: 252/255, green: 188/255, blue: 56/255, alpha: 0.5)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func firstConsentTapped(_ sender: Any) {
        firstConsentState = !firstConsentState
        if firstConsentState {
            firstCheckBoxImageView.image = checkedImage
        } else {
            firstCheckBoxImageView.image = uncheckedImage
        }
        if firstConsentState && secondConsetState {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = K.Color.primaryColor
        } else {
            confirmButton.isEnabled = false
            confirmButton.backgroundColor = UIColor(displayP3Red: 252/255, green: 188/255, blue: 56/255, alpha: 0.5)
        }
    }
    @IBAction func secondConsentTapped(_ sender: Any) {
        secondConsetState = !secondConsetState
        if secondConsetState {
            secondCheckBoxImageView.image = checkedImage
        } else {
            secondCheckBoxImageView.image = uncheckedImage
        }
        if firstConsentState && secondConsetState {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = K.Color.primaryColor
        } else {
            confirmButton.isEnabled = false
            confirmButton.backgroundColor = UIColor(displayP3Red: 252/255, green: 188/255, blue: 56/255, alpha: 0.5)
        }
    }
    @IBAction func sendMailTapped(_ sender: Any) {
        sendEmail()
    }
    
    func sendEmail () {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([K.Path.supportWeflo])
            mail.setSubject("Contact support WeFlo")
            mail.setMessageBody("""
                <p>Caractéristique de l'iPhone : </p>
                <br>Modèle : \(UIDevice().type)</br>
        
        """, isHTML: true)
            let directoryPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!
            let filePath = directoryPath.stringByAppendingPathComponent(path: "/LocalDatabase/userCacheDB")
            let filePathLoggerDB = directoryPath.stringByAppendingPathComponent(path: "/LocalDatabase/loggerDB")
            if let data = NSData(contentsOfFile: filePath) {
                mail.addAttachmentData(data as Data, mimeType: "", fileName: "userCacheDB")
                
            }
            if let loggerData = NSData(contentsOfFile: filePathLoggerDB) {
                mail.addAttachmentData(loggerData as Data, mimeType: "", fileName: "loggerDB")
            }
            present(mail, animated: true)
        } else {
            print("Error mail")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .sent:
            DispatchQueue.main.async {
                let alert = self.alertService.alert(title: NSLocalizedString("mail-popup-title",comment : ""), body: NSLocalizedString("mail-popup-body",comment : ""), buttonTitle: NSLocalizedString("mail-popup-button",comment : "")) {
                }
            self.present(alert, animated: true) {
                controller.dismiss(animated: true) {
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            }
      
        case .cancelled:
            controller.dismiss(animated: true, completion: nil)
        default:
            controller.dismiss(animated: true, completion: nil)
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        _ = navigationController?.popViewController(animated: true)
    }

}
extension String {
    func stringByAppendingPathComponent(path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
}
