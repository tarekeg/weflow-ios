//
//  SettingsViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 27/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import CoreTelephony
import Kingfisher
import Reachability
import SystemConfiguration
import SwiftyJSON
import SafariServices
import FirebaseCrashlytics


class SettingsViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var trackingSwitch: UISwitch!
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var trackingStateLabel: UILabel!
    @IBOutlet weak var alertLocLabel: UILabel!
    @IBOutlet weak var alertLocImageView: UIImageView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var highAccuracySwitch: UISwitch!
    @IBOutlet weak var alertHighAccuracyLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var alertHighAccuracyImageView: UIImageView!
    @IBOutlet weak var contactUsButton: UIButton!
    @IBOutlet weak var irebyTermsOfUseButton: UIButton!
    @IBOutlet weak var wefloTermsOfUseButton: UIButton!
    @IBOutlet weak var gamesRuleButton: UIButton!
    @IBOutlet weak var baremeButton: UIButton!
    @IBOutlet weak var viewsHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var syncButton: UIButton!
    
    let reachability = try! Reachability()
    let tripDiaryStateMachine = TripDiaryStateMachine.instance()
    

    let textAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont(name: "Avenir-Medium", size: 12)!,
        .foregroundColor: K.Color.chacoralGrey,
        .underlineStyle: NSUnderlineStyle.single.rawValue]
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setButtonTitle()
        hideSystemPointsButton()
        versionLabel.text = "Version \(String(describing: Bundle.main.releaseVersionNumber!)) (\(String(describing: Bundle.main.buildVersionNumber!)))"
        
        DispatchQueue.main.async {
            
            if(self.isTripEndedAvailable()){
                self.syncButton.isEnabled = true
            } else {
                self.syncButton.isEnabled = false
            }
        }
        setStateAccuracySwitch()
        profileImageButton.layoutIfNeeded()
        profileImageButton.subviews.first?.contentMode = .scaleAspectFill
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        
    }
    func hideSystemPointsButton () {
        if UserTripData.progFid != "Ambassadeurs WeFlo" {
            viewsHeightConstraint.constant = viewsHeightConstraint.constant - 25
            baremeButton.isHidden = true
        }
    }
    func setButtonTitle () {
        let irebyTermsOfUseTitle = NSMutableAttributedString(string: NSLocalizedString("cgu-ireby",comment: ""),attributes: textAttributes)
        let wefloTermsOfUseTitle = NSMutableAttributedString(string: NSLocalizedString("cgu-weflo",comment: ""),attributes: textAttributes)
        let gamesRule = NSMutableAttributedString(string: NSLocalizedString("politique-confidentialité",comment: ""),attributes: textAttributes)
        let baremeTitle = NSMutableAttributedString(string: NSLocalizedString("bareme", comment: ""),attributes: textAttributes)
        baremeButton.setAttributedTitle(baremeTitle, for: .normal)
        irebyTermsOfUseButton.setAttributedTitle(irebyTermsOfUseTitle, for: .normal)
        wefloTermsOfUseButton.setAttributedTitle(wefloTermsOfUseTitle, for: .normal)
        gamesRuleButton.setAttributedTitle(gamesRule, for: .normal)
        contactUsButton.setTitle(NSLocalizedString("contact-us", comment: ""), for: .normal)
        contactUsButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setStateTrackingSwitch()
        tabBarController?.delegate = self
        let resource = ImageResource(downloadURL: URL(string: UserDefaults.standard.string(forKey: "pictureUrl")!)! , cacheKey: "my_avatar")
        profileImageButton.kf.setBackgroundImage(with: resource, for: .normal)
        NotificationCenter.default.addObserver(self, selector: #selector(getLocation), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getLocation), name: UIApplication.didBecomeActiveNotification, object: nil)
        getLocation()
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(UIApplication.willEnterForegroundNotification)
        NotificationCenter.default.removeObserver(UIApplication.didBecomeActiveNotification)
        NotificationCenter.default.removeObserver(reachability)
    }
    
    
    @IBAction func toggleAccuracy(_ sender: Any) {
        if (getConfig() == 0){
            print("no data")
        } else if(getConfig() == -1){
            alertHighAccuracyLabel.textColor = .clear
            alertHighAccuracyImageView.tintColor = .clear
            alertHighAccuracyImageView.isHidden = true
            let newConfig = [AnyHashable("accuracy") : 100]
            setConfig(newConfig: newConfig)
           
        } else {
            alertHighAccuracyLabel.textColor = K.Color.graySeven
            alertHighAccuracyImageView.tintColor = K.Color.graySeven
            alertHighAccuracyImageView.isHidden = false
            let newConfig = [AnyHashable("accuracy") : -1]
            setConfig(newConfig: newConfig)
        }
        
    }
    
    func getConfig() -> Int {
        var accuracy = 0
        let cfg = ConfigManager.instance()
        guard let retDict = DataUtils.wrapper(toDict: cfg) else { return accuracy }
        accuracy = retDict[AnyHashable("accuracy")] as! Int
        return accuracy
    }
    
    func setConfig (newConfig : [AnyHashable:Any]) {
        let newCfg = LocationTrackingConfig()
        DataUtils.dict(toWrapper: newConfig, wrapper: newCfg)
        ConfigManager.update(newCfg)
    }
    
    
    
    @IBAction func startStopTracking(_ sender: Any) {
        let currstate = TripDiaryStateMachine.getStateName(tripDiaryStateMachine!.currState)
        if(currstate! != "STATE_TRACKING_STOPPED") {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CFCTransitionNotificationName), object: CFCTransitionForceStopTracking)
            trackingStateLabel.text =  NSLocalizedString("tracking-disabled",comment : "")
        } else {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CFCTransitionNotificationName), object: CFCTransitionStartTracking)
            trackingStateLabel.text =  NSLocalizedString("tracking-enabled",comment : "")
        }
    }
    
    
    
    @IBAction func forceSyncTapped(_ sender: Any) {
        forceSync()
    }
    

    
    func presentSafari(url : String) {
        let vc = SFSafariViewController(url: URL(string: url)!)
        present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func toWebView(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            presentSafari(url: K.Path.baseURLWeb + "/cgu")
        case 1:
            presentSafari(url: "")
        case 2:
            presentSafari(url: K.Path.wefloWix + "mentions-légales".stringByAddingPercentEncodingForRFC3986())
        case 3:
            presentSafari(url: K.Path.wefloWix + "politique-de-confidentialité".stringByAddingPercentEncodingForRFC3986())
        default:
            return
        }
    }
    
    @IBAction func toBareme(_ sender: Any) {
        self.performSegue(withIdentifier: "toBareme", sender: nil)
    }
    
    
    

    
    
  
    
    func setStateTrackingSwitch () {
        let currstate = TripDiaryStateMachine.getStateName(tripDiaryStateMachine!.currState)
        if(currstate != "STATE_TRACKING_STOPPED"){
            trackingSwitch.setOn(true, animated: false)
            trackingStateLabel.text = NSLocalizedString("tracking-enabled",comment : "")
        } else {
            trackingSwitch.setOn(false, animated: false)
            trackingStateLabel.text = NSLocalizedString("tracking-disabled",comment : "")
        }
    }
    func setStateAccuracySwitch () {
    if(getConfig() == -1){
        alertHighAccuracyLabel.textColor = K.Color.graySeven
        alertHighAccuracyImageView.tintColor = K.Color.graySeven
        highAccuracySwitch.setOn(true, animated: false)
        
    } else if(getConfig() == 100){
        alertHighAccuracyLabel.textColor = .clear
        alertHighAccuracyImageView.tintColor = .clear
        highAccuracySwitch.setOn(false, animated: false)
    }
    }
    
    @objc func getLocation() {
        DispatchQueue.main.async {
            if(self.isTripEndedAvailable()){
                self.syncButton.isEnabled = true
            } else {
                self.syncButton.isEnabled = false
            }
        }
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                didChangeLocAuthorization(locationIsActive: false)
            case .restricted:
                didChangeLocAuthorization(locationIsActive: false)
            case .denied:
                didChangeLocAuthorization(locationIsActive: false)
                trackingStateLabel.text = NSLocalizedString("tracking-disabled",comment : "")
            case .authorizedAlways:
                didChangeLocAuthorization(locationIsActive: true)
                alertLocImageView.tintColor = .clear
                alertLocImageView.isHidden = true
                alertLocLabel.textColor = .clear
            case .authorizedWhenInUse:
                didChangeLocAuthorization(locationIsActive: true)
                alertLocImageView.tintColor = .clear
                alertLocImageView.isHidden = true
                alertLocLabel.textColor = .clear
            @unknown default:
                break
            }
        } else {
            print("Location services are not enabled")
        }
    }
    
    func didChangeLocAuthorization(locationIsActive : Bool) {
        if(locationIsActive == true){
            alertLocImageView.isHidden = true
            alertLocImageView.tintColor = .clear
            alertLocLabel.textColor = .clear
            trackingSwitch.isEnabled = true
            setStateTrackingSwitch()
        } else {
            alertLocImageView.isHidden = false
            alertLocImageView.tintColor = K.Color.graySeven
            alertLocLabel.textColor = K.Color.graySeven
            trackingStateLabel.text = NSLocalizedString("tracking-disabled",comment : "")
            trackingSwitch.isOn = false
            trackingSwitch.isEnabled = false
        }
    }
    
    
    
  
    
    
    
    func forceSync() {
        BEMServerSyncCommunicationHelper.backgroundSync()
        BuiltinUserCache.database()?.checkAfterPull()
        LocalNotificationManager.addNotification("all sync completed", showUI: true)
    }
    
    
    func isTripEndedAvailable () -> Bool {
        let builtInUserCache = BuiltinUserCache()
        let data = JSON(builtInUserCache.getMessageForInterval("statemachine/transition",tq: BuiltinUserCache.getAllTimeQuery(),withMetadata: true) as Any)
        var isTripEndedInData = false
        if(data.count != 0){
            for i in 0...data.count - 1 {
                
                if data[i]["data"]["transition"].stringValue == "T_TRIP_ENDED" {
                    isTripEndedInData = true
                    return isTripEndedInData
                    
                }
            }
        }
        return isTripEndedInData
    }
 
    
    
    @IBAction func toSomeQuestionTapped(_ sender: Any) {
        self.performSegue(withIdentifier: "toHelp", sender: nil)
    }
    
    
}


extension Bundle {

    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }

    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }

}

extension SettingsViewController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print(tabBarController.selectedIndex)
        self.scrollView.setContentOffset(.zero, animated: true)
        
    }
}

