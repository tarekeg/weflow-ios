//
//  OnboardingView.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 13/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit


class OnboardingView: UIView, OnboardingPageViewControllerDelegate {
    
    
    @IBOutlet weak var pageControl : UIPageControl!
    @IBOutlet weak var confirmButton : UIButton!
    
    
    func setupPageController(numberOfPages: Int) {
        pageControl.numberOfPages = numberOfPages
    }
    
    func turnPageController(to index: Int) {
        print(index)
        if(index != 3) {
            confirmButton.isEnabled = false
            confirmButton.backgroundColor = UIColor(displayP3Red: 252/255, green: 188/255, blue: 56/255, alpha: 0.5)
        } else {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = K.Color.primaryColor

        }
        pageControl.currentPage = index
    }

}
