//
//  OnboardingPageViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 13/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit


protocol OnboardingPageViewControllerDelegate : class {
    func setupPageController(numberOfPages : Int )
    func turnPageController(to index : Int)
}


class OnboardingPageViewController: UIPageViewController {
    
    
    
    var images : [UIImage]? = [UIImage(named: "shield")!,UIImage(named: "pin")!,UIImage(named: "composant-14-1")!,UIImage(named: "composant-15-1")!]
    var fragmentTitle : [String]? = [NSLocalizedString("fragment-title-1",comment : ""),NSLocalizedString("fragment-title-2",comment : ""),NSLocalizedString("fragment-title-3",comment : ""),NSLocalizedString("fragment-title-4",comment : "")]
    var fragmentDescription : [String]? = [NSLocalizedString("fragment-description-1",comment : ""),NSLocalizedString("fragment-description-2",comment : ""),NSLocalizedString("fragment-description-3",comment : ""),NSLocalizedString("fragment-description-4",comment : "")]
    
    
    weak var pageViewControllerDelegate : OnboardingPageViewControllerDelegate?

    
    
    struct Storyboard {
        static let OnboardingViewController = "OnboardingViewController"
    }
    
    
    lazy var controllers : [UIViewController] = {
         
         let storyboard = UIStoryboard(name: "Main", bundle: nil )
         var controllers = [UIViewController]()
         if let images = images {
             for image in images {
                 let OnboardingVC = storyboard.instantiateViewController(withIdentifier: Storyboard.OnboardingViewController)
                 controllers.append(OnboardingVC)
             }
         }
         
         self.pageViewControllerDelegate?.setupPageController(numberOfPages: controllers.count)
         return controllers
     }()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        self.turnToPage(index : 0)
        // Do any additional setup after loading the view.
    }
    
    
    
    func turnToPage(index: Int) {
        let controller = controllers[index]
        var direction = UIPageViewController.NavigationDirection.forward
        
        if let currentVC = viewControllers? .first {
            let currentIndex = controllers.firstIndex(of: currentVC)
            if currentIndex! > index {
                direction = .reverse
            }
        }
        self.configureDisplaying(viewController : controller)
        
        setViewControllers([controller ], direction: direction, animated: true, completion: nil )
    }
    
    func configureDisplaying(viewController : UIViewController) {
           for (index, vc) in controllers.enumerated(){
               if viewController === vc {
                   if let OnboardingVC = viewController as? OnboardingViewController {
                       OnboardingVC.titleText = self.fragmentTitle![index]
                       OnboardingVC.descriptionText = self.fragmentDescription![index]
                       OnboardingVC.image = self.images?[index]
                       self.pageViewControllerDelegate?.turnPageController(to: index)
                   }
               }
           }
       }
    

}

extension OnboardingPageViewController : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
         if let index = controllers.firstIndex(of :viewController){
                   if index > 0 {
                       return controllers[index - 1]
                   }
               }
               return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
         if let index = controllers.firstIndex(of :viewController){
                   if index < controllers.count - 1 {
                       return controllers[index + 1]
                   }
               }
               return nil
    }
    
    
}

extension OnboardingPageViewController : UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        self.configureDisplaying(viewController: pendingViewControllers.first  as! OnboardingViewController)
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if !completed {
            self.configureDisplaying(viewController: previousViewControllers.first as! OnboardingViewController)
        }
    }
}

