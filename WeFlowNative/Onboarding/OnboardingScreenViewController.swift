//
//  OnboardingScreenViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 13/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

protocol OnboardingScreenDelegate {
    func didAcceptCGU(isAccepted : Bool)
}

class OnboardingScreenViewController: UIViewController {
    
    
    @IBOutlet weak var onboardingView: OnboardingView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    
    var delegate : OnboardingScreenDelegate?
    let alertService = AlertService()
    let hyperlinks = [NSLocalizedString("tcu",comment : ""):K.Path.baseURLWeb + "/cgu"]
    let bodyString = NSLocalizedString("popup-cgu",comment : "")
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowOnboardingPageViewController" {
            if let onboardingPageVC = segue.destination as? OnboardingPageViewController {
                onboardingPageVC.pageViewControllerDelegate = onboardingView
            }
        }
        
    }
    @IBAction func buttonTapped(_ sender: Any) {
        let alertVC = alertService.alertWithOption(title: NSLocalizedString("popup-title-cgu",comment : ""), body: bodyString, buttonTitle: NSLocalizedString("popup-cgu-accept",comment : ""), dismissTitle: NSLocalizedString("popup-cgu-refuse",comment : "")) {
            
            UserDefaults.standard.set(true, forKey: "isIntroDone")
            
            self.dismiss(animated: true) {
                self.delegate?.didAcceptCGU(isAccepted: true)
            }
            
        }
        
        self.present(alertVC, animated: true)
    }
    
}
