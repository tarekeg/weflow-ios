//
//  OnboardingViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 13/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fragmentTitle: UILabel!
    @IBOutlet weak var fragmentDescription: UITextView!
    
    let hyperlinks = [NSLocalizedString("see-cgu",comment : "") : K.Path.wefloWix + "mentions-légales".stringByAddingPercentEncodingForRFC3986(),NSLocalizedString("settings",comment : ""):UIApplication.openSettingsURLString,"Ireby.Pro":K.Path.baseURLWeb]
    
    var image : UIImage? {
        didSet {
            self.imageView?.image = image
        }
    }
    var titleText: String? {
        didSet {
            DispatchQueue.main.async {
              self.fragmentTitle.text = self.titleText
            }
        }
    }
    
    var descriptionText: String? {
        didSet {
            DispatchQueue.main.async {
              self.fragmentDescription.text = self.descriptionText
                self.updateTextView()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(false, forKey: "info-popup")
        self.imageView.image = image
        self.fragmentTitle.text = titleText
        self.fragmentDescription.text = descriptionText
//        updateTextView()
    }
    
    func updateTextView () {
        let font = fragmentDescription.font
        fragmentDescription.addHyperLinksToText(originalText: fragmentDescription.text!, hyperLinks: hyperlinks)
        fragmentDescription.font = font
        fragmentDescription.textColor = K.Color.grayThirtySix
        fragmentDescription.textAlignment = .center
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension UITextView {

  func addHyperLinksToText(originalText: String, hyperLinks: [String: String]) {
    let style = NSMutableParagraphStyle()
    style.alignment = .left
    let attributedOriginalText = NSMutableAttributedString(string: originalText)
    for (hyperLink, urlString) in hyperLinks {
        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
        let fullRange = NSRange(location: 0, length: attributedOriginalText.length)
        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont(name: "Avenir-Roman", size: 12)!, range: fullRange)
    }

    self.linkTextAttributes = [
        NSAttributedString.Key.foregroundColor: K.Color.grayThirtySix,
        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
    ]
    self.attributedText = attributedOriginalText
  }
}

