//
//  DiaryTableViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 13/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import Reachability
import Alamofire
import SafariServices
import Sentry
import SkeletonView

class DiaryTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PopupViewControllerDelegate, DetailTripViewControllerDelegate, AlertSyncDelegate, URLSessionDelegate, URLSessionDataDelegate, URLSessionTaskDelegate, UIScrollViewDelegate, SkeletonTableViewDataSource, SkeletonTableViewDelegate {
    func handleReselect() {
        tableView?.setContentOffset(.zero, animated: true)
    }
    
    func sendTag(tag: Int) {
        buttonTag = tag
    }
    
    func confirmTapped(indexPath: IndexPath, confirmedSections: JSON, standardPoints: String, proPoints: String) {
        allTrips[indexPath.section]![indexPath.row]["is_confirmed"] = true
        allTrips[indexPath.section]![indexPath.row]["standard_points"].int = Int(standardPoints)
        allTrips[indexPath.section]![indexPath.row]["pro_points"].int = Int(proPoints)
        allTrips[indexPath.section]![indexPath.row]["sectionsFromIreby"] = confirmedSections
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    
    
    
    
    
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var standardPointView: CustomView!
    @IBOutlet weak var proPointView: CustomView!
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var standardPointLabel: UILabel!
    @IBOutlet weak var proPointLabel: UILabel!
    @IBOutlet weak var infoPeriodTripListLabel: UILabel!
    @IBOutlet weak var syncButton: UIButton!
    
    
    
    var buffer:NSMutableData = NSMutableData()
    var refreshControl : UIRefreshControl!
    var allTrips : [Int : [JSON]] = [:]
    var tripDay : [JSON] = []
    var tripDays : JSON = []
    var confirmedTrips : JSON = []
    var numberOfDays = 0
    var expectedContentLength : Int64 = 0
    var percentageDownloaded : Float = 0
    let RF = ReusableFunctions()
    let network: NetworkManager = NetworkManager.sharedInstance
    let alertService = AlertService()
    var buttonTag : Int?
    var session:URLSession?
    var onloading = false
    private var observation: NSKeyValueObservation?
    
    deinit {
        observation?.invalidate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let resource = ImageResource(downloadURL: URL(string: UserDefaults.standard.string(forKey: "pictureUrl")!)! , cacheKey: "my_avatar")
        profileImageButton.kf.setBackgroundImage(with: resource, for: .normal)
        tabBarController?.delegate = self
        getStats()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "HeaderDiaryTableViewCell", bundle: nil), forHeaderFooterViewReuseIdentifier: "headerView")
        tableView.register(UINib(nibName: "DiaryTableViewCell", bundle: nil), forCellReuseIdentifier: "DiaryTableViewCell")
        profileImageButton.layoutIfNeeded()
        tableView.isSkeletonable = true
        tableView.showAnimatedSkeleton()
        self.tripDays = UserTripData.userTrips
        print("Nombre de trips = ",self.tripDays.count)
        imageViewHeightConstraint.constant = self.view.frame.height * 0.19
        let configuration = URLSessionConfiguration.default
        session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        percentageDownloaded = 0.0
        alertService.syncDelegate = self
        setUI()
        
        
        profileImageButton.subviews.first?.contentMode = .scaleAspectFill
        let resource = ImageResource(downloadURL: URL(string: UserDefaults.standard.string(forKey: "pictureUrl")!)! , cacheKey: "my_avatar")
        profileImageButton.kf.setBackgroundImage(with: resource, for: .normal, options: [.forceRefresh])
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = .clear
        self.refreshControl.tintColor = .lightGray
        self.refreshControl.addTarget(self, action: #selector(methodPullToRefresh), for: UIControl.Event.valueChanged)
        NetworkManager.isUnreachable { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.tableView.stopSkeletonAnimation()
                self.view.hideSkeleton()
                self.infoPeriodTripListLabel.text = ""
                self.tableView.setEmptyMessage(NSLocalizedString("no-connection",comment : ""))
                self.tableView.addSubview(self.refreshControl)
            }
        }
        DispatchQueue.main.async {
            self.infoPeriodTripListLabel.text = NSLocalizedString("info-period", comment: "")
        }
        
        
        DispatchQueue.main.async {
            self.getDataToSync(jsonArray: UserTripData.userTrips, fromFilter: false)
        }
    }
    
    
    
    
    
    func setUI () {
        proPointView.backgroundColor = K.Color.secondaryColor
        standardPointView.backgroundColor = K.Color.primaryColor
        proPointLabel.text = Point.pro
        standardPointLabel.text = Point.standard
    }
    
    func showPopup () {
        let alert = alertService.alert(title: NSLocalizedString("popup-syncIreby-title", comment: ""), body: NSLocalizedString("popup-syncIreby-body", comment: ""), buttonTitle: NSLocalizedString("error-popup-button", comment: "")) {
            UserDefaults.standard.set(true, forKey: "info-popup")
        }
        tabBarController?.present(alert, animated: true)
    }
    
    var canRefresh = true
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < -100 { //change 100 to whatever you want
            
            if canRefresh && !self.refreshControl.isRefreshing {
                
                self.canRefresh = false
                self.refreshControl.beginRefreshing()
                
                self.methodPullToRefresh() // your viewController refresh function
            }
        }else if scrollView.contentOffset.y >= 0 {
            
            self.canRefresh = true
        }
    }
    
    
    func validateTapped(firstDate: String, lastDate: String) {
        tableView.isSkeletonable = true
        tableView.showAnimatedSkeleton()
        refreshControl.removeFromSuperview()
        let firstTimeStamp = RF.formatStringToDateToTS(strDate: firstDate)
        let lastTimeStamp = RF.formatStringToDateToTS(strDate: lastDate) + 86400
        let startDate = " " + RF.formatFrenchDate(strDate: firstDate)!
        let endDate = " " + RF.formatFrenchDate(strDate: lastDate)!
        let reachability = try! Reachability()
        if (reachability.connection == .wifi || reachability.connection == .cellular) {
            self.percentageDownloaded = 0.0
            self.expectedContentLength = 0
            var section : [String : Any] = [:]
            var sections : [[String : Any]] = []
            var tripDatas : [TripData] = []
            var formattedSections : [Section] = []
            var trips : [Trip] = []
            self.numberOfDays = 0
            self.tripDays = []
            self.tripDay = []
            confirmedTrips = []
            allTrips.removeAll()
            self.tableView.reloadData()
            self.tableView.setEmptyMessage("")
            self.infoPeriodTripListLabel.text = NSLocalizedString("between-period", comment: "") +  startDate + " " + NSLocalizedString("and", comment: "") + endDate
            let params = [
                "user" : UserDefaults.standard.string(forKey: "Token")!,
                "key_list" : [
                    "analysis/recreated_location",
                    "inference/prediction"
                ],
                "start_time" : firstTimeStamp,
                "end_time" :lastTimeStamp
            ] as [String : Any]
            
            let url = URL(string: K.Path.baseURL + "/datastreams/find_entries/timestamp")!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            let task = session!.dataTask(with: request as URLRequest,completionHandler:  {(data, response, error) -> Void in
                if let value = response?.expectedContentLength {
                    self.expectedContentLength = value
                    guard let data = data else {
                        return
                    }
                    
                    self.buffer.append(data)
                    self.percentageDownloaded = Float(self.buffer.length) / Float(self.expectedContentLength)
                    do{
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any]
                        let rawJson = JSON(json)
                        let rawData = rawJson["phone_data"].arrayValue
                        if !rawData.isEmpty {
                            let predictions = rawData.filter({ (json) -> Bool in
                                return json["metadata"]["key"].stringValue == "inference/prediction"; })
                            let recreatedLocations = rawData.filter({ (json) -> Bool in
                                return json["metadata"]["key"].stringValue == "analysis/recreated_location"; })
                            let jsonRecreatedLocation = JSON(recreatedLocations)
                            let jsonPrediction = JSON(predictions)
                            DispatchQueue.main.async {
                                if let items = jsonRecreatedLocation.array {
                                    for item in items {
                                        if let sectionId = item["data"]["section"]["$oid"].string {
                                            let tripData = TripData(sectionId: sectionId, speed: item["data"]["speed"].doubleValue, distance: item["data"]["distance"].doubleValue, location: Location(latitude: item["data"]["loc"]["coordinates"][1].doubleValue, longitude: item["data"]["loc"]["coordinates"][0].doubleValue))
                                            tripDatas.append(tripData)
                                        }
                                    }
                                }
                                if let items = jsonPrediction.array {
                                    for item in items {
                                        if let tripId = item["data"]["trip_id"]["$oid"].string {
                                            section["tripId"] = tripId
                                            section["sectionId"] = item["data"]["section_id"]["$oid"].stringValue
                                            section["end_ts"] = item["data"]["end_ts"].doubleValue
                                            section["start_ts"] = item["data"]["start_ts"].doubleValue
                                            section["mode"] = Array(item["data"]["predicted_mode_map"].dictionaryValue)[0].key
                                            sections.append(section)
                                        }
                                    }
                                }
                                let groupedSection = Dictionary(grouping: tripDatas, by: { $0.sectionId })
                                for item in sections {
                                    for (key,value) in groupedSection {
                                        if key == item["sectionId"] as! String {
                                            var speeds : [Double] = []
                                            var distances : [Double] = []
                                            for i in 0...value.count - 1 {
                                                speeds.append(value[i].speed)
                                                distances.append(value[i].distance)
                                            }
                                            let section = Section(tripId: item["tripId"] as! String, sectionId: item["sectionId"] as! String, mode: item["mode"] as! String, startTs: item["start_ts"] as! Double, endTs: item["end_ts"] as! Double, data: value,speeds: speeds,distances: distances)
                                            formattedSections.append(section)
                                        }
                                    }
                                }
                                formattedSections.sort {
                                    $0.startTs < $1.startTs
                                }
                                
                                let groupedTrip = Dictionary(grouping: formattedSections, by: {$0.tripId})
                                
                                for (key,value) in groupedTrip {
                                    var totalDistance = 0.0
                                    for i in 0...value.count - 1 {
                                        totalDistance += value[i].distances.reduce(0, +)
                                    }
                                    let trip = Trip(tripId: key, sections: value, totalDistance: totalDistance)
                                    trips.append(trip)
                                }
                                trips.sort {
                                    $0.sections[0].startTs < $1.sections[0].startTs
                                }
                            }
                            
                            
                        }
                        DispatchQueue.main.async { [self] in
                            let json = try! JSONEncoder().encode(trips)
                            UserTripData.userTrips = JSON(json)
                            self.tripDays = UserTripData.userTrips
                            self.getDataToSync(jsonArray: UserTripData.userTrips,fromFilter : true)
                        }
                    }catch{
                        SentrySDK.capture(error: error)
                        print(error)
                        
                    }
                    
                }
            })
            
            task.resume()
        } else {
            self.tableView.stopSkeletonAnimation()
            self.view.hideSkeleton()
            self.infoPeriodTripListLabel.text = ""
            self.allTrips.removeAll()
            self.tableView.reloadData()
            self.tableView.setEmptyMessage(NSLocalizedString("no-connection",comment : ""))
        }
        
        
    }
    
    
    
    @objc func methodPullToRefresh() {
        
        if onloading == false {
            onloading = true
            tableView.isSkeletonable = true
            tableView.showAnimatedSkeleton()
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            tableView.isSkeletonable = true
            tableView.showAnimatedSkeleton()
            getStats()
            tableView.setEmptyMessage("")
            let reachability = try! Reachability()
            if (reachability.connection == .wifi || reachability.connection == .cellular) {
                var section : [String : Any] = [:]
                var sections : [[String : Any]] = []
                var tripDatas : [TripData] = []
                var formattedSections : [Section] = []
                var trips : [Trip] = []
                self.numberOfDays = 0
                self.tripDays = []
                self.tripDay = []
                confirmedTrips = []
                allTrips.removeAll()
                self.infoPeriodTripListLabel.text = NSLocalizedString("info-period", comment: "")
                self.tableView.reloadData()
                self.tableView.setEmptyMessage("")
                DispatchQueue.main.async {
                    //                    self.loadingView.isHidden = false
                }
                let params = [
                    "user" : UserDefaults.standard.string(forKey: "Token")!,
                    "key_list" : [
                        "analysis/recreated_location",
                        "inference/prediction"
                    ],
                    "start_time" : Date.changeDaysBy(days: -5).timeIntervalSince1970,
                    "end_time" :Date.changeDaysBy(days: 1).timeIntervalSince1970
                ] as [String : Any]
                
                let url = URL(string: K.Path.baseURL + "/datastreams/find_entries/timestamp")!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                } catch let error {
                    print(error.localizedDescription)
                }
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                let task = session!.dataTask(with: request as URLRequest,completionHandler:  {(data, response, error) -> Void in
                    if let value = response?.expectedContentLength {
                        self.expectedContentLength = value
                        guard let data = data else {
                            return
                        }
                        
                        self.buffer.append(data)
                        self.percentageDownloaded = Float(self.buffer.length) / Float(self.expectedContentLength)
                        do{
                            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any]
                            let rawJson = JSON(json)
                            let rawData = rawJson["phone_data"].arrayValue
                            if !rawData.isEmpty {
                                let predictions = rawData.filter({ (json) -> Bool in
                                    return json["metadata"]["key"].stringValue == "inference/prediction"; })
                                let recreatedLocations = rawData.filter({ (json) -> Bool in
                                    return json["metadata"]["key"].stringValue == "analysis/recreated_location"; })
                                let jsonRecreatedLocation = JSON(recreatedLocations)
                                let jsonPrediction = JSON(predictions)
                                DispatchQueue.main.async { [self] in
                                    if let items = jsonRecreatedLocation.array {
                                        for item in items {
                                            if let sectionId = item["data"]["section"]["$oid"].string {
                                                let tripData = TripData(sectionId: sectionId, speed: item["data"]["speed"].doubleValue, distance: item["data"]["distance"].doubleValue, location: Location(latitude: item["data"]["loc"]["coordinates"][1].doubleValue, longitude: item["data"]["loc"]["coordinates"][0].doubleValue))
                                                tripDatas.append(tripData)
                                            }
                                        }
                                    }
                                    if let items = jsonPrediction.array {
                                        for item in items {
                                            if let tripId = item["data"]["trip_id"]["$oid"].string {
                                                section["tripId"] = tripId
                                                section["sectionId"] = item["data"]["section_id"]["$oid"].stringValue
                                                section["end_ts"] = item["data"]["end_ts"].doubleValue
                                                section["start_ts"] = item["data"]["start_ts"].doubleValue
                                                section["mode"] = Array(item["data"]["predicted_mode_map"].dictionaryValue)[0].key
                                                sections.append(section)
                                            }
                                        }
                                    }
                                    let groupedSection = Dictionary(grouping: tripDatas, by: { $0.sectionId })
                                    for item in sections {
                                        for (key,value) in groupedSection {
                                            if key == item["sectionId"] as! String {
                                                var speeds : [Double] = []
                                                var distances : [Double] = []
                                                for i in 0...value.count - 1 {
                                                    speeds.append(value[i].speed)
                                                    distances.append(value[i].distance)
                                                }
                                                let section = Section(tripId: item["tripId"] as! String, sectionId: item["sectionId"] as! String, mode: item["mode"] as! String, startTs: item["start_ts"] as! Double, endTs: item["end_ts"] as! Double, data: value,speeds: speeds,distances: distances)
                                                formattedSections.append(section)
                                            }
                                        }
                                    }
                                    formattedSections.sort {
                                        $0.startTs < $1.startTs
                                    }
                                    
                                    let groupedTrip = Dictionary(grouping: formattedSections, by: {$0.tripId})
                                    
                                    for (key,value) in groupedTrip {
                                        var totalDistance = 0.0
                                        for i in 0...value.count - 1 {
                                            totalDistance += value[i].distances.reduce(0, +)
                                        }
                                        let trip = Trip(tripId: key, sections: value, totalDistance: totalDistance)
                                        trips.append(trip)
                                    }
                                    trips.sort {
                                        $0.sections[0].startTs < $1.sections[0].startTs
                                    }
                                }
                                
                                
                            }
                            DispatchQueue.main.async { [self] in
                                let json = try! JSONEncoder().encode(trips)
                                UserTripData.userTrips = JSON(json)
                                self.tripDays = UserTripData.userTrips
                                self.getDataToSync(jsonArray: UserTripData.userTrips,fromFilter : true)
                            }
                        }catch{
                            SentrySDK.capture(error: error)
                            print(error)
                        }
                        
                    }
                })
                observation = task.progress.observe(\.fractionCompleted) { progress, _ in
                    DispatchQueue.main.async {
                        if progress.fractionCompleted == 1.0 {
                            DispatchQueue.main.async {
                                //                                self.loadingView.isHidden = true
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                self.onloading = false
                            }
                        }
                    }
                }
                
                
                task.resume()
                
                
                
                
                
            } else {
                self.tableView.stopSkeletonAnimation()
                self.view.hideSkeleton()
                self.onloading = false
                self.infoPeriodTripListLabel.text = ""
                self.allTrips.removeAll()
                self.tableView.reloadData()
                self.tableView.setEmptyMessage(NSLocalizedString("no-connection",comment : ""))
            }
        } else {
            self.refreshControl.endRefreshing()
        }
    }
    
    func getStats () {
        let reachability = try! Reachability()
        if (reachability.connection == .wifi || reachability.connection == .cellular) {
            var jsonResponse : JSON = []
            let headers: HTTPHeaders = [
                "authorization": "Bearer " + UserDefaults.standard.string(forKey: "ireby_access_token")!,
                "content-type": "application/json"
            ]
            var request = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/dashboard/points")!)
            request.headers = headers
            request.httpMethod = "GET"
            AF.request(request).responseJSON { [self] response in
                if(response.response?.statusCode != 500){
                    UserDefaults.standard.set(true, forKey: "ireby_synchronization")
                    RF.updateUIWefloNotSync(proView: proPointView, standardView: standardPointView, buttonSync: syncButton, isSynced:  UserDefaults.standard.bool(forKey: "ireby_synchronization"))
                    jsonResponse = JSON(response.value as Any)
                    if(response.response?.statusCode == 401){
                        RF.renewIrebyToken { [self] (success) -> Void in
                            if success {
                                getStats()
                            }
                        }
                    }
                    if jsonResponse["pro_points"].stringValue == "0" {
                        Point.pro = "0 Point"
                    } else {
                        Point.pro = jsonResponse["pro_points"].stringValue + " Points"
                    }
                    if jsonResponse["points"].stringValue == "0" {
                        Point.standard = "0 Point"
                    } else {
                        Point.standard = jsonResponse["points"].stringValue + " Points"
                    }
                    self.proPointLabel.text = Point.pro
                    self.standardPointLabel.text = Point.standard
                } else {
                    let headersNotSync: HTTPHeaders = [
                        "token" : UserDefaults.standard.string(forKey: "ireby_access_token")!   ,
                        "authorization": "Bearer " + K.Constant.irebyBearer,
                        "content-type": "application/json"
                    ]
                    
                    AF.request(K.Path.baseURLWeb + "/api/users/is-synced", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headersNotSync, interceptor: nil, requestModifier: nil).responseJSON { response in
                        let jsonResponse = JSON(response.value)
                        if jsonResponse["message"].boolValue == false {
                            UserDefaults.standard.set(false, forKey: "ireby_synchronization")
                            RF.updateUIWefloNotSync(proView: proPointView, standardView: standardPointView, buttonSync: syncButton, isSynced:  UserDefaults.standard.bool(forKey: "ireby_synchronization"))
                        } else {
                            UserDefaults.standard.set(true, forKey: "ireby_synchronization")
                            RF.updateUIWefloNotSync(proView: proPointView, standardView: standardPointView, buttonSync: syncButton, isSynced:  UserDefaults.standard.bool(forKey: "ireby_synchronization"))
                        }
                    }
                    
                }
            }
        } else {
            RF.updateUIWefloNotSync(proView: proPointView, standardView: standardPointView, buttonSync: syncButton, isSynced: true)
            print ("no reachable")
        }
        
    }
    
    @IBAction func syncTapped(_ sender: Any) {
        let alertVC = alertService.showSyncAlert() { [self] in
            if(buttonTag == 0) {
                self.performSegue(withIdentifier: "toSyncInApp", sender: nil)
            } else {
                self.performSegue(withIdentifier: "toSignInInApp", sender: nil)
            }
        }
        tabBarController!.present(alertVC, animated: true)
    }
    
    
    
    
    // MARK: - Table view data source
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(allTrips.count == 0 && self.percentageDownloaded == 1.0) {
            tableView.setEmptyMessage(NSLocalizedString("no-trips-saved",comment : ""))
            //            loadingView.isHidden = true
        } else if percentageDownloaded == 1.0 {
            //            loadingView.isHidden = true
        }
        return allTrips.count
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "DiaryTableViewCell"
    }
    func collectionSkeletonView(_ skeletonView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func numSections(in collectionSkeletonView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allTrips[section]!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiaryTableViewCell", for: indexPath) as! DiaryTableViewCell
        cell.selectionStyle = .none
        cell.confirmedImageView.isHidden = true
        cell.confirmedLabel.isHidden = true
        let trip = allTrips[indexPath.section]![indexPath.row]
        let sections = trip["sections"].arrayValue
        let startTimeTrip = RF.formatStartTimeTripTs(ts: trip["sections"][0]["startTs"].doubleValue)
        let distance = RF.formatDistance(trip["totalDistance"].doubleValue)
        let duration = RF.formatDuration(trip["sections"][0]["startTs"].doubleValue, trip["sections"][sections.count - 1]["endTs"].doubleValue)
        let sensedMode = getSectionsText(jsonArray: trip)
        let standardPoints =  trip["standard_points"].intValue
        let proPoints = trip["pro_points"].intValue
        if standardPoints == 0 && proPoints == 0 {
            cell.pointsLabel.text = ""
        } else {
            cell.pointsLabel.text = String(standardPoints + proPoints) + " Points"
        }
        if trip["is_confirmed"].boolValue {
            cell.confirmedImageView.isHidden = false
            cell.confirmedLabel.isHidden = false
        }
        cell.sensedModeLabel.text = ""
        for i in 0...sensedMode.count - 1 {
            if(i == sensedMode.count - 1){
                cell.sensedModeLabel.text! += sensedMode[i]
            } else {
                cell.sensedModeLabel.text! += sensedMode[i] + " - "
                
            }
        }
        cell.timeDistanceLabel.text = distance + " km - " +  duration + " - " + NSLocalizedString("at", comment: "") + " " + startTimeTrip
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerView") as? HeaderDiaryTableViewCell
                
        else {
            return nil
        }
        let startTs = allTrips[section]![0]["sections"][0]["startTs"].doubleValue
        let formattedDate = RF.formatTimeStampToHeaderDate(ts: startTs)
        let todayDate = RF.getCurrentDateForHeader(date: Date())
        let yesterdayDate = RF.getCurrentDateForHeader(date: Date.yesterday)
        if(todayDate == formattedDate) {
            header.headerLabel.text = NSLocalizedString("today",comment : "")
        } else if (yesterdayDate == formattedDate) {
            header.headerLabel.text = NSLocalizedString("yesterday",comment : "")
        } else {
            header.headerLabel.text = formattedDate
        }
        return header
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, identifierForHeaderInSection section: Int) -> ReusableHeaderFooterIdentifier? {
        return "headerView"
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "toDetails", sender: indexPath)
    }
    
    func formatDateForHeader(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy"
        
        if let formattedDate = dateFormatterGet.date(from: date) {
            return dateFormatterPrint.string(from: formattedDate)
        } else {
            return "dd mm yyyy"
        }
    }
    
    func formatDate(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE d MMMM"
        
        if let formattedDate = dateFormatterGet.date(from: date) {
            return dateFormatterPrint.string(from: formattedDate)
        } else {
            return "dd mm yyyy"
        }
        
    }
    func getSections (jsonArray : JSON) -> [JSON] {
        
        var arrayCoord : [JSON] = []
        for i in 0...jsonArray["features"].count - 1 {
            if(jsonArray["features"][i]["type"].stringValue == "FeatureCollection"){
                for j in 0...jsonArray["features"][i]["features"].count - 1 {
                    if(jsonArray["features"][i]["features"][j]["properties"]["feature_type"].stringValue == "section") {
                        arrayCoord.append(jsonArray["features"][i]["features"][j]["geometry"]["coordinates"])
                    }
                }
            }
        }
        return arrayCoord
    }
    func getSectionsText (jsonArray : JSON) -> [String] {
        var sensedMode : [String] = []
        if(jsonArray["sectionsFromIreby"].count != 0){
            
            for i in 0...jsonArray["sectionsFromIreby"].count - 1 {
                let sensedModeText = translateDetectedTransportModeFromIreby(transportMode: jsonArray["sectionsFromIreby"][i]["mode"].stringValue)
                sensedMode.append(sensedModeText)
            }
        } else {
            for i in 0...jsonArray["sections"].count - 1 {
                let sensedModeText = translateDetectedTransportMode(transportMode: jsonArray["sections"][i]["mode"].stringValue)
                sensedMode.append(sensedModeText)
            }
        }
        
        sensedMode.removeDuplicates()
        return sensedMode
    }
    func getSectionsAndDistance (jsonArray : JSON) -> [Any] {
        
        var allData : [Any] = []
        var sensedMode : [String:Any] = [:]
        let sections = jsonArray["sections"].arrayValue
        let totaldistance = jsonArray["totalDistance"].doubleValue
        for i in 0...sections.count - 1 {
            var rawDistance = 0.0
            let arrayDistance = sections[i]["distances"].arrayValue
            for j in 0...arrayDistance.count - 1 {
                rawDistance += arrayDistance[j].doubleValue
            }
            
            let distancePerSection = rawDistance
            let durationPerSection = (sections[i]["endTs"].doubleValue - sections[i]["startTs"].doubleValue)
            let percentage = (distancePerSection / totaldistance) * 100
            let averageSpeed = RF.getAverage(array: sections[i]["speeds"].arrayValue)
            let sectionId = sections[i]["sectionId"].stringValue
            let mode = sections[i]["mode"].stringValue
            
            
            sensedMode["id"] = sectionId
            sensedMode["sectionId"] = sectionId
            sensedMode["transport"] = sensedOriginalModePerSection(transportMode: mode)
            sensedMode["mode"] = sensedOriginalModePerSection(transportMode: mode)
            sensedMode["speed"] = averageSpeed
            sensedMode["distance"] = rawDistance / 1000
            sensedMode["duration"] = durationPerSection
            sensedMode["percentage"] = "\(percentage)"
            allData.append(sensedMode)
            
        }
        return allData
        
    }
    func syncTripsToIreby (allData : [Any]) {
        let irebyHeaders: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "content-type": "application/json"
        ]
        let paramDictionnary = ["trips":allData]
        let paramJSON = JSON(paramDictionnary)
        let paramString = paramJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
        var requestToIreby = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/action/trips")!)
        requestToIreby.headers = irebyHeaders
        requestToIreby.httpMethod = "POST"
        requestToIreby.httpBody = paramString.data(using: String.Encoding.utf8, allowLossyConversion: false)
        AF.request(requestToIreby).responseJSON { response in
            
            var requestGetTrips = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/dashboard/trips")!)
            requestGetTrips.httpMethod = "GET"
            requestGetTrips.headers = irebyHeaders
            AF.request(requestGetTrips).responseJSON { [self] response in
                let confirmedTrips = JSON(response.value as Any)
                self.confirmedTrips = confirmedTrips["trips"]
                if self.confirmedTrips.count != 0 {
                    for i in 0...self.confirmedTrips.count - 1 {
                        for j in 0...self.tripDays.count - 1 {
                            if(self.confirmedTrips[i]["tripId"].stringValue == UserTripData.userTrips[j]["tripId"].stringValue){
                                UserTripData.userTrips[j]["pro_points"].stringValue = self.confirmedTrips[i]["proPoints"].stringValue
                                UserTripData.userTrips[j]["standard_points"].stringValue = self.confirmedTrips[i]["standardPoints"].stringValue
                                UserTripData.userTrips[j]["sectionsFromIreby"] = self.confirmedTrips[i]["sections"]
                                UserTripData.userTrips[j]["is_confirmed"].boolValue = self.confirmedTrips[i]["is_confirmed"].boolValue
                            }
                        }
                    }
                }
                
                
                for i in (0...UserTripData.userTrips.count - 1).reversed() {
                    self.tripDay.append(UserTripData.userTrips[i])
                    if(i > 0) {
                        let firstSessionStartDayTs = getDay(ts : UserTripData.userTrips[i - 1]["sections"][0]["startTs"].doubleValue)
                        let secondSessionStartDayTs = getDay(ts: UserTripData.userTrips[i]["sections"][0]["startTs"].doubleValue)
                        if (firstSessionStartDayTs != secondSessionStartDayTs) {
                            self.allTrips[self.numberOfDays] = self.tripDay
                            self.tripDay = []
                            self.numberOfDays += 1
                        } else {
                            self.allTrips[self.numberOfDays] = self.tripDay
                        }
                    } else {
                        self.allTrips[self.numberOfDays] = self.tripDay
                    }
                }
                DispatchQueue.main.async {
                    //                        loadingView.isHidden = true
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    self.tableView.addSubview(self.refreshControl)
                    self.tableView.reloadData()
                    self.getStats()
                }
                
            }
            
            
        }
    }
    
    func getDataToSync (jsonArray : JSON, fromFilter : Bool) {
        var dataTosyncPerTrip : [String:Any] = [:]
        var allData : [Any] = []
        
        if(jsonArray.count != 0){
            for i in 0...jsonArray.count - 1 {
                
                UserTripData.userTrips[i]["is_confirmed"].boolValue = false
                UserTripData.userTrips[i]["pro_points"].stringValue = "0"
                UserTripData.userTrips[i]["standard_points"].stringValue = "0"
                
                let sections = jsonArray[i]["sections"].arrayValue
                dataTosyncPerTrip["tripId"] = jsonArray[i]["tripId"].stringValue
                dataTosyncPerTrip["completedAt"] = RF.longFormatDateFromTimeStamp(ts: sections[sections.count - 1]["endTs"].doubleValue)
                dataTosyncPerTrip["client"] = "ios"
                dataTosyncPerTrip["sections"] = getSectionsAndDistance(jsonArray: UserTripData.userTrips[i])
                allData.append(dataTosyncPerTrip)
            }
            let irebyHeaders: HTTPHeaders = [
                "authorization": "Bearer " + UserDefaults.standard.string(forKey: "ireby_access_token")!,
                "content-type": "application/json"
            ]
            let headers: HTTPHeaders = [
                "authorization": "Bearer " + UserDefaults.standard.string(forKey: "Token")!,
                "content-type": "application/json"
            ]
            var request = URLRequest(url: URL(string: K.Path.cleverApi + "trips")!)
            request.headers = headers
            request.httpMethod = "POST"
            request.httpBody = try! JSONSerialization.data(withJSONObject: allData)
            
            AF.request(request).responseJSON { response in
                print(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))
            }
            let paramDictionnary = ["trips":allData]
            let paramJSON = JSON(paramDictionnary)
            let paramString = paramJSON.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
            var requestToIreby = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/action/trips")!)
            requestToIreby.headers = irebyHeaders
            requestToIreby.httpMethod = "POST"
            requestToIreby.httpBody = paramString.data(using: String.Encoding.utf8, allowLossyConversion: false)
            AF.request(requestToIreby).responseJSON { [self] response in
                
                print("Trips = ",NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))
                let jsonResponse = JSON(response.value)
                if(response.response?.statusCode == 401){
                    RF.renewIrebyToken { [self] (success) -> Void in
                        if success {
                            syncTripsToIreby(allData: allData)
                        }
                    }
                } else if response.response?.statusCode != 200 {
                    SentrySDK.capture(message: "Something went wrong with IrebyApi : /api/pve/action/trips")
                } else {
                    var requestGetTrips = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/dashboard/trips")!)
                    requestGetTrips.httpMethod = "GET"
                    requestGetTrips.headers = irebyHeaders
                    AF.request(requestGetTrips).responseJSON { response in
                        if response.response?.statusCode != 200 {
                            SentrySDK.capture(message: "Something went wrong with IrebyApi : /api/pve/dashboard/trips")
                        }
                        let confirmedTrips = JSON(response.value as Any)
                        self.confirmedTrips = confirmedTrips["trips"]
                        if self.confirmedTrips.count != 0 {
                            for i in 0...self.confirmedTrips.count - 1 {
                                for j in 0...self.tripDays.count - 1 {
                                    if(self.confirmedTrips[i]["tripId"].stringValue == UserTripData.userTrips[j]["tripId"].stringValue){
                                        UserTripData.userTrips[j]["pro_points"].stringValue = self.confirmedTrips[i]["proPoints"].stringValue
                                        UserTripData.userTrips[j]["standard_points"].stringValue = self.confirmedTrips[i]["standardPoints"].stringValue
                                        UserTripData.userTrips[j]["sectionsFromIreby"] = self.confirmedTrips[i]["sections"]
                                        UserTripData.userTrips[j]["is_confirmed"].boolValue = self.confirmedTrips[i]["is_confirmed"].boolValue
                                    }
                                }
                            }
                        }
                        
                        
                        for i in (0...UserTripData.userTrips.count - 1).reversed() {
                            self.tripDay.append(UserTripData.userTrips[i])
                            if(i > 0) {
                                let firstSessionStartDayTs = getDay(ts : UserTripData.userTrips[i - 1]["sections"][0]["startTs"].doubleValue)
                                let secondSessionStartDayTs = getDay(ts: UserTripData.userTrips[i]["sections"][0]["startTs"].doubleValue)
                                if (firstSessionStartDayTs != secondSessionStartDayTs) {
                                    self.allTrips[self.numberOfDays] = self.tripDay
                                    self.tripDay = []
                                    self.numberOfDays += 1
                                } else {
                                    self.allTrips[self.numberOfDays] = self.tripDay
                                }
                            } else {
                                self.allTrips[self.numberOfDays] = self.tripDay
                            }
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            self.tableView.addSubview(self.refreshControl)
                            self.tableView.stopSkeletonAnimation()
                            self.view.hideSkeleton()
                            self.tableView.reloadData()
                            self.getStats()
                        }
                        
                        
                    }
                    
                }
                
            }
            
        } else {
            DispatchQueue.main.async {
                self.tableView.stopSkeletonAnimation()
                self.view.hideSkeleton()
                self.tableView.setEmptyMessage(NSLocalizedString("no-trips-saved",comment : ""))
                self.tableView.addSubview(self.refreshControl)
            }
            
        }
        
    }
    func getDay(ts : Double) -> Int {
        let startDateFromTs = RF.longFormatDateFromTimeStamp(ts: ts)
        let startDay = RF.formatStringToDate(strDate: startDateFromTs).get(.day)
        return startDay
    }
    func getMonth(ts : Double) -> Int {
        let startDateFromTs = RF.longFormatDateFromTimeStamp(ts: ts)
        let startMonth = RF.formatStringToDate(strDate: startDateFromTs).get(.month)
        return startMonth
    }
    func getYear(ts : Double) -> Int {
        let startDateFromTs = RF.longFormatDateFromTimeStamp(ts: ts)
        let startYear = RF.formatStringToDate(strDate: startDateFromTs).get(.year)
        return startYear
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetails"{
            let index = sender as? NSIndexPath
            let trip = allTrips[index!.section]![index!.row]
            if let destinationVC =  segue.destination as? DetailTripViewController {
                destinationVC.delegate = self
                destinationVC.tripData = trip
                destinationVC.indexPath = index as IndexPath?
            }
        }
        if segue.identifier == "toFilter" {
            if let destinationVC = segue.destination as? PopupViewController {
                destinationVC.delegate = self
            }
        }
    }
    
    
    func translateDetectedTransportMode (transportMode : String) -> String {
        switch transportMode {
        case "CAR":
            return NSLocalizedString("car",comment : "")
        case "WALKING":
            return NSLocalizedString("walk",comment : "")
        case "TRAIN":
            return NSLocalizedString("train",comment : "")
        case "BICYCLING":
            return NSLocalizedString("bicycle",comment : "")
        case "BUS":
            return NSLocalizedString("bus",comment : "")
        case "AIR_OR_HSR":
            return NSLocalizedString("plane",comment : "")
        case "SUBWAY":
            return NSLocalizedString("subway",comment : "")
        case "TRAM":
            return NSLocalizedString("tram",comment : "")
        default:
            return NSLocalizedString("other",comment : "")
        }
    }
    
    
    func translateDetectedTransportModeFromIreby (transportMode : String) -> String {
        switch transportMode {
        case "car":
            return NSLocalizedString("car",comment : "")
        case "walking":
            return NSLocalizedString("walk",comment : "")
        case "train":
            return NSLocalizedString("train",comment : "")
        case "cycling":
            return NSLocalizedString("bicycle",comment : "")
        case "bus":
            return NSLocalizedString("bus",comment : "")
        case "plane":
            return NSLocalizedString("plane",comment : "")
        case "subway":
            return NSLocalizedString("subway",comment : "")
        case "tram":
            return NSLocalizedString("tram",comment : "")
        case "carpooling":
            return NSLocalizedString("carpooling", comment: "")
        case "motorbike":
            return NSLocalizedString("motorbike", comment: "")
        case "e-scooter":
            return NSLocalizedString("e-scooter", comment: "")
        default:
            return NSLocalizedString("other",comment : "")
        }
    }
    
    func sensedOriginalModePerSection (transportMode : String) -> String {
        
        switch transportMode{
        case "UNKNOWN":
            return "other"
        case "CAR":
            return "car"
        case "WALKING":
            return "walking"
        case "BICYCLING":
            return "cycling"
        case "BUS":
            return "bus"
        case "TRAIN":
            return "train"
        case "AIR_OR_HSR":
            return "plane"
        case "SUBWAY":
            return "subway"
        case "TRAM":
            return "tram"
        default:
            return "other"
        }
    }
    
    
    
    
    @IBAction func filterTapped(_ sender: Any) {
        
        performSegue(withIdentifier: "toFilter", sender: nil)
        
    }
    
    
}

extension Array where Element: Hashable {
    func removingDuplicates() -> [Element] {
        var addedDict = [Element: Bool]()
        
        return filter {
            addedDict.updateValue(true, forKey: $0) == nil
        }
    }
    
    mutating func removeDuplicates() {
        self = self.removingDuplicates()
    }
}

extension UITableView {
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel()
        messageLabel.layer.frame = CGRect(x: self.frame.midX - 100, y: self.frame.midY - 100, width: 200, height: 200)
        let tableViewBackgroundView = UIView()
        messageLabel.text = message
        messageLabel.textColor = K.Color.chacoralGrey
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "Avenir-Medium", size: 15)
        //        messageLabel.sizeToFit()
        tableViewBackgroundView.addSubview(messageLabel)
        self.backgroundView = tableViewBackgroundView
    }
}

extension DiaryTableViewController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex == 1 {
            self.tableView.setContentOffset(.zero, animated: true)
        }
    }
}
extension Date {
    func get(_ components: Calendar.Component..., calendar: Calendar = Calendar.current) -> DateComponents {
        return calendar.dateComponents(Set(components), from: self)
    }
    
    func get(_ component: Calendar.Component, calendar: Calendar = Calendar.current) -> Int {
        return calendar.component(component, from: self)
    }
}
