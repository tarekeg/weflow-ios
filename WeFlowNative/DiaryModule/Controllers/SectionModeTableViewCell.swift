//
//  SectionModeTableViewCell.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 24/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

protocol SectionModeTableViewCellDelegate {
    func didTappedOnrow(row : Int, didSelectCar : Bool)
    func getMode(mode : String, row: Int)
    func didToggle()
}


class SectionModeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sectionImageView: UIImageView!
    @IBOutlet weak var sectionDescriptionLabel: UILabel!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var transportLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var carpoolingLabel: UILabel!
    @IBOutlet weak var carpoolingView: UIView!
    @IBOutlet weak var carpoolingCheckBoxImageView: UIImageView!
    @IBOutlet weak var carpoolingButton: UIButton!
    @IBOutlet weak var pointsEarnedByModeLabel: UILabel!
    
    var timerForShowScrollIndicator: Timer?
    var delegate : SectionModeTableViewCellDelegate?
    var transportList = [NSLocalizedString("bicycle",comment : ""), NSLocalizedString("walk",comment : ""), NSLocalizedString("bus",comment : ""), NSLocalizedString("car",comment : ""), NSLocalizedString("plane",comment : ""), NSLocalizedString("subway",comment : ""), NSLocalizedString("tram",comment : ""), NSLocalizedString("train",comment : ""),NSLocalizedString("e-scooter", comment: ""), NSLocalizedString("motorbike", comment: ""),NSLocalizedString("other", comment: "")]
    var carpoolingChecked = false
    override func awakeFromNib() {
        super.awakeFromNib()
        carpoolingLabel.text = NSLocalizedString("carpooling", comment: "")
        buttonView.layer.cornerRadius = 5.0
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = true
        self.tableView.layer.cornerRadius = 5.0
        self.tableView.layer.borderWidth = 2.0
        self.tableView.layer.borderColor = K.Color.primaryColor.cgColor
        self.tableView.flashScrollIndicators()
        self.tableView.register(UINib(nibName: "DropDownTableViewCell", bundle: .main), forCellReuseIdentifier: "DropDownTableViewCell")
        self.pointsEarnedByModeLabel.isHidden = true
        
        
    }
    
    @IBAction func CarpoolingTapped(_ sender: UIButton) {
        if carpoolingChecked {
            carpoolingCheckBoxImageView.image = UIImage(named: "ic_check_box_outline_blank")
            carpoolingChecked = !carpoolingChecked
            delegate?.getMode(mode: NSLocalizedString("car", comment: ""),row: sender.tag)
        } else {
            carpoolingCheckBoxImageView.image = UIImage(named: "ic_check_box")
            delegate?.getMode(mode: NSLocalizedString("carpooling", comment: ""),row:sender.tag)
            carpoolingChecked = !carpoolingChecked
        }
    }
    @IBAction func dropDownButtonTapped(_ sender: Any) {
        if tableView.isHidden {
            animate(toogle: true)
        } else {
            animate(toogle: false)
        }
    }
    func animate(toogle: Bool) {
        
        
        if toogle {
            self.buttonView.layer.cornerRadius = 0.0
            self.buttonView.roundCorners([.topRight, .topLeft], radius: 5.0)
            startTimerForShowScrollIndicator()
            UIView.animate(withDuration: 0.3) {
                self.tableView.isHidden = false
            }
            self.delegate?.didToggle()
            
        } else {
            UIView.animate(withDuration: 0.3) {
                self.tableView.isHidden = true
                self.stopTimerForShowScrollIndicator()
                self.buttonView.layer.cornerRadius = 5.0
            }
        }
        
    }
    func getIndexPath() -> IndexPath? {
        var indexPath: IndexPath?
        guard let superView = self.superview as? UITableView else {
            print("superview is not a UITableView - getIndexPath")
            return nil
        }
        indexPath = superView.indexPath(for: self)
        return indexPath
    }
    @objc func showScrollIndicatorsInContacts() {
        UIView.animate(withDuration: 0.001) {
            self.tableView.flashScrollIndicators()
        }
    }

    /// Start timer for always show scroll indicator in table view
    func startTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.showScrollIndicatorsInContacts), userInfo: nil, repeats: true)
    }

    /// Stop timer for always show scroll indicator in table view
    func stopTimerForShowScrollIndicator() {
        self.timerForShowScrollIndicator?.invalidate()
        self.timerForShowScrollIndicator = nil
    }
}
extension SectionModeTableViewCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transportList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableViewCell", for: indexPath) as! DropDownTableViewCell
        let sortedTransportList = transportList.sorted(by: <)
        cell.tranportLabelInCell.text = sortedTransportList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sortedTransportList = transportList.sorted(by: <)
        transportLabel.text = sortedTransportList[indexPath.row]
        let row = getIndexPath()?.row
        delegate?.getMode(mode: transportLabel.text!,row: row!)
        if(transportLabel.text == NSLocalizedString("car", comment: "")) {
            carpoolingView.isHidden = false
            delegate?.didTappedOnrow(row: row!, didSelectCar : true)
        } else {
            carpoolingView.isHidden = true
            delegate?.didTappedOnrow(row: row!, didSelectCar : false)
        }
        
        animate(toogle: false)
        
    }
    
    
}
extension UIView {

    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         self.layer.mask = mask
    }

}

