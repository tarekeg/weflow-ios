//
//  ReusableFunctions.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 13/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import Foundation
import CommonCrypto
import SwiftyJSON
import Auth0
import SwiftUI


class ReusableFunctions {
    
    static var proPoint = 0
    static var standartPoint = 0
    
    
    func formatDecimals(numberString : String) -> Double {
        let formatter = NumberFormatter()
        if numberString.contains("."){
            formatter.decimalSeparator = "."
            let grade = formatter.number(from: numberString)
            
            if let doubleGrade = grade?.doubleValue {
                return doubleGrade
            } else {
                return 0.0
            }
        } else {
            formatter.decimalSeparator = ","
            let grade = formatter.number(from: numberString)
            
            if let doubleGrade = grade?.doubleValue {
                return doubleGrade
            } else {
                return 0.0
            }
        }
    }
    
    func formatDistance (_ distanceInMeter : Double) -> String {
               if (distanceInMeter > 1000) {
                return Double(String(format: "%.1f", distanceInMeter/1000))!.clean
               } else {
                return Double(String(format: "%.3f", distanceInMeter/1000))!.clean
               }
           }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func formatDuration (_ duration : Double) -> String {
        
        let intDuration = Int(duration)
        let (h,m,s) = secondsToHoursMinutesSeconds(seconds: intDuration)
        if(h == 0) {
            return String(m) + " min"
        } else if(m == 0) {
            return String(s) + " sec"
        } else {
            return String(h) + " h " + String(m) + " min"
        }
    }
    func formatDuration (_ startTs : Double, _ endTs : Double) -> String {
        let diff = endTs - startTs
        let intDiff = Int(diff)
        let (h,m,s) = secondsToHoursMinutesSeconds(seconds: intDiff)
        if(h == 0) {
            return String(m) + " min"
        } else if(m == 0) {
            return String(s) + " sec"
        } else {
            return String(h) + " h " + String(m) + " min"
        }
    }
    func formatStringToDate (strDate : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "locale")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        let date = dateFormatter.date(from:strDate)!
        return date
    }
    func formatStringFromPopupToDate (strDate : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "locale")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from:strDate)!
        return date
    }
    func getCurrentDate () -> String {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd"
        let formattedDate = format.string(from: date)
        return formattedDate
    }
    func getCurrentDateForHeader (date: Date) -> String {
        let format = DateFormatter()
        format.dateFormat = "EEEE d MMMM"
        let formattedDate = format.string(from: date)
        return formattedDate
    }
    func formatDate(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE d MMMM - HH'H'mm"
        
        if let formattedDate = dateFormatterGet.date(from: date) {
            return dateFormatterPrint.string(from: formattedDate)
        } else {
            return "dd mm yyyy"
        }
        
    }
    
    func formatDateFromTimeStamp(ts : Double) -> String {
        let date = Date(timeIntervalSince1970: ts)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone =  TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "EEEE d MMMM - HH'H'mm"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    func longFormatDateFromTimeStamp(ts : Double) -> String {
        let date = Date(timeIntervalSince1970: ts)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone =  TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    func formatStartTimeTripTs(ts : Double) -> String {
        let date = Date(timeIntervalSince1970: ts)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone =  TimeZone.current
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH'h'mm"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    func formatTimeStampToHeaderDate(ts : Double) -> String {
    let date = Date(timeIntervalSince1970: ts)
    let dateFormatter = DateFormatter()
    dateFormatter.timeZone =  TimeZone.current
    dateFormatter.locale = NSLocale.current
    dateFormatter.dateFormat = "EEEE d MMMM"
    let strDate = dateFormatter.string(from: date)
    return strDate
    }
    
    func formatStartTimeTrip(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "HH'h'mm"
        
        if let formattedDate = dateFormatterGet.date(from: date) {
            return dateFormatterPrint.string(from: formattedDate)
        } else {
            return "dd mm yyyy"
        }
        
    }
    func formatFrenchDate(strDate : String) -> String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy"
        dateFormatterPrint.timeZone = TimeZone.current
        dateFormatterPrint.locale = Locale.current
        
        if let formattedDate = dateFormatterGet.date(from: strDate) {
            return dateFormatterPrint.string(from: formattedDate)
        } else {
            return "dd mm yyyy"
        }
    }
    func formatEnglishDate(strDate : String) -> String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd-MM-yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        dateFormatterPrint.timeZone = TimeZone.current
        dateFormatterPrint.locale = Locale.current
        
        if let formattedDate = dateFormatterGet.date(from: strDate) {
            return dateFormatterPrint.string(from: formattedDate)
        } else {
            return "dd mm yyyy"
        }
    }
    
    
    func getAverage(array : [JSON]) -> Double {
        var doubleArray : [Double] = []
        if(!array.isEmpty) {
            for i in 0...array.count - 1 {
                doubleArray.append(array[i].doubleValue)
            }
            let sum = doubleArray.reduce(0, +)
            return (Double(sum) / Double(array.count)) * 3.6
        } else {
            return 0
        }
       
    }
    func renewIrebyToken(completion: @escaping(_ success: Bool) -> Void) {
        Auth0
            .authentication()
            .renew(withRefreshToken: UserDefaults.standard.string(forKey: "ireby_refresh_token")!)
            .start { result in
                switch(result) {
                case .success(let credentials):
                    // If you have Refresh Token Rotation enabled, you get a new Refresh Token
                    // Otherwise you only get a new Access Token
                    guard let _ = credentials.accessToken,
                          let _ = credentials.refreshToken else {
                        completion(false)
                        print("Error when renewing Token")
                        return
                    }
                    UserDefaults.standard.set(credentials.accessToken, forKey: "ireby_access_token")
                    UserDefaults.standard.set(credentials.refreshToken, forKey: "ireby_refresh_token")
                   
                    completion(true)
                case .failure(let error):
                    completion(false)
                print("My error =",error)
                }
        }
    }
    
    func syncIrebyApi( completion: @escaping(_ success: Bool) -> Void) {
        Auth0
            .webAuth()
            .scope("openid profile offline_access")
            .audience("https://preprod.ireby.pro/api")
            .start { result in
                switch result {
                case .failure(let error):
                    // Handle the error
                    print("Error: \(error)")
                case .success(let credentials):
                    // Do something with credentials e.g.: save them.
                    // Auth0 will automatically dismiss the hosted login page
                    print("Credentials: \(credentials)")
                    print("access_token_api =",credentials.accessToken)
                    UserDefaults.standard.set(credentials.accessToken, forKey: "ireby_access_token")
                    UserDefaults.standard.set(credentials.refreshToken, forKey: "ireby_refresh_token")
                    print("My token = ",credentials.accessToken)
                    print("refreshToken = ",credentials.refreshToken)
                    completion(true)
                }
                
            }
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func updateUIWefloNotSync (proView : UIView, standardView : UIView, buttonSync : UIButton, isSynced : Bool) {
        if isSynced {
            buttonSync.isHidden = true
            standardView.isHidden = false
            proView.isHidden = false
        } else {
            buttonSync.isHidden = false
            standardView.isHidden = true
            proView.isHidden = true
        }
    }
    
    func formatStringToDateToTS (strDate : String) -> Double {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "locale")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.date(from:strDate)!
        return date.timeIntervalSince1970
    }
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    func isPasswordValid(_ password: String) -> Bool {
        let passwordRegEx = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}$" //(Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number)
        
        let passwordPred = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordPred.evaluate(with: password)
    }
    
    func getTrips (startTs : Double, endTs : Double, completion: @escaping(_ success: Bool) -> Void)  {
        var section : [String : Any] = [:]
        var sections : [[String : Any]] = []
        var tripDatas : [TripData] = []
        var formattedSections : [Section] = []
        var trips : [Trip] = []
        var totalDistance = 0.0
        let session = URLSession(configuration: .default)
        let params = [
            "user" : UserDefaults.standard.string(forKey: "Token")!,
            "key_list" : [
                "analysis/recreated_location",
                "inference/prediction"
            ],
            "start_time" : startTs,
            "end_time" : endTs
        ] as [String : Any]
        
        let url = URL(string: K.Path.baseURL + "/datastreams/find_entries/timestamp")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session.dataTask(with: request as URLRequest,completionHandler:  {(data, response, error) -> Void in
            if let value = response?.expectedContentLength {
                guard let data = data else {
                    return
                }
                
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any]
                    let rawJson = JSON(json)
                    let rawData = rawJson["phone_data"].arrayValue
                    if !rawData.isEmpty {
                        let predictions = rawData.filter({ (json) -> Bool in
                                                            return json["metadata"]["key"].stringValue == "inference/prediction"; })
                        let recreatedLocations = rawData.filter({ (json) -> Bool in
                                                                    return json["metadata"]["key"].stringValue == "analysis/recreated_location"; })
                        let jsonRecreatedLocation = JSON(recreatedLocations)
                        let jsonPrediction = JSON(predictions)
                        DispatchQueue.main.async { [self] in
                            if let items = jsonRecreatedLocation.array {
                                for item in items {
                                    if let sectionId = item["data"]["section"]["$oid"].string {
                                        let tripData = TripData(sectionId: sectionId, speed: item["data"]["speed"].doubleValue, distance: item["data"]["distance"].doubleValue, location: Location(latitude: item["data"]["loc"]["coordinates"][1].doubleValue, longitude: item["data"]["loc"]["coordinates"][0].doubleValue))
                                        tripDatas.append(tripData)
                                    }
                                }
                            }
                            if let items = jsonPrediction.array {
                                for item in items {
                                    if let tripId = item["data"]["trip_id"]["$oid"].string {
                                        section["tripId"] = tripId
                                        section["sectionId"] = item["data"]["section_id"]["$oid"].stringValue
                                        section["end_ts"] = item["data"]["end_ts"].doubleValue
                                        section["start_ts"] = item["data"]["start_ts"].doubleValue
                                        section["mode"] = Array(item["data"]["predicted_mode_map"].dictionaryValue)[0].key
                                        sections.append(section)
                                    }
                                }
                            }
                            let groupedSection = Dictionary(grouping: tripDatas, by: { $0.sectionId })
                            for item in sections {
                                for (key,value) in groupedSection {
                                    if key == item["sectionId"] as! String {
                                        var speeds : [Double] = []
                                        var distances : [Double] = []
                                        for i in 0...value.count - 1 {
                                            speeds.append(value[i].speed)
                                            distances.append(value[i].distance)
                                        }
                                        let section = Section(tripId: item["tripId"] as! String, sectionId: item["sectionId"] as! String, mode: item["mode"] as! String, startTs: item["start_ts"] as! Double, endTs: item["end_ts"] as! Double, data: value,speeds: speeds,distances: distances)
                                        formattedSections.append(section)
                                    }
                                }
                            }
                            formattedSections.sort {
                                $0.startTs < $1.startTs
                            }
                                                        
                            let groupedTrip = Dictionary(grouping: formattedSections, by: {$0.tripId})
                            
                            for (key,value) in groupedTrip {
                                totalDistance = 0.0
                                for i in 0...value.count - 1 {
                                    totalDistance += value[i].distances.reduce(0, +)
                                }
                                print("Total Distance = ",totalDistance)
                                let trip = Trip(tripId: key, sections: value, totalDistance: totalDistance)
                                trips.append(trip)
                            }
                            trips.sort {
                                $0.sections[0].startTs < $1.sections[0].startTs
                            }
                        }
                        
                        
                    }
                    DispatchQueue.main.async { [self] in
                        let json = try! JSONEncoder().encode(trips)
                        UserTripData.userTrips = JSON(json)
                        completion(true)
                    }
                    
               
                }catch{
                    print(error)
                    print("erroMsg")
                    
                }
                
            }
        })
        
        task.resume()
    }
    
}

extension Double {
    var clean: String {
       return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

extension Date {
    static var yesterday: Date { return Date().dayBefore }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayBefore: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return dayAfter.month != month
    }
}
