//
//  TextFieldExtension.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 03/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

class CustomTextField:  UITextField {
    
    
    
    func setup () {
        self.layer.shadowColor = UIColorFromRGB(rgbValue: 0x707070).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
        self.backgroundColor = .white
    }
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    override init(frame: CGRect) {
           super.init(frame: frame)
           setup()
       }
       required public init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           setup()
       }
    
}
