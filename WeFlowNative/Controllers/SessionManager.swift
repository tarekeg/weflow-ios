//
//  SessionManager.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 11/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import Foundation
import SimpleKeychain
import Auth0
import Alamofire
import SwiftyJSON


@objc class SessionManager: NSObject {
    
    @objc static let shared = SessionManager()
    private let authentication = Auth0.authentication()
    var credentialsManager: CredentialsManager!
    var profile: UserInfo?
    @objc var credentials: Credentials?
    
    public override init () {
        self.credentialsManager = CredentialsManager(authentication: Auth0.authentication())
    }
    
    func retrieveProfile(_ callback: @escaping (Error?) -> ()) {
        guard let accessToken = self.credentials?.accessToken
        else { return callback(CredentialsManagerError.noCredentials) }
        self.authentication
            .userInfo(withAccessToken: accessToken)
            .start { [self] result in
                switch(result) {
                case .success(let profile):
                    print(profile.picture!)
                    let url = profile.picture
                    UserDefaults.standard.set(self.credentials?.accessToken!, forKey: "ireby_access_token")
                    UserDefaults.standard.set(url?.absoluteString, forKey: "pictureUrl")
                    UserDefaults.standard.set(profile.sub, forKey: "sub")
                    UserDefaults.standard.set(profile.email, forKey: "email")
                    UserDefaults.standard.set(self.credentials?.idToken!, forKey: "Token")
                    self.profile = profile
                    DispatchQueue.main.async {
                        self.isImageExistInProfile(token: (credentials?.idToken)!)
                    }
                    
                    callback(nil)
                case .failure(let error):
                    callback(error)
                }
            }
    }
    
    func isImageExistInProfile (token : String) {
        print(token)
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + token,
            "content-type": "application/json"
        ]
        AF.request(K.Path.cleverApi + "users", method: .get, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON {
            response in
            print(response.result)
            switch response.result {
            case .success(let value):
                if response.response?.statusCode == 200 {
                    
                    let jsonUser = JSON(value)
                    if jsonUser["picturePath"].stringValue != "" {
                        let url  = K.Path.cleverApi + "uploads/" + jsonUser["picturePath"].stringValue
                        UserDefaults.standard.set(url, forKey: "pictureUrl")
                    }
                }
            case .failure(let error):
                print(error)
            }
            
        }
    }
    
    func renewAuth(_ callback: @escaping (Error?) -> ()) {
        // Check it is possible to return credentials before asking for Touch
        guard self.credentialsManager.hasValid() else {
            return callback(CredentialsManagerError.noCredentials)
        }
        self.credentialsManager.credentials { error, credentials in
            guard error == nil, let credentials = credentials else {
                return callback(error)
            }
            self.credentials = credentials
            UserDefaults.standard.set(self.credentials?.accessToken!, forKey: "AccessToken")
            UserDefaults.standard.set(self.credentials?.idToken!, forKey: "Token")
            callback(nil)
        }
    }
    
    func logout(_ callback: @escaping (Error?) -> Void) {
        // Remove credentials from KeyChain
        self.credentials = nil
        self.credentialsManager.revoke(callback)
    }
    
    func store(credentials: Credentials) -> Bool {
        self.credentials = credentials
        // Store credentials in KeyChain
        return self.credentialsManager.store(credentials: credentials)
    }
}

func plistValues(bundle: Bundle) -> (clientId: String, domain: String)? {
    guard
        let path = bundle.path(forResource: "Auth0", ofType: "plist"),
        let values = NSDictionary(contentsOfFile: path) as? [String: Any]
    else {
        print("Missing Auth0.plist file with 'ClientId' and 'Domain' entries in main bundle!")
        return nil
    }
    
    guard
        let clientId = values["ClientId"] as? String,
        let domain = values["Domain"] as? String
    else {
        print("Auth0.plist file at \(path) is missing 'ClientId' and/or 'Domain' entries!")
        print("File currently has the following entries: \(values)")
        return nil
    }
    return (clientId: clientId, domain: domain)
}
