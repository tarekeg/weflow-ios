//
//  MyTextView.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 23/12/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import Foundation

class MyTextView : UITextView {
    
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {

        guard let pos = closestPosition(to: point) else { return false }

        guard let range = tokenizer.rangeEnclosingPosition(pos, with: .character, inDirection: .layout(.left)) else { return false }

        let startIndex = offset(from: beginningOfDocument, to: range.start)

        return attributedText.attribute(.link, at: startIndex, effectiveRange: nil) != nil
    }  
    
    
}
