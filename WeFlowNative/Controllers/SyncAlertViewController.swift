//
//  SyncAlertViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 19/04/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import SafariServices

protocol SyncAlertViewControllerDelegate {
    func getTag(tag : Int)
}

class SyncAlertViewController: UIViewController {
    
    @IBOutlet weak var bodyTextView: MyTextView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var crossView: UIView!
    
    
    var delegate : SyncAlertViewControllerDelegate?
    var buttonAction: (() -> Void)?
    
    let hyperlinks = ["ireby.pro":K.Path.baseURLWeb]
    

    override func viewDidLoad() {
        super.viewDidLoad()
//        bodyTextView.text = NSLocalizedString("textViewPopupDescription", comment: "")
//        bodyTextView.addHyperLinksToText(originalText: bodyTextView.text!, hyperLinks: hyperlinks)
        bodyTextView.textAlignment = .center
        
        bodyTextView.textColor = K.Color.grayThirtySix
        alertView.layer.cornerRadius = 10.0
        crossView.layer.cornerRadius = 10.0
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateTextFont(textView: bodyTextView)
    }
    
    @IBAction func syncToIrebyTapped(_ sender: UIButton) {
        self.dismiss(animated: true)
        if sender.tag == 0 {
            delegate?.getTag(tag: sender.tag)
            buttonAction?()
        } else {
            delegate?.getTag(tag: sender.tag)
            buttonAction?()
        }
        

    }
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

func updateTextFont(textView : UITextView) {
    if (textView.text.isEmpty || textView.bounds.size.equalTo(CGSize.zero)) {
        return;
    }

    let textViewSize = textView.frame.size;
    let fixedWidth = textViewSize.width;
    let expectSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT)));

    var expectFont = textView.font;
    if (expectSize.height > textViewSize.height) {
        while (textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height > textViewSize.height) {
            expectFont = textView.font!.withSize(textView.font!.pointSize - 1)
            textView.font = expectFont
        }
    }
    else {
        while (textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat(MAXFLOAT))).height < textViewSize.height) {
            expectFont = textView.font;
            textView.font = textView.font!.withSize(textView.font!.pointSize + 1)
        }
        textView.font = expectFont;
    }
}
