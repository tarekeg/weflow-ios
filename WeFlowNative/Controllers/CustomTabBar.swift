//
//  CustomTabBar.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 28/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

class CustomTabBar : UITabBar {
    
     static let height: CGFloat = 60

        override open func sizeThatFits(_ size: CGSize) -> CGSize {
            guard let window = UIApplication.shared.keyWindow else {
                return super.sizeThatFits(size)
            }
            var sizeThatFits = super.sizeThatFits(size)
            if #available(iOS 11.0, *) {
                sizeThatFits.height = CustomTabBar.self.height + window.safeAreaInsets.bottom
            } else {
                sizeThatFits.height = CustomTabBar.self.height
            }
            if #available(iOS 15.0, *) {
                let appearance = UITabBarAppearance()
                appearance.configureWithOpaqueBackground()
                appearance.backgroundColor = .white
                standardAppearance = appearance
                scrollEdgeAppearance = standardAppearance
            }
            
            return sizeThatFits
        }
}
