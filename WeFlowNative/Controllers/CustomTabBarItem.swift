//
//  CustomTabBarItem.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 28/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

@IBDesignable
class CustomTabBarItem: UITabBarItem {
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    func setup() {
        let font = UIFont(name: "Avenir-Roman", size: 11)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font!,
        ]
        self.setTitleTextAttributes(attributes, for: .normal)
        self.setTitleTextAttributes(attributes, for: .selected)

        if let image = image {
            self.image = image.withRenderingMode(.alwaysOriginal)
        }
        if let image = selectedImage {
            selectedImage = image.withRenderingMode(.alwaysOriginal)
        }
    }
}
