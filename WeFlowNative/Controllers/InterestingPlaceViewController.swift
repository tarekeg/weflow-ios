//
//  AlertViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 14/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

protocol InterestingPlaceViewControllerDelegate {
    func getPlace(place : String)
}

class InterestingPlaceViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var crossView: UIView!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var interestingPlaceLabel: UILabel!
    
    var delegate : InterestingPlaceViewControllerDelegate?
    
    var alertTitle = String()
    
    var alertBody = String()
    
    var alertActionButtonTitle = String()
    
    var buttonAction: (() -> Void)?
    
    var state = false
    
    var timerForShowScrollIndicator: Timer?
    
    var interstingLocation = [NSLocalizedString("bus-stop", comment: ""),NSLocalizedString("parking", comment: ""),NSLocalizedString("carpool-area", comment: "")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
      
    }
    
    func setupView () {
        crossView.layer.cornerRadius = 10.0
        
        alertView.layer.cornerRadius = 10.0
        
        titleLabel.text = alertTitle
        
        bodyLabel.text = alertBody
        
        actionButton.setTitle(alertActionButtonTitle, for: .normal)
        
        buttonView.layer.cornerRadius = 5.0
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.isHidden = true
        self.tableView.layer.cornerRadius = 5.0
        self.tableView.layer.borderWidth = 2.0
        self.tableView.layer.borderColor = K.Color.primaryColor.cgColor
        self.tableView.flashScrollIndicators()
        self.tableView.register(UINib(nibName: "DropDownTableViewCell", bundle: .main), forCellReuseIdentifier: "DropDownTableViewCell")
        
        self.interestingPlaceLabel.text = NSLocalizedString("actual-location", comment: "")
        
        
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        
        self.dismiss(animated: true)
        
    }
    @IBAction func actionButtonTapped(_ sender: Any) {
//        dismiss(animated: true)
        
        buttonAction?()
    }
    
    @IBAction func checkBoxButtonTapped(_ sender: Any) {
        state = !state
        if state {
            checkBoxImageView.image = UIImage(named: "ic_check_box")
            Point.popupStateDisabled = true
        } else {
            checkBoxImageView.image = UIImage(named: "ic_check_box_outline_blank")
            Point.popupStateDisabled = false
        }
    }
    
    @IBAction func dropDownButtonTapped(_ sender: Any) {
        
        if tableView.isHidden {
            animate(toogle: true)
        } else {
            animate(toogle: false)
        }
    }
    func animate(toogle: Bool) {
        
        
        if toogle {
            
            self.buttonView.layer.cornerRadius = 0.0
            self.buttonView.roundCorners([.topRight, .topLeft], radius: 5.0)
            UIView.animate(withDuration: 0.0) {
                self.tableView.isHidden = false
            }
//            self.delegate?.didToggle()
            
        } else {
            UIView.animate(withDuration: 0.0) {
                self.tableView.isHidden = true
                self.buttonView.layer.cornerRadius = 5.0
            }
        }
        
    }
    
   

 
        
 
    

}

extension InterestingPlaceViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return interstingLocation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableViewCell", for: indexPath) as! DropDownTableViewCell
        let sortedTransportList = interstingLocation.sorted(by: <)
        cell.tranportLabelInCell.text = sortedTransportList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sortedTransportList = interstingLocation.sorted(by: <)
        interestingPlaceLabel.text = sortedTransportList[indexPath.row]
//        let row = getIndexPath()?.row
        delegate?.getPlace(place: interestingPlaceLabel.text!)
//        delegate?.didTappedOnrow(row: row!)
        animate(toogle: false)
    }
    
    
}
