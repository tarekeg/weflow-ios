//
//  AlertViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 14/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit



class AlertViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var crossView: UIView!
    
    var alertTitle = String()
    
    var alertBody = String()
    
    var alertActionButtonTitle = String()
    
    var buttonAction: (() -> Void)?
    
    var state = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
      
    }
    
    func setupView () {
        crossView.layer.cornerRadius = 10.0
        
        alertView.layer.cornerRadius = 10.0
        
        titleLabel.text = alertTitle
        
        bodyLabel.text = alertBody
        
        actionButton.setTitle(alertActionButtonTitle, for: .normal)
        
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        
        self.dismiss(animated: true)
        
    }
    @IBAction func actionButtonTapped(_ sender: Any) {
        dismiss(animated: true)
        
        buttonAction?()
    }
    
    @IBAction func checkBoxButtonTapped(_ sender: Any) {
        state = !state
        if state {
            checkBoxImageView.image = UIImage(named: "ic_check_box")
            Point.popupStateDisabled = true
        } else {
            checkBoxImageView.image = UIImage(named: "ic_check_box_outline_blank")
            Point.popupStateDisabled = false
        }
    }
    

}
