//
//  AlertService.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 14/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

protocol AlertServiceDelegate {
    func sendPlace(place : String)
    
}

protocol AlertSyncDelegate {
    func sendTag(tag : Int)
}

protocol AlertResetDelegate {
    func sendResult(bool : Bool)
}

protocol AlertCompanyDelegate {
    func sendCompany(company : String, companyCode : String, img : URL)
}



class AlertService: InterestingPlaceViewControllerDelegate, SyncAlertViewControllerDelegate, AlertResetPasswordViewControllerDelegate, AlertCompanyCodeViewControllerDelegate {
    func sendCompany(company: String, companyCode : String, img : URL) {
        companyDelegate?.sendCompany(company: company,companyCode : companyCode, img : img)
    }
    
    func isResetEmailSent(bool: Bool) {
        resetDelegate?.sendResult(bool: bool)
    }
    
    func getTag(tag: Int) {
        syncDelegate?.sendTag(tag: tag)
    }
    
    func getPlace(place: String) {
        delegate?.sendPlace(place: place)
    }
    
    
    var interestingPlace = ""
    var delegate : AlertServiceDelegate?
    
    var syncDelegate : AlertSyncDelegate?
    
    var resetDelegate : AlertResetDelegate?
    
    var companyDelegate : AlertCompanyDelegate?
    
    func alert(title : String, body : String, buttonTitle : String, completion: @escaping() -> Void) -> AlertViewController {
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertVC") as! AlertViewController
        
        alertVC.alertTitle = title
        
        alertVC.alertBody = body
        
        alertVC.alertActionButtonTitle = buttonTitle
        
        alertVC.buttonAction = completion
        
        return alertVC
        
    }
    
    func alertWithOption(title : String, body : String, buttonTitle : String, dismissTitle : String, completion: @escaping() -> Void) -> AlertWithSecondButtonViewController {
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertWithSecondButtonVC") as! AlertWithSecondButtonViewController
        
        alertVC.alertTitle = title
        
        alertVC.alertBody = body
        
        alertVC.alertActionDismissButtonTitle = dismissTitle
        
        alertVC.alertActionButtonTitle = buttonTitle
        
        alertVC.buttonAction = completion
        
        return alertVC
        
    }
    
    func alertWithCheckBox(title : String, body : String, buttonTitle : String, completion: @escaping() -> Void) -> AlertViewController {
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertVCCheckBox") as! AlertViewController
        
        alertVC.alertTitle = title
        
        alertVC.alertBody = body
        
        alertVC.alertActionButtonTitle = buttonTitle
                
        alertVC.buttonAction = completion
        
        return alertVC
        
    }
    
    func alertOptionWithInterest(title : String, body : String, buttonTitle : String, completion : @escaping() -> Void) -> InterestingPlaceViewController {
        
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertPointOfInterest") as! InterestingPlaceViewController
                
        alertVC.alertTitle = title
        
        alertVC.delegate = self
        
        alertVC.alertBody = body
        
        alertVC.alertActionButtonTitle = buttonTitle
        
        alertVC.buttonAction = completion
    
        return alertVC
    }
    
    func showSyncAlert(completion : @escaping() -> Void) -> SyncAlertViewController {
        
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "SyncAlertViewController") as! SyncAlertViewController
        
        alertVC.delegate = self
        
        alertVC.buttonAction = completion
                        
        return alertVC
    }
    func alertResetPassword() -> AlertResetPasswordViewController {
        
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertResetPassword") as! AlertResetPasswordViewController
                
        alertVC.delegate = self
        
        return alertVC
    }
    
    func alertResetPasswordSuccess() -> AlertResetPasswordSuccessViewController {
        
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertResetPasswordSuccess") as! AlertResetPasswordSuccessViewController
                        
        return alertVC
    }
    func alertCompanyCode() -> AlertCompanyCodeViewController {
        
        
        let storyboard = UIStoryboard(name: "Alert", bundle: .main)
        
        let alertVC = storyboard.instantiateViewController(withIdentifier: "AlertCodeEntreprise") as! AlertCompanyCodeViewController
        
        alertVC.delegate = self
            
        return alertVC
    }
    
    
}
