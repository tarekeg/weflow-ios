//
//  MyAnnotationMap.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 21/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import Foundation
import MapKit

class MyAnnotationMap: NSObject, MKAnnotation {
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    var image: UIImage? = nil

    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
//        self.image
        super.init()
    }
}
