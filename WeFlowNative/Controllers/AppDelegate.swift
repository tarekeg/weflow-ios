//
//  AppDelegate.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 09/07/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import CoreData
import Auth0
import Firebase
import Parse
import IQKeyboardManagerSwift
import Siren
import Sentry
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let credentialsManager = CredentialsManager(authentication: Auth0.authentication())
    let serviceAlert = AlertService()
    let gcmMessageIDKey = "gcm.message_id"
    
    private let locationManager = CLLocationManager()
    
    func hyperCriticalRulesExample() {
        let siren = Siren.shared
        siren.rulesManager = RulesManager(globalRules: .critical,
                                          showAlertAfterCurrentVersionHasBeenReleasedForDays: 0)
        siren.apiManager = APIManager(countryCode: "FR")
        siren.wail { results in
            print("Result = ",results)
            switch results {
            case .success(let updateResults):
                print("AlertAction ", updateResults.alertAction)
                print("Localization ", updateResults.localization)
                print("Model ", updateResults.model)
                print("UpdateType ", updateResults.updateType)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        FirebaseApp.configure()
        SentrySDK.start { options in
            options.dsn = "https://d05b6959dd164ee3896706f5d489dc8b@o427026.ingest.sentry.io/5936922"
            options.debug = false // Enabled debug when first installing is always helpful
            options.environment = "Dev"
        }
        // Override point for customization after application launch.
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        if BEMServerSyncConfigManager.instance().ios_use_remote_push {
            // NOP - this is handled in javascript
        } else {
            applySync()
        }
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "OK"
        IQKeyboardManager.shared.toolbarTintColor = UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00)
        IQKeyboardManager.shared.enable = true
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let onboardingStoryboard = UIStoryboard(name: "Onboarding", bundle: nil)
        let permissionStoryboard = UIStoryboard(name: "Permission", bundle: nil)
        if credentialsManager.hasValid() && UserDefaults.standard.string(forKey: "Token") != nil && UserDefaults.standard.string(forKey: "hasStartedSignIn") == nil {
            if !isLocationEnabled() {
                let vc = permissionStoryboard.instantiateViewController(withIdentifier: "locationPermissionViewController")
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            }
            if !isInitEnabled() {
                let vc = permissionStoryboard.instantiateViewController(withIdentifier: "motionActivityPermissionViewController")
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            }
            if isLocationEnabled() && isInitEnabled() {
                if UserTripData.userTrips ==  [] {
                    window!.rootViewController = storyboard.instantiateViewController(withIdentifier: "progressDownloadView")
                } else {
                    window!.rootViewController = storyboard.instantiateViewController(withIdentifier: "tabBarController")
                }
                
            }
            
        } else if credentialsManager.hasValid() && UserDefaults.standard.string(forKey: "Token") != nil && UserDefaults.standard.string(forKey: "hasStartedSignIn") == "startedSync"  {
            window!.rootViewController = UIStoryboard(name: "Synchronize", bundle: nil).instantiateViewController(withIdentifier: "irebySyncController")
        } else {
            window!.rootViewController = onboardingStoryboard.instantiateViewController(withIdentifier: "firstNavController")
        }
        window!.makeKeyAndVisible()
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        if credentialsManager.hasValid() && UserDefaults.standard.string(forKey: "Token") != nil && UserDefaults.standard.string(forKey: "hasStartedSignIn") == nil {
            if !isLocationEnabled() {
                let storyboard = UIStoryboard(name: "Permission", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "locationPermissionViewController")
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            }
            if !isInitEnabled() {
                let storyboard = UIStoryboard(name: "Permission", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "motionActivityPermissionViewController")
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
            }
            
        }
    }
    
    func isLocationEnabled () -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            if #available(iOS 14.0, *) {
                switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return false
                }
            } else {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    return false
                case .authorizedAlways:
                    return true
                @unknown default:
                    return true
                }
            }
        } else {
            return false
        }
        
    }
    func isInitEnabled() -> Bool {
        print("Auth status Actvity manager = ",CMMotionActivityManager.authorizationStatus().rawValue)
        if CMMotionActivityManager.authorizationStatus().rawValue != 3  {
            return false
        } else {
            return true
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        
    }
    static func registerFornotification() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
    }
    // MARK: DidReceiveRemoteNotfication
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
    }
    
    // MARK: DidRegisterForRemoteNotificationWithDeviceToken
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        let token = tokenParts.joined()
        // 2. Print device token to use for PNs payloads
        print("Device Token: \(token)")
        // 3. Save the token to local storeage and post to app server to generate Push Notification. ...
    }
    
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "WeFlowNative")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([[.alert, .sound, .badge]])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
    func applySync() {
        if BEMServerSyncConfigManager.instance().ios_use_remote_push {
            let channel = "interval_\(NSNumber(value: BEMServerSyncConfigManager.instance().sync_interval))"
            let currentInstallation = PFInstallation.current()
            currentInstallation?.remove(forKey: "channels")
            currentInstallation?.addUniqueObject(channel, forKey: "channels")
            LocalNotificationManager.addNotification(
                "For remotePush, setting channel = \(channel)",
                showUI: true)
        } else {
            UIApplication.shared.setMinimumBackgroundFetchInterval(Double(BEMServerSyncConfigManager.instance().sync_interval))
            let currentInstallation = PFInstallation.current()
            currentInstallation?.remove(forKey: "channels")
        }
    }
    
}
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        let dataDict:[String: String?] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        
    }
}
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
