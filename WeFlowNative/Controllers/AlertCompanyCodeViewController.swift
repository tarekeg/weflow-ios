//
//  AlertCompanyCodeViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 02/06/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol AlertCompanyCodeViewControllerDelegate {
    func sendCompany(company : String, companyCode : String, img : URL)
}

class AlertCompanyCodeViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var crossView: UIView!
    @IBOutlet weak var companyCodeTextField: CustomTextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var confirmButton: LoadingButton!
    
    
    var imgUrl : URL!
    var companyName = String()
    var password = String()
    var buttonAction: (() -> Void)?
    var delegate : AlertCompanyCodeViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmButton.isEnabled = false
        confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        companyCodeTextField.delegate = self
        companyCodeTextField.addTarget(self, action: #selector(AlertCompanyCodeViewController.textFieldDidChange(_:)), for: .editingChanged)
        errorLabel.isHidden = true
        alertView.layer.cornerRadius = 10.0
        crossView.layer.cornerRadius = 10.0
        
    }
    
    
    @IBAction func confirmTapped(_ sender: Any) {
        confirmButton.showLoading()
        getCompanies(companyCode: companyCodeTextField.text!) { [self] success in
            if success {
                
                DispatchQueue.main.async {
                    delegate?.sendCompany(company: companyName, companyCode : companyCodeTextField.text!, img : imgUrl)
                    self.dismiss(animated: true) {
                        confirmButton.hideLoading()
                    }
                }
            } else {
                confirmButton.hideLoading()
                confirmButton.isEnabled = false
                confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
                companyCodeTextField.text = ""
                errorLabel.isHidden = false
            }
        }
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss(animated: true)
    }
    
    
    func getCompanies(companyCode : String , completion: @escaping(_ success: Bool) -> Void)  {
        
        let headers = [
            "content-type": "application/json",
            "token": UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "Authorization": "Bearer " + K.Constant.irebyBearer
        ]
        let parameters = [
            "company_code": companyCodeTextField.text!
        ] as [String : Any]
        
        
        let request = NSMutableURLRequest(url: NSURL(string: K.Path.baseURLWeb + "/api/pve/companies/is-weflo")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print("error serialization", error)
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async {
                if (error != nil) {
                    completion(false)
                    self.errorLabel.text = NSLocalizedString("error-company-name", comment: "")
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    let statusCode = httpResponse?.statusCode
                    
                    if statusCode == 404 {
                        completion(false)
                        self.errorLabel.text = NSLocalizedString("incorrect-company-name", comment: "")
                    }
                    if statusCode == 200 {
                        completion(true)
                        if let safeData = data {
                            let jsonResponse = JSON(safeData)
                            if NSData(contentsOf: jsonResponse["img"].url!) != nil {
                                self.imgUrl = jsonResponse["img"].url
                            } else {
                                self.imgUrl = URL(string: "https://demofree.sirv.com/nope-not-here.jpg")
                            }
                            self.companyName = jsonResponse["name"].stringValue
                            print(jsonResponse["name"].stringValue)
                            print(jsonResponse["img"].stringValue)
                        } else {
                            self.imgUrl = URL(string: "https://demofree.sirv.com/nope-not-here.jpg")
                        }
                        
                    }
                }
            }
        })
        
        dataTask.resume()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        errorLabel.isHidden = true
        if companyCodeTextField.text?.isEmpty == false {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        } else {
            confirmButton.isEnabled = false
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
        
    }
    
    
}
