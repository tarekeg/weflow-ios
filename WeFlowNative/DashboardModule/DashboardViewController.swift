//
//  DashboardViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 27/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import MapKit
import Charts
import Kingfisher
import SwiftyJSON
import Alamofire
import Reachability
import Auth0
import WebKit
import SafariServices

class DashboardViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UIScrollViewDelegate, AlertServiceDelegate, AlertSyncDelegate  {
    func sendPlace(place: String) {
        print(place)
        interestingPlace = place
    }
    func sendTag(tag: Int) {
        buttonTag = tag
    }
    

    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centeredLineView: UIView!
    @IBOutlet weak var lineWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var lineHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var lastTripDetailLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var planeCheckBoxImageView: UIImageView!
    @IBOutlet weak var trainCheckBoxImageView: UIImageView!
    @IBOutlet weak var carCheckBoxImageView: UIImageView!
    
    @IBOutlet weak var walkCheckBoxImageView: UIImageView!
    @IBOutlet weak var bikeCheckBoxImageView: UIImageView!
    @IBOutlet weak var subwayCheckBoxImageView: UIImageView!
   
    @IBOutlet weak var busCheckBoxImageView: UIImageView!
    @IBOutlet weak var carpoolingCheckBoxImageView: UIImageView!
    @IBOutlet weak var tramCheckBoxImageView: UIImageView!
   
    @IBOutlet weak var motoCheckBoxImageView: UIImageView!
    @IBOutlet weak var escooterCheckBoxImageView: UIImageView!
    @IBOutlet weak var otherCheckBoxImageView: UIImageView!
    
    @IBOutlet weak var planeButton: UIButton!
    @IBOutlet weak var trainButton: UIButton!
    @IBOutlet weak var carButton: UIButton!
    
    @IBOutlet weak var walkButton: UIButton!
    @IBOutlet weak var bikeButton: UIButton!
    @IBOutlet weak var subwayButton: UIButton!
    
    @IBOutlet weak var busButton: UIButton!
    @IBOutlet weak var carpoolingButton: UIButton!
    @IBOutlet weak var tramButton: UIButton!
    
    @IBOutlet weak var motoButton: UIButton!
    @IBOutlet weak var escooterButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var noTripsView: UIView!
    @IBOutlet weak var textView: UIView!
    
    
    
    
    
    @IBOutlet weak var startNewTripButton: UIButton!
    @IBOutlet weak var stoptripSquare: UIView!
    @IBOutlet weak var onGoinfTripContainerView: UIView!
    @IBOutlet weak var startStopContainerView: UIView!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var standardPointLabel: UILabel!
    @IBOutlet weak var proPointLabel: UILabel!
    @IBOutlet weak var totalKilometers: UILabel!
    @IBOutlet weak var totalTrips: UILabel!
   
    @IBOutlet weak var emptyMessageLabel: UILabel!
    @IBOutlet weak var standardPointsView: CustomView!
    @IBOutlet weak var proPointsView: CustomView!
    @IBOutlet weak var tripsLabel: UILabel!
    @IBOutlet weak var scrollToDownButton: UIButton!
    @IBOutlet weak var trackingStatusWaitingForTripLabel: UILabel!
    @IBOutlet weak var trackingSwitch: UISwitch!
    @IBOutlet weak var trackingStatusOnGoingTrip: UILabel!
    @IBOutlet weak var calLabel: UILabel!
    @IBOutlet weak var co2Label: UILabel!
    @IBOutlet weak var co2ImageView: UIImageView!
    @IBOutlet weak var co2InfoLabel: UILabel!
    @IBOutlet weak var calInfoLabel: UILabel!
    @IBOutlet weak var onGoingTrakcingSwitch: UISwitch!
    @IBOutlet weak var stackViewToButtomConstraint: NSLayoutConstraint!
    @IBOutlet weak var hebdoQuestionView: CustomView!
    @IBOutlet weak var syncButton: UIButton!
    
    
    
    @objc let idToken = SessionManager.shared.credentials?.idToken
    let tripDiaryStateMachine = TripDiaryStateMachine.instance()
    let locTracking = LocationTrackingConfig()
    var locationManager = CLLocationManager()
    var entries : [PieChartDataEntry] = Array()
    
    var planeChecboxChecked = false
    var trainChecboxChecked = false
    var carChecboxChecked = false
    
    var walkChecboxChecked = false
    var bikeChecboxChecked = false
    var subwayChecboxChecked = false
    
    var busChecboxChecked = false
    var carpoolingCheckBoxChecked = false
    var tramChecboxChecked = false
    
    var motoCheckBoxChecked = false
    var escooterCheckBoxChecked = false
    var otherCheckBoxChecked = false
    
    var errorChecboxChecked = false
    
    var totalGraphDistance = 0.0
    
    let airplaneImage = UIImage(named: "round_flight_black_24pt")
    let trainImage = UIImage(named: "round_train_black_24pt")
    let carImage = UIImage(named: "round_directions_car_black_24pt")
    
    let walkImage = UIImage(named: "round_directions_walk_black_24pt")
    let bikeImage = UIImage(named: "round_directions_bike_black_24pt")
    let subwayImage = UIImage(named: "round_subway_black_24pt")
    
    let busImage = UIImage(named: "round_directions_bus_black_24pt")
    let carpoolingImage = UIImage(named: "carpooling-white")
    let tramImage = UIImage(named: "round_tram_black_24pt")
    
    let motoImage = UIImage(named: "motorbike-white")
    let escooterImage = UIImage(named: "escooter-white")
    let otherImage = UIImage(named: "other-white")
    

    
  
    
    let RF = ReusableFunctions()
    var jsonMode : JSON = []
    var tripDays : JSON = []
    var sections : [JSON] = []
    var result : String = ""
    var location : [CLLocationCoordinate2D] = []
    var polylines : [MKPolyline] = []
    let refreshControl = UIRefreshControl()
    let serviceAlert = AlertService()
    var modeNumber = 0
    var hasreachedButtom = false
    var interestingPlace = ""
    var userProgFid = ""
    var buttonTag : Int?
    var weightIsHere : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tabBarController?.delegate = self
        setStateTrackingSwitch()
        if !RF.isKeyPresentInUserDefaults(key: "ireby_access_token") {
            print("ireby token does not exist")
        } else {
            getStatsPoints()
        }
        dotView.layer.sublayers?.removeAll()
        mapView.removeOverlays(polylines)
        let resource = ImageResource(downloadURL: URL(string : UserDefaults.standard.string(forKey: "pictureUrl")!)!, cacheKey: "my_avatar")
        profileImageButton.kf.setBackgroundImage(with: resource, for: .normal)
        getCurrentState(didPullToRefresh: false)
        getStats(filter: "?days=5")
        segmentedControl.selectedSegmentIndex = 0 
        NotificationCenter.default.addObserver(self, selector: #selector(getCurrentState), name: UIApplication.willEnterForegroundNotification, object: nil)
        segmentedControl.font(name: "Avenir-Roman", size: 13)
        if #available(iOS 13.0, *) {
            fixBackgroundSegmentControl(segmentedControl)
        } else {
            let color = K.Color.graySeven
            let font = UIFont(name: "Avenir-Roman", size: 10)
            let attributes: [NSAttributedString.Key: Any] = [
                .font: font!,
                .foregroundColor: color
            ]
            segmentedControl.tintColor = .lightGray
            segmentedControl.setTitleTextAttributes(attributes, for: .normal)
          
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.layer.cornerRadius = 10.0
        noTripsView.isHidden = true
        serviceAlert.syncDelegate = self
        SyncUserProgFid()
        serviceAlert.delegate = self
        scrollView.delegate = self
        dotView.layer.sublayers?.removeAll()
        mapView.delegate = self
        emptyMessageLabel.isHidden = true
        self.tabBarController?.delegate = self
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        scrollView.addSubview(refreshControl)
        locationManager.delegate = self
        setArrayPieChart()
        setConfig()
        setupPieChart()
        TripDiaryStateMachine.init()
        BEMTransitionNotifier.init()
        setDesign()
        getAccuracyOption()
        if(UserDefaults.standard.bool(forKey: "hasReachedBottom")) {
            scrollToDownButton.isHidden = true
        }
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NotificationCenter.default.removeObserver(UIApplication.willEnterForegroundNotification)
        if dotView.layer.sublayers?.count != 0 {
            dotView.layer.sublayers?.removeAll()
            mapView.removeOverlays(polylines)
            polylines.removeAll()
        }
        
    }
 
    
    func SyncUserProgFid () {
            getUserProgFid()
    }
  
    @IBAction func syncTapped(_ sender: Any) {
        let alertVC = serviceAlert.showSyncAlert() { [self] in
            if(buttonTag == 0) {
                self.performSegue(withIdentifier: "toSyncInApp", sender: nil)
            } else {
                self.performSegue(withIdentifier: "toSignInInApp", sender: nil)
            }
        }
        tabBarController!.present(alertVC, animated: true)
    }


    
    func setStateTrackingSwitch () {
        let currstate = TripDiaryStateMachine.getStateName(tripDiaryStateMachine!.currState)
        if(currstate != "STATE_TRACKING_STOPPED"){
            trackingSwitch.setOn(true, animated: false)
            onGoingTrakcingSwitch.setOn(true, animated: false)
            trackingStatusOnGoingTrip.text = NSLocalizedString("tracking-enabled",comment : "")
            trackingStatusWaitingForTripLabel.text = NSLocalizedString("tracking-enabled",comment : "")
            
        } else {
            trackingSwitch.setOn(false, animated: false)
            onGoingTrakcingSwitch.setOn(false, animated: false)
            trackingStatusOnGoingTrip.text = NSLocalizedString("tracking-disabled",comment : "")
            trackingStatusWaitingForTripLabel.text = NSLocalizedString("tracking-disabled",comment : "")
        }
    }
    
    @IBAction func startStopTracking(_ sender: Any) {
        let currstate = TripDiaryStateMachine.getStateName(tripDiaryStateMachine!.currState)
        if(currstate == "STATE_TRACKING_STOPPED") {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CFCTransitionNotificationName), object: CFCTransitionStartTracking)
            trackingStatusOnGoingTrip.text = NSLocalizedString("tracking-enabled",comment : "")
            trackingSwitch.setOn(true, animated: false)
            onGoingTrakcingSwitch.setOn(true, animated: false)
            trackingStatusOnGoingTrip.text = NSLocalizedString("tracking-enabled",comment : "")
            trackingStatusWaitingForTripLabel.text = NSLocalizedString("tracking-enabled",comment : "")
        } else {
            if currstate == "STATE_ONGOING_TRIP" {
                lastTripDetailLabel.text = NSLocalizedString("onload",comment : "")
                showWaitingForTripToStart()
                getLastTrip(didpullToRefresh: false)
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: CFCTransitionNotificationName), object: CFCTransitionForceStopTracking)
            trackingSwitch.setOn(false, animated: false)
            onGoingTrakcingSwitch.setOn(false, animated: false)
            trackingStatusOnGoingTrip.text = NSLocalizedString("tracking-disabled",comment : "")
            trackingStatusWaitingForTripLabel.text = NSLocalizedString("tracking-disabled",comment : "")
        }
    }
    
    
    
    func setArrayPieChart() {
        entries.removeAll()
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))

        entries.append(PieChartDataEntry(value: 0, label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0,label: "", icon: nil))
        entries.append(PieChartDataEntry(value: 0, label: "", icon: nil))
    }
    @IBAction func segmentedControlTapped(_ sender: Any) {
        let index = segmentedControl.selectedSegmentIndex
        reattributeValue()
        if index == 0 {
            getStats(filter: "?days=5")
        } else if index == 1 {
            getStats(filter: "?filter=this-month")
        } else if index == 2 {
            getStats(filter: "")
        }
    }
    func setupCheckBox() {
        planeCheckBoxImageView.image = UIImage(named: "airplane_unchecked")
        planeButton.isEnabled = false
        planeChecboxChecked = false
        planeCheckBoxImageView.alpha = 0.5
        trainCheckBoxImageView.image = UIImage(named: "train_unchecked")
        trainButton.isEnabled = false
        trainChecboxChecked = false
        trainCheckBoxImageView.alpha = 0.5
        carCheckBoxImageView.image = UIImage(named: "car_unchecked")
        carButton.isEnabled = false
        carChecboxChecked = false
        carCheckBoxImageView.alpha = 0.5
        
        walkCheckBoxImageView.image = UIImage(named: "walk_unchecked")
        walkButton.isEnabled = false
        walkChecboxChecked = false
        walkCheckBoxImageView.alpha = 0.5
        bikeCheckBoxImageView.image = UIImage(named: "bicycle_unchecked")
        bikeButton.isEnabled = false
        bikeChecboxChecked = false
        bikeCheckBoxImageView.alpha = 0.5
        subwayCheckBoxImageView.image = UIImage(named: "subway_unchecked")
        subwayButton.isEnabled = false
        subwayChecboxChecked = false
        subwayCheckBoxImageView.alpha = 0.5
        
        busCheckBoxImageView.image = UIImage(named: "bus_unchecked")
        busButton.isEnabled = false
        busChecboxChecked = false
        busCheckBoxImageView.alpha = 0.5
        carpoolingCheckBoxImageView.image = UIImage(named: "carpooling_unchecked")
        carpoolingButton.isEnabled = false
        carpoolingCheckBoxChecked = false
        carpoolingCheckBoxImageView.alpha = 0.5
        tramCheckBoxImageView.image = UIImage(named: "tram_unchecked")
        tramButton.isEnabled = false
        tramChecboxChecked = false
        tramCheckBoxImageView.alpha = 0.5
        
        motoCheckBoxImageView.image = UIImage(named: "moto_unchecked")
        motoButton.isEnabled = false
        motoCheckBoxChecked = false
        motoCheckBoxImageView.alpha = 0.5
        escooterCheckBoxImageView.image = UIImage(named: "escooter_unchecked")
        escooterButton.isEnabled = false
        escooterCheckBoxChecked = false
        escooterCheckBoxImageView.alpha = 0.5
        otherCheckBoxImageView.image = UIImage(named: "other_unchecked")
        otherButton.isEnabled = false
        otherCheckBoxChecked = false
        otherCheckBoxImageView.alpha = 0.5
    }
    
    @IBAction func toStoreTapped(_ sender: Any) {
        let url = URL(string: K.Path.baseURLWeb + "/weflo/boutique")
        let vc = SFSafariViewController(url: url!)
        present(vc, animated: true)
    }
    
    func getUserProgFid() {
        var jsonResponse : JSON = []
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "content-type": "application/json"
        ]
        var request = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/dashboard/company")!)
        request.headers = headers
        request.httpMethod = "GET"
        AF.request(request).responseJSON { [self] response in
            jsonResponse = JSON(response.value as Any)
            if(response.value != nil){
                if(response.response?.statusCode == 401){
                    RF.renewIrebyToken { [self] (success) -> Void in
                        if success {
                            getUserProgFid()
                        }
                    }
                }
                userProgFid = jsonResponse["name"].stringValue
                UserTripData.progFid = userProgFid
            }
            if(userProgFid != "PVE") {
                hebdoQuestionView.isHidden = true
                stackViewToButtomConstraint.constant = 30.0
            }
        }
    }
    @IBAction func getQuestion(_ sender: Any) {
        var jsonResponse : JSON = []
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "content-type": "application/json"
        ]
        var request = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/weeklyquestion/hasalreadyanswered")!)
        request.headers = headers
        request.httpMethod = "GET"
        AF.request(request).responseJSON { [self] response in
            if(response.value != nil){
                jsonResponse = JSON(response.value as Any)
                print("JSON RESPONSE = ",jsonResponse)
                if(jsonResponse["answered"].boolValue) {
                    let alertVC = self.serviceAlert.alert(title:  NSLocalizedString("question-popup-title",comment : ""), body: NSLocalizedString("question-popup-body",comment : ""), buttonTitle: NSLocalizedString("question-popup-button",comment : "")) {
                        
                    }
                    tabBarController!.present(alertVC, animated: true)
                    print("Already answered")
                } else {
                    let url = URL(string: K.Path.baseURLWeb + "/question-du-jour?access_token=" + UserDefaults.standard.string(forKey: "Token")!)
                    let vc = SFSafariViewController(url: url!)
                    present(vc, animated: true)
                }
            } else {
                print("error something wrong happened with ireby")
            }
            
        }
        
        
        

    }
    func getStatsPointsAfterViewDidLoad () {
        proPointLabel.text = Point.pro
        standardPointLabel.text = Point.standard
    }
    
    func getStatsPoints() {
        let reachability = try! Reachability()
        if (reachability.connection == .wifi || reachability.connection == .cellular) {
        var jsonResponse : JSON = []
            print("My access token = ",UserDefaults.standard.string(forKey: "ireby_access_token")!)
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "content-type": "application/json"
        ]
        var request = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/dashboard/points")!)
        request.headers = headers
        request.httpMethod = "GET"
        AF.request(request).responseJSON { [self] response in
            if(response.response?.statusCode != 500){
                UserDefaults.standard.removeObject(forKey: "hasStartedSignIn")
                UserDefaults.standard.set(true, forKey: "ireby_synchronization")
                RF.updateUIWefloNotSync(proView: proPointsView, standardView: standardPointsView, buttonSync: syncButton, isSynced:  UserDefaults.standard.bool(forKey: "ireby_synchronization"))
                jsonResponse = JSON(response.value as Any)
                print("My response = ",jsonResponse)
                if(response.response?.statusCode == 401){
                    RF.renewIrebyToken { [self] (success) -> Void in
                        if success {
                            getStatsPoints()
                        }
                    }
                } else {
                    
                    if jsonResponse["pro_points"].stringValue == "0" {
                        Point.pro = "0 Point"
                    } else {
                        Point.pro = jsonResponse["pro_points"].stringValue + " Points"
                    }
                    if jsonResponse["points"].stringValue == "0" {
                        Point.standard = "0 Point"
                    } else {
                        Point.standard = jsonResponse["points"].stringValue + " Points"
                    }
                    self.proPointLabel.text = Point.pro
                    self.standardPointLabel.text = Point.standard
                }
            } else {
                let headersNotSync: HTTPHeaders = [
                    "token" : UserDefaults.standard.string(forKey: "ireby_access_token")!   ,
                    "authorization": "Bearer " + K.Constant.irebyBearer,
                    "content-type": "application/json"
                ]
                
                AF.request(K.Path.baseURLWeb + "/api/users/is-synced", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headersNotSync, interceptor: nil, requestModifier: nil).responseJSON { response in
                    let jsonResponse = JSON(response.value)
                    if jsonResponse["message"].boolValue == false {
                        UserDefaults.standard.set(false, forKey: "ireby_synchronization")
                        RF.updateUIWefloNotSync(proView: proPointsView, standardView: standardPointsView, buttonSync: syncButton, isSynced:  UserDefaults.standard.bool(forKey: "ireby_synchronization"))
                    } else {
                        UserDefaults.standard.set(true, forKey: "ireby_synchronization")
                        RF.updateUIWefloNotSync(proView: proPointsView, standardView: standardPointsView, buttonSync: syncButton, isSynced:  UserDefaults.standard.bool(forKey: "ireby_synchronization"))
                    }
                }
                
            }
           
        }
        } else {
            print("no reachable")
            RF.updateUIWefloNotSync(proView: proPointsView, standardView: standardPointsView, buttonSync: syncButton, isSynced: true)
            
        }
    }
    
    func comparaisonHamburgerCloud (kcal : Double, co2 : Double, filter : String) -> [String:String] {
        var co2PerDay = 0.0
        let date = Date()
        switch filter {
        case "?days=5":
            co2PerDay = co2 / 5
        case "?filter=this-month":
            let calendar = Calendar.current
            let day = calendar.component(.day, from: date)
            co2PerDay = co2 / Double(day)
        default:
            co2PerDay = co2
        }
        return ["cal":String(format: "%.1f", kcal / 278),"co2":String(format: "%.1f", co2PerDay / 1000)]
    }
    
    func getStats (filter : String) {
        var totalKm = 0.0
        jsonMode = []
        setupCheckBox()
        setArrayPieChart()
        var jsonStatResponse : JSON = []
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "Token")!,
            "content-type": "application/json"
        ]
        var request = URLRequest(url: URL(string: K.Path.cleverApi + "users/stats" + filter)!)
        request.headers = headers
        request.httpMethod = "GET"
        AF.request(request).responseJSON { [self] response in
            if response.value != nil {
                jsonStatResponse = JSON(response.value!)
                self.totalTrips.text = jsonStatResponse["tripCount"].stringValue
                if (jsonStatResponse["tripCount"].intValue > 1) {
                    tripsLabel.text = NSLocalizedString("trips", comment: "")
                } else {
                    tripsLabel.text = NSLocalizedString("trip", comment: "")
                }
                jsonMode = jsonStatResponse["modes"]
                print(jsonMode)
                self.modeNumber = 0
                if jsonMode.count != 0 {
                    for i in 0...jsonMode.count - 1 {
                        totalKm += jsonMode[i]["distance"].doubleValue
                        switch jsonMode[i]["name"] {
                        case "plane":
                            entries[0] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: airplaneImage)
                            planeChecboxChecked = true
                            planeCheckBoxImageView.image = UIImage(named: "airplane_checked")
                            planeCheckBoxImageView.alpha = 1.0
                            planeButton.isEnabled = true
                            modeNumber += 1
                        case "train":
                            entries[1] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: trainImage)
                            trainChecboxChecked = true
                            trainCheckBoxImageView.image = UIImage(named: "train_checked")
                            trainCheckBoxImageView.alpha = 1.0
                            trainButton.isEnabled = true
                            modeNumber += 1
                        case "car":
                            entries[2] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: carImage)
                            carChecboxChecked = true
                            carCheckBoxImageView.image = UIImage(named: "car_checked")
                            carCheckBoxImageView.alpha = 1.0
                            carButton.isEnabled = true
                            modeNumber += 1
                            
                            
                        case "walking":
                            entries[3] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: walkImage)
                            walkChecboxChecked = true
                            walkCheckBoxImageView.image = UIImage(named: "walk_checked")
                            walkCheckBoxImageView.alpha = 1.0
                            walkButton.isEnabled = true
                            modeNumber += 1
                        case "cycling":
                            entries[4] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: bikeImage)
                            bikeChecboxChecked = true
                            bikeCheckBoxImageView.image = UIImage(named: "bicycle_checked")
                            bikeCheckBoxImageView.alpha = 1.0
                            bikeButton.isEnabled = true
                            modeNumber += 1
                        case "subway":
                            entries[5] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: subwayImage)
                            subwayChecboxChecked = true
                            subwayCheckBoxImageView.image = UIImage(named: "subway_checked")
                            subwayCheckBoxImageView.alpha = 1.0
                            subwayButton.isEnabled = true
                            modeNumber += 1
                            
                            
                        case "bus":
                            entries[6] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: busImage)
                            busChecboxChecked = true
                            busCheckBoxImageView.image = UIImage(named: "bus_checked")
                            busCheckBoxImageView.alpha = 1.0
                            busButton.isEnabled = true
                            modeNumber += 1
                        case "carpooling":
                            entries[7] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: carpoolingImage)
                            carpoolingCheckBoxChecked = true
                            carpoolingCheckBoxImageView.image = UIImage(named: "carpooling_checked")
                            carpoolingCheckBoxImageView.alpha = 1.0
                            carpoolingButton.isEnabled = true
                            modeNumber += 1
                        case "tram":
                            entries[8] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: tramImage)
                            tramChecboxChecked = true
                            tramCheckBoxImageView.image = UIImage(named: "tram_checked")
                            tramCheckBoxImageView.alpha = 1.0
                            tramButton.isEnabled = true
                            modeNumber += 1
                            
                            
                        case "motorbike":
                            entries[9] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: motoImage)
                            motoCheckBoxChecked = true
                            motoCheckBoxImageView.image = UIImage(named: "moto_checked")
                            motoCheckBoxImageView.alpha = 1.0
                            motoButton.isEnabled = true
                            modeNumber += 1
                        
                        case "e-scooter":
                            entries[10] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: escooterImage)
                            escooterCheckBoxChecked = true
                            escooterCheckBoxImageView.image = UIImage(named: "escooter_checked")
                            escooterCheckBoxImageView.alpha = 1.0
                            escooterButton.isEnabled = true
                            modeNumber += 1
                        case "other":
                            entries[11] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: otherImage)
                            otherCheckBoxChecked = true
                            otherCheckBoxImageView.image = UIImage(named: "other_checked")
                            otherCheckBoxImageView.alpha = 1.0
                            otherButton.isEnabled = true
                            modeNumber += 1
                        default:
                            print("Error entries pieChart")
                        }
                    }
                }
                let comparaison = comparaisonHamburgerCloud(kcal: jsonStatResponse["calories"].doubleValue, co2: jsonStatResponse["carbonDioxide"].doubleValue, filter: filter)
                totalGraphDistance = totalKm
                totalKilometers.text = String(format: "%.0f", totalKm) + " Km"
                calLabel.text = jsonStatResponse["calories"].stringValue
                co2Label.text = comparaison["co2"]!
                minimumAnglePieChart()
                calInfoLabel.text = "= " + comparaison["cal"]! + " 🍔 "
                if(filter == ""){
                    co2Label.text = comparaison["co2"]!
                    co2ImageView.image = UIImage(named: "carbon-dioxide-1")
                    co2InfoLabel.text = NSLocalizedString("allco2", comment: "")
                } else {
                    co2Label.text = comparaison["co2"]! + " / " + NSLocalizedString("day", comment: "")
                    if(0.0 <= Double(comparaison["co2"]!)! && Double(comparaison["co2"]!)! < 2.9) {
                        co2ImageView.image = UIImage(named: "co-2")
                        co2InfoLabel.text = NSLocalizedString("1-co2", comment: "")
                    } else if(2.9 <= Double(comparaison["co2"]!)! && Double(comparaison["co2"]!)! < 5.9 ) {
                        co2ImageView.image = UIImage(named: "composant-48-1")
                        co2InfoLabel.text = NSLocalizedString("2-co2", comment: "")
                    } else {
                        co2ImageView.image = UIImage(named: "composant-49-1")
                        co2InfoLabel.text = NSLocalizedString("3-co2", comment: "")
                    }
                }
                
                setupPieChart()
            }
        }
    }
    
    @IBAction func scrollToButtomTapped(_ sender: Any) {
        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.height + scrollView.contentInset.bottom)
        scrollView.setContentOffset(bottomOffset, animated: true)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + 1) >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            scrollToDownButton.isHidden = true
            UserDefaults.standard.setValue(true, forKey: "hasReachedBottom")
           }
        }
    
    func reattributeValue () {
        if jsonMode.count != 0 {
            for i in 0...jsonMode.count - 1 {
                switch jsonMode[i]["name"] {
                case "plane":
                    if planeChecboxChecked == true {
                        entries[0] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: airplaneImage)
                    } else {
                        entries[0] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                case "train":
                    if trainChecboxChecked == true {
                        entries[1] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: trainImage)
                    } else {
                        entries[1] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                case "car":
                    if carChecboxChecked == true {
                        entries[2] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: carImage)
                    } else {
                        entries[2] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }

                    
                case "walking":
                    if walkChecboxChecked == true {
                        entries[3] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: walkImage)
                    } else {
                        entries[3] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                    
                case "cycling":
                    if bikeChecboxChecked == true {
                        entries[4] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: bikeImage)
                    } else {
                        entries[4] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                case "subway":
                    if subwayChecboxChecked == true {
                        entries[5] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: subwayImage)
                    } else {
                        entries[5] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                    
                    
                case "bus":
                    if busChecboxChecked == true {
                        entries[6] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: busImage)
                    } else {
                        entries[6] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                case "carpooling":
                    if carpoolingCheckBoxChecked == true {
                        entries[7] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: carpoolingImage)
                    } else {
                        entries[7] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                case "tram":
                    if tramChecboxChecked == true {
                        entries[8] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: tramImage)
                    } else {
                        entries[8] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                    
                case "motorbike":
                    if motoCheckBoxChecked == true {
                        entries[9] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: motoImage)
                    } else {
                        entries[9] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                case "e-scooter":
                    if escooterCheckBoxChecked == true {
                        entries[10] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: escooterImage)
                    } else {
                        entries[10] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                case "other":
                    if otherCheckBoxChecked == true {
                        entries[11] = PieChartDataEntry(value: jsonMode[i]["distance"].doubleValue,label: String(format: "%.1f",jsonMode[i]["distance"].doubleValue) + " km", icon: otherImage)
                    } else {
                        entries[11] = PieChartDataEntry(value: 0,label: "", icon: nil)
                    }
                    
                default:
                    print("Error entries pieChart")
                }
            }
        }
    }
    
    func calcPercentage (val: Int, total: Double) -> Int {
        if(Int(total) != 0) {
            return val * 100 / Int(total)
        } else {
            return 0
        }
        
    }
    
    func minimumAnglePieChart () {
        for i in 0...entries.count - 1 {
            if (entries[i].value != 0 && calcPercentage(val: Int(entries[i].value), total: totalGraphDistance) < 5) {
                entries[i] = PieChartDataEntry(value: entries[i].value, label: "", icon: nil)
            }
        }
    }
    
    
    
    
    
    func setDesign() {
        standardPointsView.backgroundColor = K.Color.primaryColor
        proPointsView.backgroundColor = K.Color.secondaryColor
        profileImageButton.layoutIfNeeded()
        profileImageButton.subviews.first?.contentMode = .scaleAspectFill
        lineWidthConstraint.constant = 0.5
        lineHeightConstraint.constant = 0.5
        containerView.backgroundColor = .clear
        containerView.layer.shadowRadius = 4
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowOffset = CGSize(width: 1, height: 1)
        containerView.layer.shadowColor = UIColor.gray.cgColor
        mapView.frame = containerView.bounds
        mapView.layer.cornerRadius = 10.0
        mapView.layer.masksToBounds = true
        
    }
    
    
    
    func setConfig () {
        let cfg = ConfigManager.instance()
        let retDict = DataUtils.wrapper(toDict: cfg)
        let newCfg = LocationTrackingConfig()
        DataUtils.dict(toWrapper: retDict, wrapper: newCfg)
        ConfigManager.update(newCfg)
    }
    
    func getAccuracyOption () {
        var retVal: [AnyHashable : Any] = [:]
        retVal["kCLLocationAccuracyBestForNavigation"] = NSNumber(value: kCLLocationAccuracyBestForNavigation)
        retVal["kCLLocationAccuracyBest"] = NSNumber(value: kCLLocationAccuracyBest)
        retVal["kCLLocationAccuracyNearestTenMeters"] = NSNumber(value: kCLLocationAccuracyNearestTenMeters)
        retVal["kCLLocationAccuracyHundredMeters"] = NSNumber(value: kCLLocationAccuracyHundredMeters)
        retVal["kCLLocationAccuracyKilometer"] = NSNumber(value: kCLLocationAccuracyKilometer)
        retVal["kCLLocationAccuracyThreeKilometers"] = NSNumber(value: kCLLocationAccuracyThreeKilometers)
        print(retVal)
    }

    
    @objc func getCurrentState(didPullToRefresh : Bool) {
        self.tripDays = UserTripData.userTrips
        dotView.layer.sublayers?.removeAll()
        let annotations = mapView.annotations.filter {
            $0 !== self.mapView.userLocation
        }
        mapView.removeAnnotations(annotations)
        let currstate = TripDiaryStateMachine.getStateName(tripDiaryStateMachine!.currState)
        if(currstate == "STATE_ONGOING_TRIP") {
            showOngoingTrip()
            refreshControl.endRefreshing()
        } else {
            lastTripDetailLabel.text = NSLocalizedString("onload",comment : "")
            showWaitingForTripToStart()
            getLastTrip(didpullToRefresh: didPullToRefresh)
        }
        
        
    }
    
    func setMap () {
        mapView.removeOverlays(polylines)
        mapView.showsUserLocation = true
        if let userLocation = locationManager.location?.coordinate {
            let viewRegion = MKCoordinateRegion(center: userLocation, latitudinalMeters: 500, longitudinalMeters: 500)
            mapView.setRegion(viewRegion, animated: false)
        }
        
        
        DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }
    }
    
    
    
    func setupPieChart () {
        
        let dataSet = PieChartDataSet(entries: entries)
        
        let airplaneColor = K.Color.AIR
        let trainColor = K.Color.TRAIN
        let carColor = K.Color.CAR
        
        let walkColor = K.Color.WALKING
        let bikeColor = K.Color.BICYCLING
        let subwayColor = K.Color.SUBWAY
        
        let busColor = K.Color.BUS
        let carpoolingColor = K.Color.CARPOOLING
        let tramColor = K.Color.TRAM
        
        let motoColor = K.Color.MOTO
        let escooterColor = K.Color.ESCOOTER
        let otherColor = K.Color.OTHER
        
        
        dataSet.colors = [airplaneColor,trainColor,carColor,walkColor,bikeColor,subwayColor,busColor,carpoolingColor,tramColor,motoColor,escooterColor,otherColor]
        
        let data = PieChartData(dataSet: dataSet)
        dataSet.iconsOffset = CGPoint(x: -10, y: 0)
        data.setValueTextColor(.clear)
        dataSet.entryLabelFont = UIFont(name: "Avenir-Roman", size: 11.0)
        pieChartView.data = data
        pieChartView.legend.enabled = false
        pieChartView.highlightValues(nil)
        pieChartView.noDataText = NSLocalizedString("no-data-graph",comment : "")
        pieChartView.noDataFont = UIFont(name: "Avenir-Roman", size: 25.0)!
        pieChartView.noDataTextAlignment = .center
        print(entries)
        if(modeNumber == 0){
            pieChartView.data = nil
            
        }
        if pieChartView.data == nil {
            noTripsView.isHidden = false
        } else {
            noTripsView.isHidden = true
        }
        
    }
    
    @IBAction func planeCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "plane"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        planeChecboxChecked = !planeChecboxChecked
        if(planeChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[0] = PieChartDataEntry(value: distance, label: String(format: "%.1f",distance) + " km", icon: airplaneImage)
            self.planeCheckBoxImageView.image = UIImage(named: "airplane_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.planeChecboxChecked = !self.planeChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[0] = PieChartDataEntry(value: 0, label: "")
                self.planeCheckBoxImageView.image = UIImage(named: "airplane_unchecked")
            }
        }
        
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func trainCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "train"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        trainChecboxChecked = !trainChecboxChecked
        if(trainChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[1] = PieChartDataEntry(value: distance, label: String(format: "%.1f",distance) + " km", icon: trainImage)
            self.trainCheckBoxImageView.image = UIImage(named: "train_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.trainChecboxChecked = !self.trainChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[1] = PieChartDataEntry(value: 0, label: "")
                self.trainCheckBoxImageView.image = UIImage(named: "train_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func carCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "car"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        carChecboxChecked = !carChecboxChecked
        if(carChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[2] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: carImage)
            self.carCheckBoxImageView.image = UIImage(named: "car_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.carChecboxChecked = !self.carChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                print("nombre de mode =",modeNumber)
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[2] = PieChartDataEntry(value: 0, label: "")
                self.carCheckBoxImageView.image = UIImage(named: "car_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }

    @IBAction func walkCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "walking"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        walkChecboxChecked = !walkChecboxChecked
        print("walkIS",walkChecboxChecked)
        if(walkChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[3] = PieChartDataEntry(value: distance, label: String(format: "%.1f",distance) + " km", icon: walkImage)
            self.walkCheckBoxImageView.image = UIImage(named: "walk_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.walkChecboxChecked = !self.walkChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[3] = PieChartDataEntry(value: 0, label: "")
                self.walkCheckBoxImageView.image = UIImage(named: "walk_unchecked")
            }
            
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func bikeCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "cycling"){
                distance = jsonMode[i]["distance"].doubleValue
                
            }
        }
        bikeChecboxChecked = !bikeChecboxChecked
        if(bikeChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[4] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: bikeImage)
            self.bikeCheckBoxImageView.image = UIImage(named: "bicycle_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.bikeChecboxChecked = !self.bikeChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[4] = PieChartDataEntry(value: 0, label: "")
                self.bikeCheckBoxImageView.image = UIImage(named: "bicycle_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func subwayCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "subway"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        subwayChecboxChecked = !subwayChecboxChecked
        if(subwayChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[5] = PieChartDataEntry(value: distance, label: String(format: "%.1f",distance) + " km", icon: subwayImage)
            self.subwayCheckBoxImageView.image = UIImage(named: "subway_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.subwayChecboxChecked = !self.subwayChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[5] = PieChartDataEntry(value: 0, label: "")
                self.subwayCheckBoxImageView.image = UIImage(named: "subway_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    
    @IBAction func busCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "bus"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        busChecboxChecked = !busChecboxChecked
        if(busChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[6] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: busImage)
            self.busCheckBoxImageView.image = UIImage(named: "bus_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.busChecboxChecked = !self.busChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[6] = PieChartDataEntry(value: 0, label: "")
                self.busCheckBoxImageView.image = UIImage(named: "bus_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func carpoolingCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "carpooling"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        carpoolingCheckBoxChecked = !carpoolingCheckBoxChecked
        if(carpoolingCheckBoxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[7] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: carpoolingImage)
            self.carpoolingCheckBoxImageView.image = UIImage(named: "carpooling_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.carpoolingCheckBoxChecked = !self.carpoolingCheckBoxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[7] = PieChartDataEntry(value: 0, label: "")
                self.carpoolingCheckBoxImageView.image = UIImage(named: "carpooling_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func tramCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "tram"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        tramChecboxChecked = !tramChecboxChecked
        if(tramChecboxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[8] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: tramImage)
            self.tramCheckBoxImageView.image = UIImage(named: "tram_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.tramChecboxChecked = !self.tramChecboxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[8] = PieChartDataEntry(value: 0, label: "")
                self.tramCheckBoxImageView.image = UIImage(named: "tram_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    
    @IBAction func motoCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "motorbike"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        motoCheckBoxChecked = !motoCheckBoxChecked
        if(motoCheckBoxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[9] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: motoImage)
            self.motoCheckBoxImageView.image = UIImage(named: "moto_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.motoCheckBoxChecked = !self.motoCheckBoxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[9] = PieChartDataEntry(value: 0, label: "")
                self.motoCheckBoxImageView.image = UIImage(named: "moto_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func escooterCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "e-scooter"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        escooterCheckBoxChecked = !escooterCheckBoxChecked
        if(escooterCheckBoxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[10] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: escooterImage)
            self.escooterCheckBoxImageView.image = UIImage(named: "escooter_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.escooterCheckBoxChecked = !self.escooterCheckBoxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[10] = PieChartDataEntry(value: 0, label: "")
                self.escooterCheckBoxImageView.image = UIImage(named: "escooter_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    @IBAction func otherCheckBoxTapped(_ sender: Any) {
        reattributeValue()
        var distance = 0.0
        for i in 0...jsonMode.count - 1 {
            if(jsonMode[i]["name"] == "other"){
                distance = jsonMode[i]["distance"].doubleValue
            }
        }
        otherCheckBoxChecked = !otherCheckBoxChecked
        if(otherCheckBoxChecked){
            totalGraphDistance += distance
            modeNumber += 1
            entries[11] = PieChartDataEntry(value: distance, label: String(format: "%.1f", distance) + " km", icon: otherImage)
            self.otherCheckBoxImageView.image = UIImage(named: "other_checked")
        } else {
            if(modeNumber == 1){
                let alert = serviceAlert.alert(title: NSLocalizedString("warning-graph-popup-title",comment : ""), body: NSLocalizedString("warning-graph-popup-body", comment : ""), buttonTitle: NSLocalizedString("warning-graph-popup-button", comment : "")) {
                    self.otherCheckBoxChecked = !self.otherCheckBoxChecked
                }
                tabBarController!.present(alert, animated: true)
            } else {
                totalGraphDistance -= distance
                modeNumber -= 1
                entries[11] = PieChartDataEntry(value: 0, label: "")
                self.otherCheckBoxImageView.image = UIImage(named: "other_unchecked")
            }
        }
        minimumAnglePieChart()
        setupPieChart()
    }
    
    @IBAction func startNewTripTapped(_ sender: Any) {


        let alert = serviceAlert.alertOptionWithInterest(title: NSLocalizedString("title-interest-place", comment: ""), body: NSLocalizedString("body-interest-place", comment: ""), buttonTitle: NSLocalizedString("button-interest-place", comment: ""), completion: { [self] in
            let token = UserDefaults.standard.string(forKey: "Token")
            if interestingPlace == "" {
                showAlertError()
            } else {
                if let userLocation = locationManager.location?.coordinate {
                    print(userLocation.latitude)
                    print(userLocation.longitude)
                    let params : [String:Any] = ["name":getPlace(place: interestingPlace),"latitude":userLocation.latitude,"longitude":userLocation.longitude]
                    let body = try! JSONSerialization.data(withJSONObject: params)
                    print(params)
                    let headers: HTTPHeaders = [
                        "authorization": "Bearer " + token!,
                        "content-type": "application/json"
                    ]
                    var request = URLRequest(url: URL(string: K.Path.cleverApi + "checkpoints")!)
                    request.headers = headers
                    request.httpBody = body
                    request.httpMethod = "POST"
                    
                    AF.request(request).responseJSON { response in
                        print(response.result)
                        if response.response?.statusCode != 201 {
                            showAlertError()
                        } else {
                            interestingPlace = ""
                            self.dismiss(animated: true)
                        }
                    }
                } else {
                    showAlertError()
                }
            }
        })
        tabBarController!.present(alert, animated: true)
        
    }
    
    func showAlertError () {
        let alertError = serviceAlert.alert(title: NSLocalizedString("popup-error-title", comment: ""), body: NSLocalizedString("popup-error-body", comment: ""), buttonTitle: NSLocalizedString("popup-error-button", comment: "")) {
            
        }
        self.dismiss(animated: true) {
            self.tabBarController!.present(alertError, animated: true)
        }
    }
    
    func getPlace (place : String) -> String {
        switch place {
        case NSLocalizedString("parking", comment: ""):
            return "parking"
        case NSLocalizedString("carpool-area", comment: ""):
            return "carpooling_area"
        case NSLocalizedString("bus-stop", comment: ""):
            return "bus_stop"
        default:
            return "error"
        }
    }
    
 
    
    func showWaitingForTripToStart() {
        trackingSwitch.isHidden = false
        trackingStatusWaitingForTripLabel.isHidden = false
        startStopContainerView.isHidden = true
        centeredLineView.isHidden = true
        onGoinfTripContainerView.isHidden = true
        stateLabel.text = NSLocalizedString("last-trip-text",comment : "")
        self.dotView.layer.sublayers?.removeAll()
    }
    func showOngoingTrip() {
        trackingSwitch.isHidden = true
        trackingStatusWaitingForTripLabel.isHidden = true
        startStopContainerView.isHidden = false
        centeredLineView.isHidden = false
        onGoinfTripContainerView.isHidden = false
        stateLabel.text = NSLocalizedString("ongoing-trip-text",comment : "")
        lastTripDetailLabel.text = NSLocalizedString("trip-detail-ongoing-text",comment : "")
        animateDots()
        setMap()
    }
 
    
    func animateDots() {
        let lay = CAReplicatorLayer()
        lay.frame = CGRect(x: 0, y: 0, width: 50, height: 10)
        
        let bar = CALayer()
        bar.frame = CGRect(x: 0,y: 0,width: 10,height: 10)
        bar.cornerRadius = 5.0
        bar.backgroundColor = K.Color.primaryColor.cgColor
        lay.addSublayer(bar)
        lay.instanceCount = 3
        lay.instanceTransform = CATransform3DMakeTranslation(20, 0, 0)
        let anim = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        anim.fromValue = 1.0
        anim.toValue = 0.2
        anim.duration = 1
        anim.repeatCount = .infinity
        bar.add(anim, forKey: nil)
        lay.instanceDelay = anim.duration / Double(lay.instanceCount)
        self.dotView.layer.addSublayer(lay)
        
    }
    func fixBackgroundSegmentControl( _ segmentControl: UISegmentedControl){
        //just to be sure it is full loaded
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            for i in 0...(segmentControl.numberOfSegments-1)  {
                let backgroundSegmentView = segmentControl.subviews[i]
                //it is not enogh changing the background color. It has some kind of shadow layer
                backgroundSegmentView.isHidden = true
            }
        }
    }
    
}


extension DashboardViewController {
    
    
    
    func getLastTrip (didpullToRefresh : Bool) {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
        }
        self.sections = []
        DispatchQueue.main.async { [self] in
            let reachability = try! Reachability()
            if (reachability.connection == .wifi || reachability.connection == .cellular) {
                self.emptyMessageLabel.isHidden = true
                if self.tripDays != [] && didpullToRefresh == false {
                    let trip = self.tripDays[self.tripDays.count - 1]
                    getSections(jsonArray: trip)
                    let date = RF.formatDateFromTimeStamp(ts: sections[0]["startTs"].doubleValue)
                    let duration = RF.formatDuration(sections[sections.count - 1]["endTs"].doubleValue - sections[0]["startTs"].doubleValue)
                    let distance = self.RF.formatDistance(trip["totalDistance"].doubleValue)
                    self.result = date + " - " + distance + " km - " + duration
                    self.lastTripDetailLabel.text = self.result
                    self.mapView.isHidden = false
                    self.setupMapKit()
                    self.setPins(sections: sections)
                    if let first = self.mapView.overlays.first {
                        let rect = self.mapView.overlays.reduce(first.boundingMapRect, {$0.union($1.boundingMapRect)})
                        self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                    }
                } else if didpullToRefresh == false && self.tripDays == [] || didpullToRefresh == true {
                    self.activityIndicator.isHidden = false
                    self.activityIndicator.startAnimating()
                    self.mapView.isHidden = true
                    RF.getTrips(startTs: Date.changeDaysBy(days: -5).timeIntervalSince1970, endTs: Date.changeDaysBy(days: 1).timeIntervalSince1970) { success in
                        if success {
                            self.tripDays = UserTripData.userTrips
                            if(self.tripDays.count != 0){
                                let trip = self.tripDays[self.tripDays.count-1]
                                self.getSections(jsonArray: trip)
                                let date = RF.formatDateFromTimeStamp(ts: sections[0]["startTs"].doubleValue)
                                let duration = RF.formatDuration(sections[sections.count - 1]["endTs"].doubleValue - sections[0]["startTs"].doubleValue)
                                let distance = self.RF.formatDistance(trip["totalDistance"].doubleValue)
                                self.result = date + " - " + distance + " km - " + duration
                                self.lastTripDetailLabel.text = self.result
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    self.activityIndicator.isHidden = true
                                    self.activityIndicator.stopAnimating()
                                    self.mapView.isHidden = false
                                    self.refreshControl.endRefreshing()
                                    self.setupMapKit()
                                    self.setPins(sections: sections)
                                    if let first = self.mapView.overlays.first {
                                        let rect = self.mapView.overlays.reduce(first.boundingMapRect, {$0.union($1.boundingMapRect)})
                                        self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                                    }
                                }
                            } else {
                                self.activityIndicator.isHidden = true
                                self.activityIndicator.stopAnimating()
                                self.refreshControl.endRefreshing()
                                self.lastTripDetailLabel.text = NSLocalizedString("no-trip-for-five-days",comment : "")
                                self.emptyMessageLabel.isHidden = false
                                self.emptyMessageLabel.text = NSLocalizedString("empty-map-text",comment : "")
                            }
                        }
                    }
                } else {
                    self.refreshControl.endRefreshing()
                    self.activityIndicator.isHidden = true
                    self.mapView.isHidden = true
                    self.emptyMessageLabel.isHidden = false
                }
            } else {
                self.refreshControl.endRefreshing()
                self.activityIndicator.isHidden = true
                self.mapView.isHidden = true
                self.emptyMessageLabel.isHidden = false
                self.emptyMessageLabel.text = NSLocalizedString("no-connection",comment : "")
                self.lastTripDetailLabel.text = NSLocalizedString("no-connection",comment : "")
            }
        }
    }
    
    func getSections (jsonArray : JSON) {
        sections = jsonArray["sections"].arrayValue
    }
    func setupMapKit () {
        mapView.showsUserLocation = false
        mapView.removeOverlays(polylines)
        polylines = []
        for i in 0...sections.count - 1 {
            let data = sections[i]["data"].arrayValue
            location = []
            if data.count != 0 {
                if sections[i]["mode"].stringValue == "CAR" {
                    for j in 0...data.count - 1 {
                        
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let polyline = MKPolyline(coordinates: &location, count: location.count)
                    polyline.title = "CAR"
                    polylines.append(polyline)
                } else if(sensedModePerSection(data: sections[i]) == "WALKING") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let walkPolyline = MKPolyline(coordinates: &location, count: location.count)
                    walkPolyline.title = "WALKING"
                    polylines.append(walkPolyline)
                    
                } else if(sensedModePerSection(data: sections[i]) == "BICYCLING") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let bicyclePolyline = MKPolyline(coordinates: &location, count: location.count)
                    bicyclePolyline.title = "BICYCLING"
                    polylines.append(bicyclePolyline)
                    
                }  else if(sensedModePerSection(data: sections[i]) == "BUS") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let busPolyline = MKPolyline(coordinates: &location, count: location.count)
                    busPolyline.title = "BUS"
                    polylines.append(busPolyline)
                }  else if(sensedModePerSection(data: sections[i]) == "TRAIN") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let trainPolyline = MKPolyline(coordinates: &location, count: location.count)
                    trainPolyline.title = "TRAIN"
                    polylines.append(trainPolyline)
                }  else if(sensedModePerSection(data: sections[i]) == "AIR_OR_HSR") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let airPolyline = MKPolyline(coordinates: &location, count: location.count)
                    airPolyline.title = "AIR_OR_HSR"
                    polylines.append(airPolyline)
                } else if(sensedModePerSection(data: sections[i]) == "SUBWAY") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let subwayPolyline = MKPolyline(coordinates: &location, count: location.count)
                    subwayPolyline.title = "SUBWAY"
                    polylines.append(subwayPolyline)
                } else if(sensedModePerSection(data: sections[i]) == "TRAM") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let tramPolyline = MKPolyline(coordinates: &location, count: location.count)
                    tramPolyline.title = "TRAM"
                    polylines.append(tramPolyline)
                } else if(sensedModePerSection(data: sections[i]) == "UNKNOWN") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let unknownPolyline = MKPolyline(coordinates: &location, count: location.count)
                    unknownPolyline.title = "UNKNOWN"
                    polylines.append(unknownPolyline)
                }
            } else {
                print("no coordinates")
            }
        }
        mapView.addOverlays(polylines)
    }
    
    func setPins(sections : [JSON]) {
        let endData = sections[sections.count - 1]["data"].arrayValue
        let startLocation = CLLocationCoordinate2D(latitude: sections[0]["data"][0]["location"]["latitude"].doubleValue, longitude: sections[0]["data"][0]["location"]["longitude"].doubleValue)
        let endLocation = CLLocationCoordinate2D(latitude: endData[endData.count - 1]["location"]["latitude"].doubleValue, longitude: endData[endData.count - 1]["location"]["longitude"].doubleValue)
        let pin1 = MapPin(title: "", locationName: "", coordinate: startLocation)
        let pin2 = EndMapPin(title: "", locationName: "", coordinate: endLocation)
        
        mapView.addAnnotations([pin1,pin2])
        
    }
    
    func sensedModePerSection (data : JSON) -> String {
        var sensed_mode : String?
        switch data["mode"].stringValue {
        case "UNKNOWN":
            sensed_mode = "UNKNOWN"
        case "CAR":
            sensed_mode = "CAR"
        case ".WALKING":
            sensed_mode = "WALKING"
        case "BICYCLING":
            sensed_mode = "BICYCLING"
        case "BUS":
            sensed_mode = "BUS"
        case "TRAIN":
            sensed_mode = "TRAIN"
        case "AIR_OR_HSR":
            sensed_mode = "AIR_OR_HSR"
        case "SUBWAY":
            sensed_mode = "SUBWAY"
        case "TRAM":
            sensed_mode = "TRAM"
        default:
            sensed_mode = "UNKNOWN"
        }
        return sensed_mode!
    }
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        print("polyline count = ", polylines.count)
        if let polyline = overlay as? MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: polyline)
            polylineRenderer.strokeColor = .lightGray
            polylineRenderer.lineWidth = 3
            return polylineRenderer
        }
        
        return MKOverlayRenderer(overlay: overlay)
    }
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let Identifier = "Pin"
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: Identifier) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: Identifier)
        
        annotationView.canShowCallout = true
        if annotation is MKUserLocation {
            return nil
        } else if annotation is MapPin {
            let mapPinImage =  UIImage(named: "map-pin-s")
            let size = CGSize(width: 11, height: 19)
            UIGraphicsBeginImageContext(size)
            mapPinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            annotationView.image = resizedImage
            annotationView.layer.anchorPoint = CGPoint(x: 0.5 ,y: 1.0)
            return annotationView
        } else if annotation is EndMapPin {
            let mapPinImage =  UIImage(named: "Page-1")
            let size = CGSize(width: 11, height: 19)
            UIGraphicsBeginImageContext(size)
            mapPinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            annotationView.image = resizedImage
            annotationView.layer.anchorPoint = CGPoint(x: 0.0 ,y: 1.0)
            return annotationView
        } else {
            return nil
        }
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        let currstate = TripDiaryStateMachine.getStateName(tripDiaryStateMachine!.currState)
        if(currstate == "STATE_ONGOING_TRIP") {
            self.mapView.setCenter(userLocation.location!.coordinate, animated: true)
        }
    }
    
    
    
    @objc func didPullToRefresh() {
        getStatsPoints()
        getStats(filter: "?days=5")
        mapView.removeOverlays(polylines)
        
        getCurrentState(didPullToRefresh: true)
    }
}

extension DashboardViewController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print(tabBarController.selectedIndex)
        self.scrollView.setContentOffset(.zero, animated: true)
        
    }
}


