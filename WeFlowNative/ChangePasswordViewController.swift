//
//  ChangePasswordViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 10/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import Alamofire

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var newPasswordTextField: CustomTextField!
    @IBOutlet weak var confirmNewPasswordTextField: CustomTextField!
    @IBOutlet weak var navView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func confirmButtonTapped(_ sender: Any) {
        if(confirmNewPasswordTextField.text != newPasswordTextField.text){
            
        } else {
            guard let clientInfo = plistValues(bundle: Bundle.main) else { return }
            let headers: HTTPHeaders = [
                "authorization": "Bearer " + UserDefaults.standard.string(forKey: "AccessToken")!,
                "content-type": "application/json"
            ]
            let parameters = [
                "picture": "https://lh3.googleusercontent.com/-N1GHZmUkLmg/WRbeVFmmNfI/AAAAAAAAA-8/mVN9xSFs2-wRXLRuJn22LtvJxtb9_b1WACEwYBhgLKtMDAL1OcqyoK4M87PA7L_0f2s2ZeDSyzAmUBC73T0D1HfzzUzhAajXNDK0_SZ-worz3t5PqPAfSGlIkFd2oGyrS3CpzVkyKM_guthr0aFYR0wlB_6iPkg3ntWrebW5Z--ELjoe6kwPk9umLNkKbezPtz3PCiSW00dSBMBi35RIvIl4OYLhLLtV3rN7TMIel-FXNllaVptZ6wDbRMW543RDiXO_7bEmQEVysFQdmC2K5clhjVpvb9AfdVuO505vn9F6iA2oVFtuTwYHwFK6zeFvqj--XvYAeR7DCzrtuF_9ySbW_ofqe1MufZPmnHj9SrXQAaSTgVIfepXcxdOeSZCLAFnH_6ikHEoYRqCDMWkEICNIeDYx3zougYqem84mHuhguzXTeFgnrHxXn_DoxqxuAO33eWeGzrxBDlK2oJkJBXhMzJZ65ze-xh9EpmteYM3XA0bwUDYcIoO9ikcM6541dC8gAKnUFlzl7GUvX0GRRsYFlte7qN__VJgsIOOnEtbO0xPLOBfF71xb_JsKYdjJrRTVbJ2_Tc-duMlnabXL7RBIGGJ1_WAtM404mvc_MhTDUzLthnYg9qUeMs_jVpMuh3l_w9lgYxii9ajfF2datm9YJPB8w293n-gU/w280-h280-p/IMG_1325.JPG",
//              "connection": "Username-Password-Authentication"
            ] as [String : Any]
            let originalString = "https://" + clientInfo.domain + "/api/v2/users/" + UserDefaults.standard.string(forKey: "sub")!
            let urlString = originalString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            
            AF.request(urlString!, method: .patch, parameters: parameters, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON { response in
                print(response)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
