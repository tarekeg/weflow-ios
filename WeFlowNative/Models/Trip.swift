//
//  Trip.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 30/06/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import Foundation

struct Trip: Encodable {
    let tripId : String
    let sections : [Section]
    let totalDistance : Double
}

struct Section: Encodable {
    let tripId : String
    let sectionId : String
    let mode : String
    let startTs : Double
    let endTs : Double
    let data : [TripData]
    let speeds : [Double]
    let distances : [Double]
}

struct TripData: Encodable {
    let sectionId : String
    let speed : Double
    let distance : Double
    let location : Location
}

struct Location: Encodable {
    let latitude : Double
    let longitude : Double
}



