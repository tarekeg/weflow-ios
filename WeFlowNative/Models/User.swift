//
//  User.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 22/10/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    var firstname : String
    var lastname : String
    var gender : String
    var birthDate : String
    var street : String
    var zipCode : String
    var city : String
    var weight : String
    
    init(firstname : String, lastname : String, birthDate : String, street : String, zipCode : String, city : String, gender : String, weight : String) {
        self.firstname = firstname
        self.lastname = lastname
        self.birthDate = birthDate
        self.gender = gender
        self.street = street
        self.zipCode = zipCode
        self.city = city
        self.weight = weight
    }
}

struct UserTripData {
    static var userTrips : JSON = []
    static var progFid = ""
}

struct firstTimeLunch {
    static var showWeightPopupEnabled : Bool = true
}

struct Point {
    static var popupStateDisabled : Bool = false
    static var pro : String = "0 Point"
    static var standard : String = "0 Point"
}
