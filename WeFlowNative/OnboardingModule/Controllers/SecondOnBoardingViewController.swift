//
//  OnboardingDescriptionViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 09/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit

class SecondOnBoardingViewController: UIViewController, UIGestureRecognizerDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    @IBAction func presentSecondOnboardingDescription(_ sender: Any) {
        if let vc = UIStoryboard(name: "Authentication", bundle: nil).instantiateViewController(withIdentifier: "navControllerLogin") as? UINavigationController {
            let secondOnboardingVC = vc.viewControllers.first as! DescriptionViewController
            secondOnboardingVC.backButtonIsHidden = false
            present(vc, animated: true, completion: nil)
        }
    }
    
    
}
