//
//  Environment.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 04/11/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import Foundation

// MARK: - Keys
enum Keys {
  enum Plist {
    static let emissionURL = "EMISSION_URL"
    static let wefloApi = "WEFLO_API"
    static let irebyWebURL = "IREBY_WEB_URL"
  }
}

// MARK: - Plist
public enum Environment {
  private static let infoDictionary: [String: Any] = {
    guard let dict = Bundle.main.infoDictionary else {
      fatalError("Plist file not found")
    }
    return dict
  }()
    // MARK: - PlistValues
  static let emissionUrl: String = {
    guard let rootEmissionURL = Environment.infoDictionary[Keys.Plist.emissionURL] as? String else {
      fatalError("Emission URL not set in plist for this environment")
    }
    return rootEmissionURL
  }()

  static let wefloApi: String = {
    guard let wefloApiURL = Environment.infoDictionary[Keys.Plist.wefloApi] as? String else {
      fatalError("WefloApi URL not set in plist for this environment")
    }
    return wefloApiURL
  }()


static let irebyWebUrl: String = {
    guard let rootIrebyURL = Environment.infoDictionary[Keys.Plist.irebyWebURL] as? String else {
    fatalError("WefloApi URL not set in plist for this environment")
  }
  return rootIrebyURL
}()
}
