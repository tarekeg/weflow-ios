//
//  AlertResetPasswordSuccessViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 27/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit

class AlertResetPasswordSuccessViewController: UIViewController {

    @IBOutlet weak var crossView: UIView!
    @IBOutlet weak var alertView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.layer.cornerRadius = 10.0
        crossView.layer.cornerRadius = 10.0
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
