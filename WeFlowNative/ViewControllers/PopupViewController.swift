//
//  PopupViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 03/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

protocol PopupViewControllerDelegate: class {
    func validateTapped(firstDate : String, lastDate : String)
}

class PopupViewController: UIViewController, CalendarViewControllerDelegate {
    
    func getDate(date: String) {
        firstDateTextField.text = date
    }
    func getDate2(date: String) {
        finalDateTextField.text = date
    }
    
    
    static let identifier = "PopupViewController"
    weak var delegate : PopupViewControllerDelegate?
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var finalDateTextField: CustomTextField!
    @IBOutlet weak var firstDateTextField: CustomTextField!
    
    let alertService = AlertService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        popupView.layer.cornerRadius = 10.0
        finalDateTextField.tintColor = .clear
        firstDateTextField.tintColor = .clear
        finalDateTextField.inputView = UIView()
        firstDateTextField.inputView = UIView()
        finalDateTextField.inputAccessoryView = UIView()
        firstDateTextField.inputAccessoryView = UIView()
        finalDateTextField.tintColor = .white
        firstDateTextField.tintColor = .white
        firstDateTextField.addTarget(self, action: #selector(showCalendar), for: .touchDown)
        finalDateTextField.addTarget(self, action: #selector(showCalendar2), for: .touchDown)
    }
    
    @objc func showCalendar () {
        
        performSegue(withIdentifier: "toCalendar", sender: nil)
        
    }
    @objc func showCalendar2 () {
           
           performSegue(withIdentifier: "toCalendar2", sender: nil)
           
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCalendar" {
            if let destinationVC = segue.destination as? CalendarViewController {
                destinationVC.delegate = self
                destinationVC.textFieldNumber = 1
                destinationVC.date = firstDateTextField.text 
            }
        }
        if segue.identifier == "toCalendar2" {
            if let destinationVC = segue.destination as? CalendarViewController {
                destinationVC.delegate = self
                destinationVC.textFieldNumber = 2
                destinationVC.date = finalDateTextField.text 
            }
        }
    }
    
    func getDateFromString(strDate : String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: strDate)
    }
    
    func compareDate (firstDate : String, finalDate : String) -> Bool {
        let formattedFirstDate = getDateFromString(strDate: firstDate)
        let formattedFinalDate = getDateFromString(strDate: finalDate)
        let calendar = Calendar.current

        let date1 = calendar.startOfDay(for: formattedFirstDate!)
        let date2 = calendar.startOfDay(for: formattedFinalDate!)

        let components = calendar.dateComponents([.day], from: date1, to: date2)
        if(formattedFinalDate! > formattedFirstDate!) && components.day! <= 40  {
            return true
        } else {
            return false
        }
    }



    
    @IBAction func applyTapped(_ sender: Any) {
        if(firstDateTextField.text == "" || finalDateTextField.text == "" || !compareDate(firstDate: firstDateTextField.text!, finalDate: finalDateTextField.text!)){
            
            let alert = alertService.alert(title: NSLocalizedString("popupview-title",comment : ""), body: NSLocalizedString("popupview-body",comment : ""), buttonTitle: NSLocalizedString("popupview-button",comment : "")) {
            }
            self.present(alert, animated: true, completion: nil)
        } else {
            self.delegate?.validateTapped(firstDate: firstDateTextField.text!, lastDate: finalDateTextField.text!)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
 
 

}
