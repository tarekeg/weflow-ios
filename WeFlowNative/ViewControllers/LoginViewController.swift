//
//  LoginViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 22/07/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import Auth0
import Alamofire
import SwiftyJSON
import Firebase

class LoginViewController : UIViewController {

    
    @IBOutlet weak var startButton: UIButton!
    
    let credentialsManager = CredentialsManager(authentication: Auth0.authentication())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        credentialsManager.clear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        startButton.backgroundColor = K.Color.primaryColor
    }
    
    func checkFCMToken () {        
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching FCM registration token: \(error)")
            } else if let token = token {
                print("FCM registration token: \(token)")
                UserDefaults.standard.set(token, forKey: "FCMToken")
            }
        }
    }
    

    


}


