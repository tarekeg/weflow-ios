//
//  locationPermissionViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 12/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import CoreLocation

class locationPermissionViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var descriptionLocationTextView: UILabel!
    @IBOutlet weak var toSettingsButton: UIButton!
    private let locationManager = CLLocationManager()
    var locationIsAlwaysAvailable = false
    
    
    let serviceAlert = AlertService()
    override func viewDidLoad() {
        super.viewDidLoad()
        setAlwaysToBold()
        getLocationStatus()
    }
    
    
    func setAlwaysToBold () {
        descriptionLocationTextView.text = NSLocalizedString("share-location-description", comment: "")
        descriptionLocationTextView.font = UIFont(name: "Avenir-Roman", size: 13)
        descriptionLocationTextView.changeFont(ofText: NSLocalizedString("always", comment: ""), with: UIFont(name: "Avenir-Heavy", size: 13)!)
    }
    
    func setButtonFunction () {
        if UserDefaults.standard.bool(forKey: "hasAskedLocationRequest") {
            toSettingsButton.setTitle(NSLocalizedString("go-to-appsettings", comment: ""), for: .normal)
        } else {
            toSettingsButton.setTitle(NSLocalizedString("share-location", comment: ""), for: .normal)
        }
    }
    
    @IBAction func toSettingsTapped(_ sender: Any) {
        if(toSettingsButton.title(for: .normal) == NSLocalizedString("go-to-appsettings", comment: "")) {
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
                
            }
        } else {
            locationManager.requestAlwaysAuthorization()
            UserDefaults.standard.set(true,forKey: "hasAskedLocationRequest")
            
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(getLocationStatus), name: UIApplication.didBecomeActiveNotification, object: nil)
        setButtonFunction()
    }
    

    func isInitEnabled() -> Bool {
        if CMMotionActivityManager.authorizationStatus().rawValue != 3 {
            return false
        } else {
            return true
        }
    }
    
    @objc func getLocationStatus () {
        if CLLocationManager.locationServicesEnabled() {
            if #available(iOS 14.0, *) {
                switch locationManager.authorizationStatus {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    locationIsAlwaysAvailable = false
                    print(locationIsAlwaysAvailable)
                case .authorizedAlways:
                    locationIsAlwaysAvailable = true
                    print(locationIsAlwaysAvailable)
                @unknown default:
                    break
                }
            } else {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied, .authorizedWhenInUse:
                    locationIsAlwaysAvailable = false
                    print(locationIsAlwaysAvailable)
                case .authorizedAlways:
                    locationIsAlwaysAvailable = true
                    print(locationIsAlwaysAvailable)
                @unknown default:
                    break
                }
            }
        } else {
            print("Location services are not enabled")
        }
        if locationIsAlwaysAvailable && !isInitEnabled() {
            DispatchQueue.main.async {
                NotificationCenter.default.removeObserver(UIApplication.didBecomeActiveNotification)
                self.performSegue(withIdentifier: "toMotion", sender: nil)
            }
           
            
        }
        if locationIsAlwaysAvailable && isInitEnabled() {
            DispatchQueue.main.async {
                NotificationCenter.default.removeObserver(UIApplication.didBecomeActiveNotification)
                if UserTripData.userTrips == [] {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "progressDownloadView")
                    UIApplication.shared.windows.first?.rootViewController = vc
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    self.present(vc, animated: true)
                } else {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "tabBarController")
                    UIApplication.shared.windows.first?.rootViewController = vc
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    self.present(vc, animated: true)
                }

//                self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
            
        } else  {
            setButtonFunction()
        }
        
    }
    
    

 




}

public extension String {

    func range(ofText text: String) -> NSRange {
        let fullText = self
        let range = (fullText as NSString).range(of: text)
        return range
    }
}

public protocol ChangableFont: AnyObject {
    var rangedAttributes: [RangedAttributes] { get }
    func getText() -> String?
    func set(text: String?)
    func getAttributedText() -> NSAttributedString?
    func set(attributedText: NSAttributedString?)
    func getFont() -> UIFont?
    func changeFont(ofText text: String, with font: UIFont)
    func changeFont(inRange range: NSRange, with font: UIFont)
    func changeTextColor(ofText text: String, with color: UIColor)
    func changeTextColor(inRange range: NSRange, with color: UIColor)
    func resetFontChanges()
}

public struct RangedAttributes {

    public let attributes: [NSAttributedString.Key: Any]
    public let range: NSRange

    public init(_ attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
        self.attributes = attributes
        self.range = range
    }
}

extension UILabel: ChangableFont {

    public func getText() -> String? {
        return text
    }

    public func set(text: String?) {
        self.text = text
    }

    public func getAttributedText() -> NSAttributedString? {
        return attributedText
    }

    public func set(attributedText: NSAttributedString?) {
        self.attributedText = attributedText
    }

    public func getFont() -> UIFont? {
        return font
    }
}

public extension ChangableFont {

    var rangedAttributes: [RangedAttributes] {
        guard let attributedText = getAttributedText() else {
            return []
        }
        var rangedAttributes: [RangedAttributes] = []
        let fullRange = NSRange(
            location: 0,
            length: attributedText.string.count
        )
        attributedText.enumerateAttributes(
            in: fullRange,
            options: []
        ) { (attributes, range, stop) in
            guard range != fullRange, !attributes.isEmpty else { return }
            rangedAttributes.append(RangedAttributes(attributes, inRange: range))
        }
        return rangedAttributes
    }

    func changeFont(ofText text: String, with font: UIFont) {
        guard let range = (self.getAttributedText()?.string ?? self.getText())?.range(ofText: text) else { return }
        changeFont(inRange: range, with: font)
    }

    func changeFont(inRange range: NSRange, with font: UIFont) {
        add(attributes: [.font: font], inRange: range)
    }

    func changeTextColor(ofText text: String, with color: UIColor) {
        guard let range = (self.getAttributedText()?.string ?? self.getText())?.range(ofText: text) else { return }
        changeTextColor(inRange: range, with: color)
    }

    func changeTextColor(inRange range: NSRange, with color: UIColor) {
        add(attributes: [.foregroundColor: color], inRange: range)
    }

    private func add(attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
        guard !attributes.isEmpty else { return }

        var rangedAttributes: [RangedAttributes] = self.rangedAttributes

        var attributedString: NSMutableAttributedString

        if let attributedText = getAttributedText() {
            attributedString = NSMutableAttributedString(attributedString: attributedText)
        } else if let text = getText() {
            attributedString = NSMutableAttributedString(string: text)
        } else {
            return
        }

        rangedAttributes.append(RangedAttributes(attributes, inRange: range))

        rangedAttributes.forEach { (rangedAttributes) in
            attributedString.addAttributes(
                rangedAttributes.attributes,
                range: rangedAttributes.range
            )
        }

        set(attributedText: attributedString)
    }

    func resetFontChanges() {
        guard let text = getText() else { return }
        set(attributedText: NSMutableAttributedString(string: text))
    }
}
