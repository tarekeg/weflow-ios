//
//  DetailTripViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 20/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import SwiftyJSON
import MapKit
import SwiftMoment
import Kingfisher
import Alamofire
import Sentry

protocol DetailTripViewControllerDelegate {
    func confirmTapped(indexPath : IndexPath, confirmedSections : JSON, standardPoints : String, proPoints : String)
}

class DetailTripViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mapView: MKMapView!
    var location : [CLLocationCoordinate2D] = []
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var declareButton: LoadingButton!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var detailTripLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var profileImageButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var standardPointsLabel: UILabel!
    @IBOutlet weak var proPointsLabel: UILabel!
    @IBOutlet weak var caloriesLabel: UILabel!
    @IBOutlet weak var co2Label: UILabel!
    
    var firstTime : Bool = true
    var delegate : DetailTripViewControllerDelegate?
    var shouldCellBeExpanded:Bool = false
    var indexOfExpendedCell:NSInteger = -1
    var polyCar : MKPolyline?
    var polyWalk : MKPolyline?
    var polyBicycle : MKPolyline?
    var polylines : [MKPolyline] = []
    var tripData : JSON?
    var indexPath : IndexPath?
    var numberOfSection = 0
    var sections : [JSON] = []
    var startEndLocation : [Double] = []
    var expandedIndex : [Int:Bool] = [:]
    var expandedCarIndex : [Int:Bool] = [:]
    var didEndDisplaying = false
    var sectionsToSync : [JSON] = []
    var tripIsConfirmed : Bool?
    var total = 0.0
    let RF = ReusableFunctions()
    let alertService = AlertService()
    var mode = ""
    var index = 0
    var proPoints = "0"
    var standardPoints = "0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSectionsFromServer(jsonArray: self.tripData!)
        if UserDefaults.standard.bool(forKey: "ireby_synchronization") == false {
            declareButton.isEnabled = false
            declareButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
            declareButton.setTitle("Votre compte weflo n'est pas synchronisé", for: .normal)
        }
        getTripData()
        profileImageButton.layoutIfNeeded()
        profileImageButton.subviews.first?.contentMode = .scaleAspectFill
        let resource = ImageResource(downloadURL: URL(string: UserDefaults.standard.string(forKey: "pictureUrl")!)! , cacheKey: "my_avatar")
        profileImageButton.kf.setBackgroundImage(with: resource, for: .normal)
        setUI()
        setPointLabelText(proPoints: tripData!["pro_points"].stringValue, standardPoints: tripData!["standard_points"].stringValue)
        
        
        
        
        if tripData!["is_confirmed"].boolValue {
            declareButton.setTitle(NSLocalizedString("trip-validated", comment: ""), for: .normal)
            declareButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
            declareButton.isEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [self] in
                
                self.setPins(sections: sections)
                self.setupMapKit()
                if let first = self.mapView.overlays.first {
                    let rect = self.mapView.overlays.reduce(first.boundingMapRect, {$0.union($1.boundingMapRect)})
                    self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                }
            }
        } else {
            
            getSectionsToSync()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [self] in
                
                self.setPins(sections: sections)
                self.setupMapKitFromServer()
                if let first = self.mapView.overlays.first {
                    let rect = self.mapView.overlays.reduce(first.boundingMapRect, {$0.union($1.boundingMapRect)})
                    self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0), animated: true)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.1) { [self] in
                        let bottomOffset = CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.bounds.height + scrollView.contentInset.bottom)
                        scrollView.setContentOffset(bottomOffset, animated: true)
                    }
                }
            }
        }
        
        setDesign()
        tableView.register(UINib(nibName: "SectionModeTableViewCell", bundle: nil), forCellReuseIdentifier: "SectionModeTableViewCell")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableHeight?.constant = self.tableView.contentSize.height
    }
    
    func getTripData () {
        let tripId = tripData!["tripId"].stringValue
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "Token")!,
            "content-type": "application/json"
        ]
        var request = URLRequest(url: URL(string: K.Path.cleverApi + "trips/" + tripId)!)
        request.headers = headers
        request.httpMethod = "GET"
        AF.request(request).responseJSON { response in
            let jsonResponse = JSON(response.value)
            print("my tripdata response : ",jsonResponse)
            self.caloriesLabel.text = jsonResponse["calories"].stringValue + " KCal"
            self.co2Label.text = String(format: "%.0f",jsonResponse["carbonDioxide"].doubleValue) + " G"
        }
    }
    
    func getSectionsToSync () {
        var section : [String:Any] = [:]
        for i in 0...sections.count - 1 {
            section["sectionId"] = sections[i]["sectionId"].stringValue
            section["mode"] = sensedOriginalModePerSection(transportMode: sections[i]["mode"].stringValue)
            sectionsToSync.append(JSON(section))
        }
    }
    
    func setPointLabelText(proPoints : String, standardPoints : String) {
        standardPointsLabel.text = standardPoints + " Pts"
        proPointsLabel.text = proPoints + " Pts"
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polyline = overlay as? MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: polyline)
            if polyline.title == "CAR" {
                polylineRenderer.strokeColor = K.Color.CAR
            } else if polyline.title == "WALKING" {
                polylineRenderer.strokeColor = K.Color.WALKING
            } else if polyline.title == "BICYCLING" {
                polylineRenderer.strokeColor = K.Color.BICYCLING
            } else if polyline.title == "TRAM" {
                polylineRenderer.strokeColor = K.Color.TRAM
            } else if polyline.title == "SUBWAY" {
                polylineRenderer.strokeColor = K.Color.SUBWAY
            } else if polyline.title == "TRAIN" {
                polylineRenderer.strokeColor = K.Color.TRAIN
            } else if polyline.title == "AIR_OR_HSR" {
                polylineRenderer.strokeColor = K.Color.AIR
            } else if polyline.title == "BUS" {
                polylineRenderer.strokeColor = K.Color.BUS
            } else if polyline.title == "CARPOOLING" {
                polylineRenderer.strokeColor = K.Color.CARPOOLING
            } else if polyline.title == "ESCOOTER" {
                polylineRenderer.strokeColor = K.Color.ESCOOTER
            } else if polyline.title == "MOTORBIKE" {
                polylineRenderer.strokeColor = K.Color.MOTO
            } else if polyline.title == "OTHER" {
                polylineRenderer.strokeColor = K.Color.OTHER
            } else if polyline.title == "UNKNOWN" {
                polylineRenderer.strokeColor = K.Color.OTHER
            }
            else {
                polylineRenderer.strokeColor = K.Color.OTHER
            }
            polylineRenderer.lineWidth = 3
            return polylineRenderer
        }
        
        return MKOverlayRenderer(overlay: overlay)
        
    }
    
    
    
    
    
    @IBAction func dismissTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    func setupMapKitFromServer () {
        mapView.delegate = self
        polylines = []
        for i in 0...sections.count - 1 {
            let data = sections[i]["data"].arrayValue
            location = []
            if data.count != 0 {
                if sections[i]["mode"].stringValue == "CAR" {
                    for j in 0...data.count - 1 {
                        
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let polyline = MKPolyline(coordinates: &location, count: location.count)
                    polyline.title = "CAR"
                    polylines.append(polyline)
                } else if(sensedModePerSection(data: sections[i]) == "WALKING") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let walkPolyline = MKPolyline(coordinates: &location, count: location.count)
                    walkPolyline.title = "WALKING"
                    polylines.append(walkPolyline)
                    
                } else if(sensedModePerSection(data: sections[i]) == "BICYCLING") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let bicyclePolyline = MKPolyline(coordinates: &location, count: location.count)
                    bicyclePolyline.title = "BICYCLING"
                    polylines.append(bicyclePolyline)
                    
                }  else if(sensedModePerSection(data: sections[i]) == "BUS") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let busPolyline = MKPolyline(coordinates: &location, count: location.count)
                    busPolyline.title = "BUS"
                    polylines.append(busPolyline)
                }  else if(sensedModePerSection(data: sections[i]) == "TRAIN") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let trainPolyline = MKPolyline(coordinates: &location, count: location.count)
                    trainPolyline.title = "TRAIN"
                    polylines.append(trainPolyline)
                }  else if(sensedModePerSection(data: sections[i]) == "AIR_OR_HSR") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let airPolyline = MKPolyline(coordinates: &location, count: location.count)
                    airPolyline.title = "AIR_OR_HSR"
                    polylines.append(airPolyline)
                } else if(sensedModePerSection(data: sections[i]) == "SUBWAY") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let subwayPolyline = MKPolyline(coordinates: &location, count: location.count)
                    subwayPolyline.title = "SUBWAY"
                    polylines.append(subwayPolyline)
                } else if(sensedModePerSection(data: sections[i]) == "TRAM") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let tramPolyline = MKPolyline(coordinates: &location, count: location.count)
                    tramPolyline.title = "TRAM"
                    polylines.append(tramPolyline)
                } else if(sensedModePerSection(data: sections[i]) == "UNKNOWN") {
                    for j in 0...data.count - 1 {
                        let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                        location.append(coord)
                    }
                    let unknownPolyline = MKPolyline(coordinates: &location, count: location.count)
                    unknownPolyline.title = "UNKNOWN"
                    polylines.append(unknownPolyline)
                }
            } else {
            }
        }
        mapView.addOverlays(polylines)
    }
    func setupMapKit () {
        mapView.delegate = self
        for i in 0...tripData!["sectionsFromIreby"].count - 1 {
            for j in 0...sections.count - 1 {
                if tripData!["sectionsFromIreby"][i]["sectionId"].stringValue == sections[j]["sectionId"].stringValue {
                    let data = sections[i]["data"].arrayValue
                    location = []
                    if data.count != 0 {
                        if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "car" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let polyline = MKPolyline(coordinates: &location, count: location.count)
                            polyline.title = "CAR"
                            polylines.append(polyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "walking" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let walkPolyline = MKPolyline(coordinates: &location, count: location.count)
                            walkPolyline.title = "WALKING"
                            polylines.append(walkPolyline)
                            
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "cycling" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let bicyclePolyline = MKPolyline(coordinates: &location, count: location.count)
                            bicyclePolyline.title = "BICYCLING"
                            polylines.append(bicyclePolyline)
                            
                        }  else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "bus" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let busPolyline = MKPolyline(coordinates: &location, count: location.count)
                            busPolyline.title = "BUS"
                            polylines.append(busPolyline)
                        }  else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "train" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let trainPolyline = MKPolyline(coordinates: &location, count: location.count)
                            polylines.append(trainPolyline)
                        }  else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "plane" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let airPolyline = MKPolyline(coordinates: &location, count: location.count)
                            airPolyline.title = "AIR_OR_HSR"
                            polylines.append(airPolyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "subway" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let subwayPolyline = MKPolyline(coordinates: &location, count: location.count)
                            polylines.append(subwayPolyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "tram" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let tramPolyline = MKPolyline(coordinates: &location, count: location.count)
                            tramPolyline.title = "TRAM"
                            polylines.append(tramPolyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "carpooling" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let carpoolingPolyline = MKPolyline(coordinates: &location, count: location.count)
                            carpoolingPolyline.title = "CARPOOLING"
                            polylines.append(carpoolingPolyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "e-scooter" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let escooterPolyline = MKPolyline(coordinates: &location, count: location.count)
                            escooterPolyline.title = "ESCOOTER"
                            polylines.append(escooterPolyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "motorbike" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let motorbikePolyline = MKPolyline(coordinates: &location, count: location.count)
                            motorbikePolyline.title = "MOTORBIKE"
                            polylines.append(motorbikePolyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "other" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let otherPolyline = MKPolyline(coordinates: &location, count: location.count)
                            otherPolyline.title = "OTHER"
                            polylines.append(otherPolyline)
                        } else if tripData!["sectionsFromIreby"][i]["mode"].stringValue == "unknown" {
                            for j in 0...data.count - 1 {
                                let coord = CLLocationCoordinate2D(latitude: data[j]["location"]["latitude"].doubleValue, longitude: data[j]["location"]["longitude"].doubleValue)
                                location.append(coord)
                            }
                            let unknownPolyline = MKPolyline(coordinates: &location, count: location.count)
                            unknownPolyline.title = "UNKNOWN"
                            polylines.append(unknownPolyline)
                        }
                    }
                }
            }
        }
        
        mapView.addOverlays(polylines)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let Identifier = "Pin"
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: Identifier) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: Identifier)
        
        annotationView.canShowCallout = true
        if annotation is MKUserLocation {
            return nil
        } else if annotation is MapPin {
            let mapPinImage =  UIImage(imageLiteralResourceName: "map-pin-s")
            let size = CGSize(width: 11, height: 19)
            UIGraphicsBeginImageContext(size)
            mapPinImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            annotationView.image = resizedImage
            annotationView.layer.anchorPoint = CGPoint(x: 0.5 ,y: 1.0)
            return annotationView
        } else if annotation is EndMapPin {
            let mapPinImage =  UIImage(imageLiteralResourceName: "Page-1")
            let size = CGSize(width: 11, height: 19)
            UIGraphicsBeginImageContext(size)
            mapPinImage.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            annotationView.image = resizedImage
            annotationView.layer.anchorPoint = CGPoint(x: 0.0 ,y: 1.0)
            return annotationView
        } else {
            return nil
        }
    }
    
    
    func setDesign() {
        declareButton.layer.cornerRadius = 10.0
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        containerView.backgroundColor = .clear
        containerView.layer.shadowRadius = 4
        containerView.layer.shadowOpacity = 0.5
        containerView.layer.shadowOffset = CGSize(width: 1, height: 1)
        containerView.layer.shadowColor = UIColor.gray.cgColor
        mapView.frame = containerView.bounds
        mapView.layer.cornerRadius = 10.0
        mapView.layer.masksToBounds = true
    }
    
    
    func setUI() {
        let date = RF.formatDateFromTimeStamp(ts: tripData!["sections"][0]["startTs"].doubleValue)
        dateLabel.text = date
        
        let duration = RF.formatDuration(sections[sections.count - 1]["endTs"].doubleValue - sections[0]["startTs"].doubleValue)
        let distance = RF.formatDistance(tripData!["totalDistance"].doubleValue)
        detailTripLabel.text = String(distance) + " km - " + duration
        
    }
    
    
    func getSectionsFromServer (jsonArray : JSON) {
        for i in 0...jsonArray["sections"].count - 1 {
            self.sections.append(jsonArray["sections"][i])
        }
    }
    
    func sensedModePerSection (data : JSON) -> String {
        var sensed_mode : String?
        switch data["mode"].stringValue {
        case "UNKNOWN":
            sensed_mode = "UNKNOWN"
        case "CAR":
            sensed_mode = "CAR"
        case "WALKING":
            sensed_mode = "WALKING"
        case "BICYCLING":
            sensed_mode = "BICYCLING"
        case "BUS":
            sensed_mode = "BUS"
        case "TRAIN":
            sensed_mode = "TRAIN"
        case "AIR_OR_HSR":
            sensed_mode = "AIR_OR_HSR"
        case "SUBWAY":
            sensed_mode = "SUBWAY"
        case "TRAM":
            sensed_mode = "TRAM"
        default:
            sensed_mode = "UNKNOWN"
        }
        return sensed_mode!
    }
    func sensedOriginalModePerSection (transportMode : String) -> String {
        
        switch transportMode{
        case "UNKNOWN":
            return "other"
        case "CAR":
            return "car"
        case "WALKING":
            return "walking"
        case "BICYCLING":
            return "cycling"
        case "BUS":
            return "bus"
        case "TRAIN":
            return "train"
        case "AIR_OR_HSR":
            return "plane"
        case "SUBWAY":
            return "subway"
        case "TRAM":
            return "tram"
        default:
            return "other"
        }
    }
    func sensedModePerSectionToSync (modeFR : String) -> String {
        var sensed_mode : String?
        switch modeFR {
        case NSLocalizedString("other",comment : ""):
            sensed_mode = "other"
        case NSLocalizedString("car",comment : ""):
            sensed_mode = "car"
        case NSLocalizedString("walk",comment : ""):
            sensed_mode = "walking"
        case NSLocalizedString("bicycle",comment : ""):
            sensed_mode = "cycling"
        case NSLocalizedString("bus",comment : ""):
            sensed_mode = "bus"
        case NSLocalizedString("train",comment : ""):
            sensed_mode = "train"
        case NSLocalizedString("plane",comment : ""):
            sensed_mode = "plane"
        case NSLocalizedString("subway",comment : ""):
            sensed_mode = "subway"
        case NSLocalizedString("tram",comment : ""):
            sensed_mode = "tram"
        case NSLocalizedString("carpooling", comment: ""):
            sensed_mode = "carpooling"
        case NSLocalizedString("e-scooter", comment: ""):
            sensed_mode = "e-scooter"
        case NSLocalizedString("motorbike", comment: ""):
            sensed_mode = "motorbike"
        default:
            sensed_mode = "other"
        }
        return sensed_mode!
    }
    
    func setPins(sections : [JSON]) {
        let endData = sections[sections.count - 1]["data"].arrayValue
        let startLocation = CLLocationCoordinate2D(latitude: sections[0]["data"][0]["location"]["latitude"].doubleValue, longitude: sections[0]["data"][0]["location"]["longitude"].doubleValue)
        let endLocation = CLLocationCoordinate2D(latitude: endData[endData.count - 1]["location"]["latitude"].doubleValue, longitude: endData[endData.count - 1]["location"]["longitude"].doubleValue)
        let pin1 = MapPin(title: "", locationName: "", coordinate: startLocation)
        let pin2 = EndMapPin(title: "", locationName: "", coordinate: endLocation)
        mapView.addAnnotations([pin1,pin2])
    }
    
    
    
    func refreshTableView() {
        let alert = alertService.alertWithCheckBox(title: NSLocalizedString("confirm-popup-title",comment : ""), body: NSLocalizedString("confirm-popup-body",comment : ""), buttonTitle: NSLocalizedString("confirm-popup-button",comment : "")) {
            if(Point.popupStateDisabled) {
                UserDefaults.standard.set(true, forKey: "isPopupDisabled")
            } else if (!Point.popupStateDisabled) {
                UserDefaults.standard.set(false, forKey: "isPopupDisabled")
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                self.declareButton.hideLoading()
                declareButton.setTitle(NSLocalizedString("trip-validated", comment: ""), for: .normal)
                declareButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
                declareButton.isEnabled = false
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
        expandedIndex.removeAll()
        expandedCarIndex.removeAll()
        tableView.beginUpdates()
        tableView.reloadData()
        tableView.endUpdates()
        mapView.removeOverlays(polylines)
        polylines = []
        setupMapKit()
        
        
        if !UserDefaults.standard.bool(forKey: "isPopupDisabled") {
            tabBarController?.present(alert, animated: true)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
                declareButton.hideLoading()
                declareButton.setTitle(NSLocalizedString("trip-validated", comment: ""), for: .normal)
                declareButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
                declareButton.isEnabled = false
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    func irebySyncSectionsRequest (parsedDataToSync : [Any]) {
        
        
        let irebyHeaders: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "content-type": "application/json"
        ]
        var request = URLRequest(url: URL(string: K.Path.baseURLWeb + "/api/pve/action/sections")!)
        request.headers = irebyHeaders
        request.httpMethod = "POST"
        let params : [String:Any] = ["tripId":tripData!["tripId"].stringValue, "sections" : parsedDataToSync]
        let serializedParams = try! JSONSerialization.data(withJSONObject: params)
        request.httpBody = serializedParams
        AF.request(request).responseJSON { [self] response in
            let responseJson = JSON(response.value)
            print(response.response?.statusCode)
            print(responseJson)
            print(NSString(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8.rawValue))
            print("bearer : ",UserDefaults.standard.string(forKey: "ireby_access_token")!)
            if(response.response?.statusCode == 401){
                RF.renewIrebyToken { [self] (success) -> Void in
                    if success {
                        irebySyncSectionsRequest(parsedDataToSync: parsedDataToSync)
                    }
                }
            }
            if(response.response?.statusCode == 200) {
                
                setPointLabelText(proPoints: responseJson["proPoints"].stringValue, standardPoints: responseJson["standardPoints"].stringValue)
                self.proPoints = responseJson["proPoints"].stringValue
                self.standardPoints = responseJson["standardPoints"].stringValue
                tripData!["is_confirmed"].boolValue = true
                for i in 0...responseJson["sections"].count - 1 {
                    tripData!["sectionsFromIreby"][i]["standardPoints"].stringValue = responseJson["sections"][i]["standardPoints"].stringValue
                    tripData!["sectionsFromIreby"][i]["proPoints"].stringValue = responseJson["sections"][i]["proPoints"].stringValue
                    tripData!["sectionsFromIreby"][i]["sectionId"].stringValue = responseJson["sections"][i]["sectionId"].stringValue
                    UserTripData.userTrips[index]["sectionsFromIreby"][i]["standardPoints"].stringValue = responseJson["sections"][i]["standardPoints"].stringValue
                    UserTripData.userTrips[index]["sectionsFromIreby"][i]["proPoints"].stringValue = responseJson["sections"][i]["proPoints"].stringValue
                    UserTripData.userTrips[index]["sectionsFromIreby"][i]["sectionId"].stringValue = responseJson["sections"][i]["sectionId"].stringValue
                    
                }
                DispatchQueue.main.async { [self] in
                    self.delegate?.confirmTapped(indexPath: self.indexPath!, confirmedSections: tripData!["sectionsFromIreby"], standardPoints: self.standardPoints, proPoints: self.proPoints)
                    let params : [String:Any] = ["tripId":tripData!["tripId"].stringValue, "sections" : parsedDataToSync]
                    let serializedParams = try! JSONSerialization.data(withJSONObject: params)
                    let headers: HTTPHeaders = [
                        "authorization": "Bearer " + UserDefaults.standard.string(forKey: "Token")!,
                        "content-type": "application/json"
                    ]
                    var requestToWefloApi = URLRequest(url: URL(string: K.Path.cleverApi + "trips/sections/confirm")!)
                    requestToWefloApi.httpMethod = "PUT"
                    requestToWefloApi.headers = headers
                    requestToWefloApi.httpBody = serializedParams
                    AF.request(requestToWefloApi).responseJSON { [self] response in
                        getTripData()
                        refreshTableView()
                    }
                }
                
                
            } else {
                
                SentrySDK.capture(message: "Something went wrong with IrebyApi : trips/sections/confirm")
                let alert = alertService.alert(title: NSLocalizedString("error-popup-title",comment : ""), body: NSLocalizedString("error-popup-body",comment : ""), buttonTitle: NSLocalizedString("error-popup-button",comment : "")) {
                    self.declareButton.hideLoading()
                }
                tabBarController?.present(alert, animated: true)
            }
            
        }
    }
    
    
    @IBAction func declareModeTapped(_ sender: Any) {
        
        declareButton.showLoading()
        var id = "0"
        var parsedDataToSync : [Any] = []
        var section : [String:Any] = [:]
        for i in 0...sectionsToSync.count - 1 {
            section["id"] = sectionsToSync[i]["sectionId"].stringValue
            section["mode"] = sectionsToSync[i]["mode"].stringValue
            section["sectionId"] = sectionsToSync[i]["sectionId"].stringValue
            section["transport"] = sectionsToSync[i]["mode"].stringValue
            section["mode"] = sectionsToSync[i]["mode"].stringValue
            parsedDataToSync.append(section)
            
            for j in 0...UserTripData.userTrips.count - 1 {
                if(UserTripData.userTrips[j]["tripId"].stringValue == tripData!["tripId"].stringValue) {
                    index = j
                    id = UserTripData.userTrips[j]["tripId"].stringValue
                    if(UserTripData.userTrips[j]["sectionsFromIreby"].count == 0){
                        let alert = alertService.alert(title: NSLocalizedString("error-popup-title",comment : ""), body: NSLocalizedString("error-popup-body",comment : ""), buttonTitle: NSLocalizedString("error-popup-button",comment : "")) {
                            self.declareButton.hideLoading()
                        }
                        tabBarController?.present(alert, animated: true)
                        break
                    } else {
                        for k in 0...UserTripData.userTrips[j]["sections"].count - 1 {
                            UserTripData.userTrips[j]["sectionsFromIreby"][k]["sectionId"].stringValue = sectionsToSync[i]["sectionId"].stringValue
                            UserTripData.userTrips[j]["sectionsFromIreby"][k]["mode"].stringValue = sectionsToSync[i]["mode"].stringValue
                            tripData!["sectionsFromIreby"][i]["sectionId"].stringValue = sectionsToSync[i]["sectionId"].stringValue
                            tripData!["sectionsFromIreby"][i]["mode"].stringValue = sectionsToSync[i]["mode"].stringValue
                        }
                        
                    }
                }
            }
        }
        irebySyncSectionsRequest(parsedDataToSync: parsedDataToSync)
    }
}


extension DetailTripViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    @objc func expendButtonAction(button : UIButton) {
        
        if let val = expandedIndex[button.tag] {
            expandedIndex[button.tag] = !val
        } else {
            expandedIndex[button.tag] = true
        }
        let countedSet = NSCountedSet()
        for (_, value) in expandedIndex {
            countedSet.add(value)
        }
        self.tableView.reloadData()
        self.tableView.layoutIfNeeded()
        self.tableHeight.constant = self.tableView.contentSize.height
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SectionModeTableViewCell", for: indexPath) as! SectionModeTableViewCell
        cell.delegate = self
        
        cell.button.tag = indexPath.row
        cell.carpoolingButton.tag = indexPath.row
        cell.carpoolingCheckBoxImageView.tag = indexPath.row
        cell.button.addTarget(self, action: #selector(expendButtonAction), for: UIControl.Event.touchUpInside)
        let section = sections[indexPath.row]
        if tripData!["is_confirmed"].boolValue {
            cell.tableView.isHidden = true
            cell.buttonView.isHidden = true
            cell.carpoolingView.isHidden =  true
            for i in 0...tripData!["sections"].count - 1 {
                if(section["sectionId"].stringValue == tripData!["sectionsFromIreby"][i]["sectionId"].stringValue) {
                    if tripData!["sectionsFromIreby"][i]["standardPoints"].intValue + tripData!["sectionsFromIreby"][i]["proPoints"].intValue != 0 {
                        cell.pointsEarnedByModeLabel.text = "+ " + String(tripData!["sectionsFromIreby"][i]["standardPoints"].intValue + tripData!["sectionsFromIreby"][i]["proPoints"].intValue) + " points"
                        cell.pointsEarnedByModeLabel.isHidden = false
                        
                    }
                    switch tripData!["sectionsFromIreby"][i]["mode"].stringValue {
                    case "unknown":
                        cell.sectionImageView.image = UIImage(named: "other")
                    case "bus":
                        cell.sectionImageView.image = UIImage(named: "bus-stop")
                    case "car":
                        cell.sectionImageView.image = UIImage(named: "car-alt")
                    case "cycling":
                        cell.sectionImageView.image = UIImage(named: "bike")
                    case "walking":
                        cell.sectionImageView.image = UIImage(named: "walk")
                    case "subway":
                        cell.sectionImageView.image = UIImage(named: "subway")
                    case "train":
                        cell.sectionImageView.image = UIImage(named: "train")
                    case "tram":
                        cell.sectionImageView.image = UIImage(named: "tram")
                    case "plane":
                        cell.sectionImageView.image = UIImage(named: "airplane")
                    case "carpooling":
                        cell.sectionImageView.image = UIImage(named: "carpooling")
                    case "e-scooter":
                        cell.sectionImageView.image = UIImage(named: "escooter")
                    case "motorbike":
                        cell.sectionImageView.image = UIImage(named: "motorbike")
                    case "other":
                        cell.sectionImageView.image = UIImage(named: "other")
                    default:
                        cell.sectionImageView.image = UIImage(named: "other")
                    }
                }
            }
            
            
        } else if firstTime {
            switch sensedModePerSection(data: section) {
            case "UNKNOWN":
                cell.sectionImageView.image = UIImage(named: "other")
                cell.transportLabel.text = NSLocalizedString("other", comment: "")
                cell.carpoolingView.isHidden =  true
            case "BUS":
                cell.sectionImageView.image = UIImage(named: "bus-stop")
                cell.transportLabel.text = NSLocalizedString("bus", comment: "")
                cell.carpoolingView.isHidden =  true
            case "CAR":
                cell.sectionImageView.image = UIImage(named: "car-alt")
                cell.transportLabel.text = NSLocalizedString("car", comment: "")
                expandedCarIndex[indexPath.row] = true
                cell.carpoolingView.isHidden = false
            case "BICYCLING":
                cell.sectionImageView.image = UIImage(named: "bike")
                cell.transportLabel.text = NSLocalizedString("bicycle", comment: "")
                cell.carpoolingView.isHidden =  true
            case "WALKING":
                cell.sectionImageView.image = UIImage(named: "walk")
                cell.transportLabel.text = NSLocalizedString("walk", comment: "")
                cell.carpoolingView.isHidden =  true
            case "SUBWAY":
                cell.sectionImageView.image = UIImage(named: "subway")
                cell.transportLabel.text = NSLocalizedString("subway", comment: "")
                cell.carpoolingView.isHidden =  true
            case "TRAIN":
                cell.sectionImageView.image = UIImage(named: "train")
                cell.transportLabel.text = NSLocalizedString("train", comment: "")
                cell.carpoolingView.isHidden =  true
            case "TRAM":
                cell.sectionImageView.image = UIImage(named: "tram")
                cell.transportLabel.text = NSLocalizedString("tram", comment: "")
                cell.carpoolingView.isHidden =  true
            case "AIR_OR_HSR":
                cell.sectionImageView.image = UIImage(named: "airplane")
                cell.transportLabel.text = NSLocalizedString("plane", comment: "")
                cell.carpoolingView.isHidden =  true
            default:
                cell.sectionImageView.image = UIImage(named: "circle-off-outline")
                cell.transportLabel.text = NSLocalizedString("unknow", comment: "")
                cell.button.setTitle(NSLocalizedString("unknow", comment: ""), for: .normal)
                cell.carpoolingView.isHidden =  true
            }
            
            
        }
        DispatchQueue.main.async {
            if indexPath.row == self.sections.count - 1 {
                self.firstTime = false
            }
        }
        
        let doubleArr:[Double] = section["distances"].arrayValue.map { $0.doubleValue }
        let distancePerSection = doubleArr.reduce(0, +)
        let formattedDistance = RF.formatDistance(distancePerSection)
        let formattedDuration = RF.formatDuration(section["startTs"].doubleValue, section["endTs"].doubleValue)
        let percentage = (distancePerSection / self.tripData!["totalDistance"].doubleValue) * 100
        cell.sectionDescriptionLabel.text =  String(formattedDistance) + " km - " + formattedDuration
        cell.percentageLabel.text = "\(percentage.cleanValue) %"
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var size : CGFloat = 70.0
        
        for (key,value) in self.expandedIndex {
            if value == true && indexPath.row == key{
                size = 210
            }
        }
        for (key,value) in self.expandedCarIndex {
            if value == true && indexPath.row == key{
                size = size + 30
            }
        }
        
        
        return size
        
        
    }
    
}

extension DetailTripViewController : SectionModeTableViewCellDelegate {
    
    func didToggle() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.scrollView.scrollToBottom(animated: true)
        }
        
    }
    
    func getMode(mode: String, row : Int) {
        sectionsToSync[row]["mode"].stringValue = sensedModePerSectionToSync(modeFR: mode)
    }
    
    func didTappedOnrow(row: Int, didSelectCar : Bool) {
        if let val = expandedIndex[row] {
            expandedIndex[row] = !val
            if didSelectCar {
                expandedCarIndex[row] = true
            } else {
                expandedCarIndex[row] = false
            }
        } else {
            
            expandedIndex[row] = true
            if didSelectCar {
                expandedCarIndex[row] = true
            } else {
                expandedCarIndex[row] = false
            }
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.tableHeight.constant = self.tableView.contentSize.height
        }
        
    }
    
}














extension UIView {
    
    func setShadow() {
        
        
        self.clipsToBounds = true
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowColor = UIColor.gray.cgColor
    }
}

class MapPin: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
    }
}

class EndMapPin: NSObject, MKAnnotation {
    let title: String?
    let locationName: String?
    let coordinate: CLLocationCoordinate2D
    var image: UIImage? = nil
    
    init(title: String, locationName: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        super.init()
    }
}



extension Double{
    var cleanValue: String{
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(format: "%.2f", self)//
    }
}

extension UIScrollView {
    func scrollToBottom(animated: Bool) {
        if self.contentSize.height < self.bounds.size.height { return }
        let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
        self.setContentOffset(bottomOffset, animated: animated)
    }
}
