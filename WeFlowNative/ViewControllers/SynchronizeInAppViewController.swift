//
//  SynchronizeInAppViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 03/08/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Alamofire
import AudioToolbox
import Sentry
import SwiftyJSON

class SynchronizeInAppViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var irebyMailTextField: CustomTextField!
    @IBOutlet weak var syncButton: LoadingButton!
    @IBOutlet weak var informativeSyncLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    
    var params : Parameters?
    let alertService = AlertService()
    let RF = ReusableFunctions()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        syncButton.isEnabled = false
        syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        setUpTextFields()
        addTargetTotextFields()
        syncButton.setTitle(NSLocalizedString("synchronize", comment: ""), for: .normal)
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        
    }
    
    @IBAction func syncTapped(_ sender: Any) {
        if(syncButton.title(for: .normal) == NSLocalizedString("synchronize", comment: "")) {
            syncWithIreby(token: UserDefaults.standard.string(forKey: "Token")!, mail: irebyMailTextField.text!)
        } else if syncButton.title(for: .normal) == NSLocalizedString("lets_go", comment: "") {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func syncWithIreby (token : String, mail : String) {
        syncButton.showLoading()
        syncButton.isEnabled = false
        syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        params = [
            "email" : irebyMailTextField.text
        ]
        let headers: HTTPHeaders = [
            "token" : UserDefaults.standard.string(forKey: "ireby_access_token")!   ,
            "authorization": "Bearer " + K.Constant.irebyBearer,
            "content-type": "application/json",
            "Accept": "application/json"
        ]
        
        AF.request(K.Path.baseURLWeb + "/api/users/weflo/sync", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseString { [self]
            response in
            switch response.result {
            case .success(let value):
                print(value)
                print("status code =",response.response?.statusCode)
                if response.response?.statusCode == 200 {
                    let alert = alertService.alert(title: NSLocalizedString("sync-mail-popup-title", comment: ""), body: NSLocalizedString("sync-mail-popup-body", comment: ""), buttonTitle: NSLocalizedString("sync-mail-popup-button", comment: "")) {
                        navigationController?.popViewController(animated: true)
                    }
                    self.tabBarController?.present(alert, animated: true)
                    
                } else if response.response?.statusCode == 409 {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("popup-already-sync-title", comment: ""), body: NSLocalizedString("popup-already-sync-body", comment: ""), ButtonTitle: NSLocalizedString("popup-already-sync-button", comment: ""))
                } else if response.response?.statusCode == 404 {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("error1-mail-popup-title", comment: ""), body: NSLocalizedString("error1-mail-popup-body", comment: ""), ButtonTitle: NSLocalizedString("error1-mail-popup-button", comment: ""))
                }  else if response.response?.statusCode == 422 {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("error-mail-popup-title", comment: ""), body: NSLocalizedString("error-mail-popup-body", comment: ""), ButtonTitle: NSLocalizedString("error-mail-popup-button", comment: ""))
                } else {
                    resetTextFields()
                    showAlert(title: NSLocalizedString("error-popup-sync-title", comment: ""), body: NSLocalizedString("error-popup-sync-body", comment: ""), ButtonTitle: NSLocalizedString("error-popup-sync-button", comment: ""))
                }
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
    }
    func resetTextFields () {
        irebyMailTextField.text = ""
        syncButton.hideLoading()
        syncButton.isEnabled = true
        syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
    }
    func showAlert(title : String, body : String, ButtonTitle : String) {
        let alert = alertService.alert(title: title, body: body, buttonTitle: ButtonTitle) {
            
        }
        tabBarController!.present(alert, animated: true)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func setUpTextFields () {
        irebyMailTextField.delegate = self
        irebyMailTextField.delegate = self
    }
    
    
    func addTargetTotextFields () {
        irebyMailTextField.addTarget(self, action: #selector(SynchronizeAccountViewController.textFieldDidChange(_:)), for: .editingChanged)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        checkEntries()
    }
    func checkEntries () {
        if RF.isValidEmail(irebyMailTextField.text!) == true  {
            syncButton.isEnabled = true
            syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        } else {
            syncButton.isEnabled = false
            syncButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
    }
    
    
    
}
