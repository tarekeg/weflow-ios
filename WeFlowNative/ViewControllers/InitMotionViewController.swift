//
//  InitMotionViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 12/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Sentry

class InitMotionViewController: UIViewController {
    @IBOutlet weak var descriptionMotionTextView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionMotionTextView.text = NSLocalizedString("description-text-motion", comment: "")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(getMotionStatus), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    
    
    @objc func getMotionStatus() {
       
        
            if CMMotionActivityManager.authorizationStatus().rawValue == 3 {
                NotificationCenter.default.removeObserver(UIApplication.didBecomeActiveNotification)
                if UserTripData.userTrips == [] {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "progressDownloadView")
                    UIApplication.shared.windows.first?.rootViewController = vc
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    self.present(vc, animated: true)
                } else {
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "tabBarController")
                    UIApplication.shared.windows.first?.rootViewController = vc
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    self.present(vc, animated: true)
                }
                
                
                
            }
        
        
    }
    
    
    @IBAction func EnableMotionActivity(_ sender: Any) {
        initMotion()
        if CMMotionActivityManager.authorizationStatus().rawValue == 2 {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        
    }
    
    
    
    func initMotion () {
        if CMMotionActivityManager.isActivityAvailable() == true {
            let activityMgr = CMMotionActivityManager()
            let mq = OperationQueue.main
            let startDate = Date()
            let dayAgoSecs: TimeInterval = 24 * 60 * 60
            let endDate = Date(timeIntervalSinceNow: -(dayAgoSecs))
            activityMgr.queryActivityStarting(from: startDate, to: endDate, to: mq, withHandler: { activities, error in
                if error == nil {
                    LocalNotificationManager.addNotification("activity recognition works fine")
                } else {
                    if let error = error {
                        SentrySDK.capture(error: error)
                        LocalNotificationManager.addNotification("Error \(error) while reading activities, travel mode detection may be unavailable")
                    }
                }
            })
        }
    }
    
}
