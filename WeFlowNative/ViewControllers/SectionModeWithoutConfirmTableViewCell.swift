//
//  SectionModeWithoutConfirmTableViewCell.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 20/10/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

class SectionModeWithoutConfirmTableViewCell: UITableViewCell {
    
    @IBOutlet weak var sectionImageView: UIImageView!
    @IBOutlet weak var sectionDescriptionLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
