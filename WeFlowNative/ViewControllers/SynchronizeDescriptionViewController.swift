//
//  SynchronizeDescriptionViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 11/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit

class SynchronizeDescriptionViewController: UIViewController {

    @IBOutlet weak var synchronizeAccountView: UIView!
    @IBOutlet weak var accountCreationView: UIView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        accountCreationView.layer.cornerRadius = 5.0
        synchronizeAccountView.layer.cornerRadius = 5.0

    }
    
    


}
