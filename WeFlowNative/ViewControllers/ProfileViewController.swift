//
//  ProfileViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 27/08/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import Auth0
import Kingfisher
import SwiftyJSON
import Alamofire
import Reachability
import SafariServices
import Sentry

class ProfileViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, AlertSyncDelegate, AlertResetDelegate, UIGestureRecognizerDelegate {
    func sendResult(bool: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            if bool {
                let alert = alertService.alertResetPasswordSuccess()
                tabBarController!.present(alert, animated: true)
            } else {
                self.showToast(message: NSLocalizedString("error-toast", comment: ""))
            }
        }
    }
    
    func sendTag(tag: Int) {
        print(String(tag))
        buttonTag = tag
    }
    
    
    
    @IBOutlet weak var resetPasswordButton: UIButton!
    @IBOutlet weak var changeProfileImageButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var lastNameTextField: CustomTextField!
    @IBOutlet weak var firstNameTextField: CustomTextField!
    @IBOutlet weak var birthDateTextField: CustomTextField!
    @IBOutlet weak var emailTextField: CustomTextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var womenCheckBoxImageView: UIImageView!
    @IBOutlet weak var menCheckBoxImageView: UIImageView!
    @IBOutlet weak var othersCheckBoxImageView: UIImageView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var backgroundProfileImageView: CustomView!
    @IBOutlet weak var weightTextField: CustomTextField!
    
    
    let kgLabel = UILabel()
    
    enum ImageSource {
        case photoLibrary
        case camera
    }
    
    let RF = ReusableFunctions()
    var user = User(firstname: "", lastname: "", birthDate: "", street: "", zipCode: "", city: "", gender: "", weight: "")
    var jsonMode : JSON = []
    let credentialsManager = CredentialsManager(authentication: Auth0.authentication())
    var datePicker :UIDatePicker!
    let checkedImage = UIImage(named: "ic_check_box")
    let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")
    var imagePicker : UIImagePickerController!
    let alertService = AlertService()
    var gender : String?
    var buttonTag : Int?
    let refreshControl = UIRefreshControl()
    var popupDidPop = false
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont(name: "Avenir-Medium", size: 15)!,
        .foregroundColor: K.Color.grayThirtySix,
        .underlineStyle: NSUnderlineStyle.single.rawValue
    ]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let attributeResetString = NSMutableAttributedString(
            string: NSLocalizedString("reset-password", comment: ""),
            attributes: yourAttributes
        )
        tabBarController?.delegate = self
        alertService.syncDelegate = self
        alertService.resetDelegate = self
        resetPasswordButton.setAttributedTitle(attributeResetString, for: .normal)
        setUI()
        setUpPicker()
        emailTextField.backgroundColor = .groupTableViewBackground
        scrollView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        lastNameTextField.delegate = self
        firstNameTextField.delegate = self
        birthDateTextField.delegate = self
        weightTextField.delegate = self
        confirmButton.isEnabled = false
        confirmButton.backgroundColor = UIColor(red: 0.96, green: 0.82, blue: 0.51, alpha: 1.00)
        getUserData()
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        addTargetTotextFields()
        roudedImage(profileImageView)
        emailTextField.isEnabled = false
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    
    
    @IBAction func backTapped(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func resetPasswordTapped(_ sender: Any) {
        let alert = alertService.alertResetPassword()
        tabBarController!.present(alert, animated: true)
    }
    
    
    
    func setUI() {
        backgroundProfileImageView.backgroundColor = K.Color.primaryColor
        confirmButton.backgroundColor = K.Color.primaryColor
        let contentView = UIView()
        kgLabel.font = UIFont(name: "Avenir-Medium", size: 15.0)
        kgLabel.textColor = UIColor(red: 0.24, green: 0.25, blue: 0.26, alpha: 1.00)
        kgLabel.text = " Kg"
        contentView.addSubview(kgLabel)
        contentView.frame = CGRect(x: 0, y: -10, width: 30, height: 30 )
        kgLabel.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        weightTextField.rightView = contentView
        weightTextField.rightViewMode = .always
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == weightTextField {
            let aSet = NSCharacterSet(charactersIn:"0123456789,").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let length = textstring.count
            if length > 5 {
                return false
            }
            return string == numberFiltered
        } else {
            return true
        }
    }
    
    func setUpPicker() {
        
        birthDateTextField.tintColor = .clear
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            
        }
        datePicker.maximumDate = Date()
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
        datePicker.setDate(from: "1/1/2000", format: "dd/MM/yyyy", animated: true)
        datePicker.addTarget(self, action: #selector(self.dateChanged), for: .allEvents)
        birthDateTextField.inputView = datePicker
        datePicker.datePickerMode = .date
        let doneButton = UIBarButtonItem.init(title: "OK", style: .done, target: self, action: #selector(self.datePickerDone))
        doneButton.tintColor = UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00)
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        birthDateTextField.inputAccessoryView = toolBar
    }
    @objc func datePickerDone() {
        birthDateTextField.resignFirstResponder()
    }
    
    @objc func dateChanged() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        birthDateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == birthDateTextField){
            testEquality(firstText: birthDateTextField.text!, secondText: self.user.birthDate)
        }
        
    }
    
    
    
    
    func roudedImage(_ image : UIImageView) {
        image.layer.borderWidth = 2
        image.layer.masksToBounds = false
        image.layer.borderColor = UIColor.white.cgColor
        image.clipsToBounds = true
        image.layer.cornerRadius = image.frame.height/2
    }
    
    func addTargetTotextFields () {
        lastNameTextField.addTarget(self, action: #selector(ProfileViewController.textFieldDidChange(_:)), for: .editingChanged)
        firstNameTextField.addTarget(self, action: #selector(ProfileViewController.textFieldDidChange(_:)), for: .editingChanged)
        birthDateTextField.addTarget(self, action: #selector(ProfileViewController.textFieldDidChange(_:)), for: .editingChanged)
        weightTextField.addTarget(self, action: #selector(ProfileViewController.textFieldDidChange(_:)), for: .editingChanged)
        
    }
    func testEquality (firstText : String, secondText : String) {
        if firstText != secondText {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1.00)
        } else {
            confirmButton.isEnabled = false
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        switch textField {
        case firstNameTextField:
            testEquality(firstText: firstNameTextField.text!, secondText: self.user.firstname)
        case lastNameTextField:
            testEquality(firstText: lastNameTextField.text!, secondText: self.user.lastname)
        case weightTextField:
            testEquality(firstText: weightTextField.text!, secondText: self.user.weight)
        default:
            print("error")
        }
    }
    
    
    
    @IBAction func changeProfilePictureTapped(_ sender: Any) {
        let sheet = UIAlertController(title: NSLocalizedString("add-photo-picker-title",comment : ""), message:  NSLocalizedString("add-photo-picker-message",comment : ""), preferredStyle: .actionSheet)
        
        let actionSheetLibrary = UIAlertAction(title:  NSLocalizedString("library-source",comment : ""), style: .default) { (UIAlertAction) in
            guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
                self.selectImageFrom(.camera)
                return
            }
            self.selectImageFrom(.photoLibrary)
        }
        
        let actionSheetCamera = UIAlertAction(title:  NSLocalizedString("camera-source",comment : ""), style: .default) { (UIAlertAction) in
            guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
                self.selectImageFrom(.photoLibrary)
                return
            }
            self.selectImageFrom(.camera)
        }
        
        let actionSheetCancel = UIAlertAction(title:  NSLocalizedString("cancel",comment : ""), style: .cancel, handler: nil)
        sheet.addAction(actionSheetLibrary)
        sheet.addAction(actionSheetCamera)
        sheet.addAction(actionSheetCancel)
        tabBarController!.present(sheet, animated: true, completion: nil)
    }
    
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        tabBarController!.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        profileImageView.image = selectedImage
        ImageCache.default.store(profileImageView.image!, forKey: "my_avatar")
        uploadImage()
    }
    
    
    
    func getUserData() {
        self.emailTextField.text = UserDefaults.standard.string(forKey: "email")
        DispatchQueue.main.async {
            self.profileImageView.kf.indicatorType = .activity
            self.profileImageView.kf.setImage(with: URL(string: UserDefaults.standard.string(forKey: "pictureUrl")!))
        }
        
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "Token")!,
            "content-type": "application/json"
        ]
        AF.request(K.Path.cleverApi + "users", method: .get, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                if response.response?.statusCode == 200 {
                    
                    let jsonUser = JSON(value)
                    if jsonUser["firstName"].stringValue == "" {
                        if self.popupDidPop == false {
                            let alert = self.alertService.alert(title:NSLocalizedString("error-popup-oldUser-title", comment: ""), body: NSLocalizedString("error-popup-oldUser-body", comment: ""), buttonTitle: NSLocalizedString("error-popup-oldUser-button", comment: "")) {
                                
                            }
                            DispatchQueue.main.async {
                                self.tabBarController!.present(alert, animated: true) {
                                    self.popupDidPop = true
                                }
                            }
                        }
                        
                        
                    } else {
                        self.user.firstname = jsonUser["firstName"].stringValue
                        self.user.lastname = jsonUser["lastName"].stringValue
                        if jsonUser["birthDate"].string == nil {
                            self.user.birthDate = ""
                        } else {
                            print(jsonUser["birthDate"].stringValue, "Formatted date = ",self.formatDate(date:jsonUser["birthDate"].stringValue))
                            self.user.birthDate = self.formatDate(date:jsonUser["birthDate"].stringValue)
                        }
                        self.user.gender = jsonUser["gender"].stringValue
                        let doubleWeight = jsonUser["weight"].doubleValue.clean
                        self.user.weight = doubleWeight
                        self.fullNameLabel.text = self.user.firstname.uppercased() + " " + self.user.lastname.uppercased()
                        self.birthDateTextField.text = self.user.birthDate
                        self.firstNameTextField.text = self.user.firstname
                        self.lastNameTextField.text = self.user.lastname
                        self.weightTextField.text = self.user.weight
                        switch self.user.gender {
                        case "female" :
                            self.womenCheckBoxImageView.image = self.checkedImage
                        case "male":
                            self.menCheckBoxImageView.image = self.checkedImage
                        case "other" :
                            self.othersCheckBoxImageView.image = self.checkedImage
                        default:
                            self.womenCheckBoxImageView.image = self.uncheckedImage
                        }
                    }
                }
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
        
        
    }
    
    func uploadImage() {
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "Token")!,
            "content-type": "application/json"
        ]
        let imageData = self.profileImageView.image!.jpegData(compressionQuality: 0.5)
        AF.upload(multipartFormData: { (MultipartFormData) in
            MultipartFormData.append(imageData!, withName: "picture", fileName: "picture.jpeg", mimeType: "image/jpeg")
        }, to: K.Path.cleverApi + "uploads/",headers: headers).responseJSON { response in
            let jsonResponse = JSON(response.value!)
            let url = K.Path.cleverApi + "uploads/" + jsonResponse["picturePath"].stringValue
            let fileUrl = URL(string: url)
            UserDefaults.standard.set(url, forKey: "pictureUrl")
        }
    }
    @IBAction func saveDataTapped(_ sender: Any) {
        updateData()
        let alertVC = alertService.alert(title: NSLocalizedString("update-popup-title",comment : ""), body: NSLocalizedString("update-popup-body",comment : ""), buttonTitle: NSLocalizedString("update-popup-button",comment : "")) {
            self.confirmButton.isEnabled = false
            self.confirmButton.backgroundColor = UIColor(red: 0.96, green: 0.82, blue: 0.51, alpha: 1.00)
            self.getUserData()
        }
        
        tabBarController!.present(alertVC, animated: true, completion: nil)
        
        
        
    }
    
    
    func updateData() {
        var weight = 0.0
        var params : Parameters?
        if(weightTextField.text == ""){
            weight = K.Constant.averageWeight
        } else {
            weight = RF.formatDecimals(numberString: weightTextField.text!)
        }
        params  = ["weight":weight, "firstName" : firstNameTextField.text!, "lastName" : lastNameTextField.text!, "gender" : gender, "birthDate" : formatDateToSync(date: birthDateTextField.text!)]
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + UserDefaults.standard.string(forKey: "Token")!,
            "content-type": "application/json"
        ]
        AF.request(K.Path.cleverApi + "users/update", method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON : \(json)")
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
        
    }
    
    func getGender () -> String{
        if menCheckBoxImageView.image == checkedImage {
            gender = "male"
        } else if womenCheckBoxImageView.image == checkedImage {
            gender = "female"
        } else if othersCheckBoxImageView.image == checkedImage {
            gender = "other"
        } else {
            return ""
        }
        return gender!
    }
    
    @IBAction func womenButtonTapped(_ sender: Any) {
        gender = "female"
        if(gender != self.user.gender) {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1.00)
        } else {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
        
        womenCheckBoxImageView.image = checkedImage
        menCheckBoxImageView.image = uncheckedImage
        othersCheckBoxImageView.image = uncheckedImage
    }
    @IBAction func menButtonTapped(_ sender: Any) {
        gender = "male"
        if(gender != self.user.gender) {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1.00)
        } else {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
        
        womenCheckBoxImageView.image = uncheckedImage
        menCheckBoxImageView.image = checkedImage
        othersCheckBoxImageView.image = uncheckedImage
    }
    @IBAction func othersButtonTapped(_ sender: Any) {
        gender = "other"
        if(gender != self.user.gender) {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1.00)
        } else {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
        womenCheckBoxImageView.image = uncheckedImage
        menCheckBoxImageView.image = uncheckedImage
        othersCheckBoxImageView.image = checkedImage
    }
    @objc func didPullToRefresh() {
        let reachability = try! Reachability()
        if (reachability.connection == .wifi || reachability.connection == .cellular) {
            getUserData()
            refreshControl.endRefreshing()
        } else {
            refreshControl.endRefreshing()
        }
    }
    
    
    
    @IBAction func logoutTapped(_ sender: Any) {
        
        let alertVC = alertService.alert(title: NSLocalizedString("logout-popup-title",comment : ""), body: NSLocalizedString("logout-popup-body",comment : ""), buttonTitle: NSLocalizedString("logout-popup-button",comment : "")) {
            SessionManager.shared.logout { error in
                
                guard error == nil else {
                    SentrySDK.capture(error: error as! Error)
                    return print("Error revoking token: \(String(describing: error))")
                }
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                UserTripData.userTrips = []
                ImageCache.default.removeImage(forKey: "my_avatar")
                UserDefaults.standard.removeObject(forKey: "ireby_access_token")
                UserDefaults.standard.removeObject(forKey: "ireby_synchronization")
                UserDefaults.standard.removeObject(forKey: "sub")
                UserDefaults.standard.removeObject(forKey: "hasStartedSignIn")
                Point.pro = "0 Pts"
                Point.standard = "0 Pts"
                DispatchQueue.main.async {
                    UserDefaults.standard.set(false, forKey: "hasAlreadyWeight")
                    guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return }
                    let rootController = UIStoryboard(name: "Authentication", bundle: Bundle.main).instantiateViewController(withIdentifier: "navControllerLogin")
                    appDel.window?.rootViewController = rootController
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
        tabBarController!.present(alertVC, animated: true, completion: nil)
        
        
        
        
    }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 125, y: self.view.frame.size.height-100, width: 250, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Avenir-Roman", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func formatDateToSync (date : String) -> String {
        var resultString = ""
        if birthDateTextField.text?.isEmpty == false {
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "dd/MM/yyyy"
            let showDate = inputFormatter.date(from: date)
            inputFormatter.dateFormat = "yyyy-MM-dd"
            resultString = inputFormatter.string(from: showDate!)
        }
        return resultString
    }
    func formatDate (date : String) -> String {
        var resultString = ""
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let showDate = inputFormatter.date(from: date)
        inputFormatter.dateFormat = "dd/MM/yyyy"
        resultString = inputFormatter.string(from: showDate!)
        return resultString
    }
    
}

extension UISegmentedControl {
    func font(name:String?, size:CGFloat?) {
        let attributedSegmentFont = NSDictionary(object: UIFont(name: name!, size: size!)!, forKey: NSAttributedString.Key.font as NSCopying)
        setTitleTextAttributes(attributedSegmentFont as [NSObject : AnyObject] as [NSObject : AnyObject] as? [NSAttributedString.Key : Any], for: .normal)
    }
}
extension ProfileViewController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        navigationController?.popViewController(animated: false)
    }
}
