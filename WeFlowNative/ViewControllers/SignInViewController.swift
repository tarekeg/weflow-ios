//
//  SignInViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 10/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Auth0
import Alamofire
import SwiftyJSON
import Firebase
import SwiftKeychainWrapper
import Sentry

class SignInViewController: UIViewController, UITextFieldDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var womenCheckBoxImageView: UIImageView!
    @IBOutlet weak var menCheckBoxImageView: UIImageView!
    @IBOutlet weak var nonGenderedCheckBoxImageView: UIImageView!
    @IBOutlet weak var lastNameTextField: CustomTextField!
    @IBOutlet weak var firstNameTextField: CustomTextField!
    @IBOutlet weak var birthDateTextField: CustomTextField!
    @IBOutlet weak var weightTextField: CustomTextField!
    @IBOutlet weak var mailTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var confirmPasswordTextField: CustomTextField!
    @IBOutlet weak var tcuTextView: UITextView!
    @IBOutlet weak var tcuCheckBoxImageView: UIImageView!
    @IBOutlet weak var signInButton: LoadingButton!
    
    
    let alertService = AlertService()
    var datePicker : UIDatePicker!
    let checkedImage = UIImage(named: "ic_check_box")
    let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")
    var gender = ""
    var tcuIsConfirmed = false
    var passwordIconClick = false
    let passwordImageIcon = UIImageView()
    var confirmPasswordIconClick = false
    let confirmPasswordImageIcon = UIImageView()
    let eyeColor = UIColor(red: 0.67, green: 0.67, blue: 0.67, alpha: 1.00)
    let RF = ReusableFunctions()
    
    let hyperlinks = [NSLocalizedString("tcu",comment : "") : K.Path.wefloWix + "mentions-légales".stringByAddingPercentEncodingForRFC3986(),NSLocalizedString("pol-conf",comment : ""):K.Path.wefloWix + "politique-de-confidentialité".stringByAddingPercentEncodingForRFC3986(), "cookies" : K.Path.wefloWix + "politique-de-confidentialit%C3%A9/#comp-kj1aowim".stringByAddingPercentEncodingForRFC3986()]
    let kgLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkFCMToken()
        setDesign()
        weightTextField.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    func checkFCMToken () {
        Messaging.messaging().token { token, error in
            if let error = error {
                SentrySDK.capture(error: error)
                print("Error fetching FCM registration token: \(error)")
            } else if let token = token {
                print("FCM registration token: \(token)")
                UserDefaults.standard.set(token, forKey: "FCMToken")
            }
        }
    }
    
    func setEyeForPassword (imageIcon : UIImageView, textField : UITextField, tapGestureRecognizer : UITapGestureRecognizer) {
        imageIcon.image = UIImage(named: "visibility_off")?.imageWithColor(color1: eyeColor)
        let contentView = UIView()
        contentView.addSubview(imageIcon)
        contentView.frame = CGRect(x: 0, y: 0, width: UIImage(named: "visibility_off")!.size.width + 10, height: UIImage(named: "visibility_off")!.size.height + 10 )
        imageIcon.frame = CGRect(x: -10, y: 5, width: UIImage(named: "visibility_off")!.size.width, height: UIImage(named: "visibility_off")!.size.height)
        
        textField.rightView = contentView
        textField.rightViewMode = .always
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func passwordImageTapped(tapGestureRecognizer:UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if passwordIconClick {
            passwordIconClick = false
            tappedImage.image = UIImage(named: "visibility")?.imageWithColor(color1: eyeColor)
            passwordTextField.isSecureTextEntry = false
        } else {
            passwordIconClick = true
            tappedImage.image = UIImage(named: "visibility_off")?.imageWithColor(color1: eyeColor)
            passwordTextField.isSecureTextEntry = true
        }
    }
    @objc func confirmPasswordImageTapped(tapGestureRecognizer:UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if confirmPasswordIconClick {
            confirmPasswordIconClick = false
            tappedImage.image = UIImage(named: "visibility")?.imageWithColor(color1: eyeColor)
            confirmPasswordTextField.isSecureTextEntry = false
        } else {
            confirmPasswordIconClick = true
            tappedImage.image = UIImage(named: "visibility_off")?.imageWithColor(color1: eyeColor)
            confirmPasswordTextField.isSecureTextEntry = true
        }
    }
    
    func setDesign () {
        let passwordTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(passwordImageTapped(tapGestureRecognizer:)))
        let confirmPasswordTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(confirmPasswordImageTapped(tapGestureRecognizer:)))
        setEyeForPassword(imageIcon: passwordImageIcon, textField: passwordTextField, tapGestureRecognizer: passwordTapGestureRecognizer)
        setEyeForPassword(imageIcon: confirmPasswordImageIcon, textField: confirmPasswordTextField, tapGestureRecognizer: confirmPasswordTapGestureRecognizer)
        tcuTextView.text = NSLocalizedString("tcu-text", comment: "")
        setUpPicker()
        signInButton.isEnabled = false
        signInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        tcuTextView.addHyperLinksToText(originalText: tcuTextView.text!, hyperLinks: hyperlinks)
        let contentView = UIView()
        kgLabel.font = UIFont(name: "Avenir-Medium", size: 15.0)
        kgLabel.textColor = UIColor(red: 0.24, green: 0.25, blue: 0.26, alpha: 1.00)
        kgLabel.text = " Kg"
        contentView.addSubview(kgLabel)
        contentView.frame = CGRect(x: 0, y: -10, width: 30, height: 30 )
        kgLabel.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        weightTextField.rightView = contentView
        weightTextField.rightViewMode = .always
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == weightTextField {
            let aSet = NSCharacterSet(charactersIn:"0123456789,").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let length = textstring.count
                if length > 5 {
                    return false
                }
            return string == numberFiltered
        } else {
            return true
        }
    }

    
    @IBAction func signInTapped(_ sender: Any) {
        if isGenderChoosen() == false {
            showAlert(title: NSLocalizedString("error-signup-gender-title", comment: ""), body: NSLocalizedString("error-signup-gender-body", comment: ""), buttonTitle: NSLocalizedString("error-signup-gender-button", comment: ""))
            return
        } else if firstNameTextField.text?.isEmpty == true || lastNameTextField.text?.isEmpty == true {
            showAlert(title: NSLocalizedString("error-signup-firstname-title", comment: ""), body: NSLocalizedString("error-signup-firstname-body", comment: ""), buttonTitle: NSLocalizedString("error-signup-firstname-button", comment: ""))
            return
        } else if RF.isValidEmail(mailTextField.text!) == false {
            showAlert(title: NSLocalizedString("error-signup-mail-title", comment: ""), body: NSLocalizedString("error-signup-mail-body", comment: ""), buttonTitle: NSLocalizedString("error-signup-mail-button", comment: ""))
            return
        } else if RF.isPasswordValid(passwordTextField.text!) == false {
            showAlert(title: NSLocalizedString("error-signup-password-title", comment: ""), body: NSLocalizedString("error-signup-password-body", comment: ""), buttonTitle: NSLocalizedString("error-signup-password-button", comment: ""))
            return
        } else if passwordTextField.text != confirmPasswordTextField.text {
            showAlert(title: NSLocalizedString("error-signup-confirmPassword-title", comment: ""), body: NSLocalizedString("error-signup-confirmPassword-body", comment: ""), buttonTitle: NSLocalizedString("error-signup-confirmPassword-button", comment: ""))
            return
        } else {
            signInButton.showLoading()
            signInButton.isEnabled = false
            signInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
            auth0SignUp()
        }
       
    }
    
    func showAlert (title : String , body : String , buttonTitle : String) {
        let alert = alertService.alert(title: title, body: body, buttonTitle: buttonTitle) {
            
        }
        self.present(alert, animated: true) 
    }
    
    func auth0SignUp () {
        Auth0
            .authentication()
            .createUser(
                email: mailTextField.text!,
                password: passwordTextField.text!,
                connection: "Username-Password-Authentication",
                userMetadata: ["first_name": firstNameTextField.text,
                               "last_name": lastNameTextField.text])
            .start { result in
                switch result {
                case .failure(let error as Auth0.AuthenticationError):
                    SentrySDK.capture(error: error)
                    print("Error code: \(error.code), description:\(error.description), info:\(error.info), statusCode:\(error.statusCode)")
                    DispatchQueue.main.async {
                        if (error.statusCode == 400) {
                            self.showPopup(title: NSLocalizedString("error-signup-mailalreadyexist-title", comment: ""), body: NSLocalizedString("error-signup-mailalreadyexist-body", comment: ""), buttonTitle: NSLocalizedString("error-signup-mailalreadyexist-button", comment: ""))
                        } else {
                            self.showPopup(title: NSLocalizedString("error-popup-signIn-title", comment: ""), body: NSLocalizedString("error-popup-signIn-body", comment: ""), buttonTitle: NSLocalizedString("error-popup-signIn-button", comment: ""))
                        }
                       
                    }
                    
                    
                case .failure(let error):
                    SentrySDK.capture(error: error)
                    print("Error: \(error.localizedDescription)")
                case .success(_):
                    print("Login success")
                    UserDefaults.standard.set("startedSync", forKey: "hasStartedSignIn")
                    DispatchQueue.main.async {
                        KeychainWrapper.standard.set(self.mailTextField.text!, forKey: "irebymail")
                        KeychainWrapper.standard.set(self.firstNameTextField.text!, forKey: "irebyfirstname")
                        KeychainWrapper.standard.set(self.lastNameTextField.text!, forKey: "irebylastname")
                        KeychainWrapper.standard.set(self.passwordTextField.text!, forKey: "irebypassword")
                        let mail = self.mailTextField.text!
                        let password = self.passwordTextField.text!
                        self.auth0Login(mail,password)
                    }
                    
                }
            }
    }
    func auth0Login (_ mail : String, _ password : String) {
        Auth0
            .authentication()
            .login(
                usernameOrEmail: mail,
                password: password,
                realm: "Username-Password-Authentication",
                audience : "https://preprod.ireby.pro/api",
                scope: "openid profile email offline_access")
            .start { result in
                switch result {
                case .success(let credentials):
                    UserDefaults.standard.set(credentials.idToken!, forKey: "Token")
                    UserDefaults.standard.set(credentials.accessToken!, forKey: "ireby_access_token")
                    UserDefaults.standard.set(credentials.refreshToken!, forKey: "ireby_refresh_token")
                    print("my access token : ",credentials.accessToken)
                    DispatchQueue.main.async {
                        self.syncTokenProfile(token: credentials.idToken!)
                        self.syncWeflowApiUser(token: credentials.idToken!)
                    }
                    if(!SessionManager.shared.store(credentials: credentials)) {
                        print("Failed to store credentials")
                    } else {
                        SessionManager.shared.retrieveProfile { error in
                            DispatchQueue.main.async {
                                guard error == nil else {
                                    print("Failed to retrieve profile: \(String(describing: error))")
                                    return
                                }
                                self.signInButton.hideLoading()
                                    self.performSegue(withIdentifier: "toSync", sender: nil)
                                
                                
                            }
                        }
                    }
                case .failure(let error):
                    SentrySDK.capture(error: error)
                    DispatchQueue.main.async {
                        self.showPopup(title: NSLocalizedString("error-popup-signIn-title", comment: ""), body: NSLocalizedString("error-popup-signIn-body", comment: ""), buttonTitle: NSLocalizedString("error-popup-signIn-button", comment: ""))
                    }
                    print("Failed with \(error)")
                }
            }
    }
    
    func showPopup(title : String, body : String, buttonTitle : String) {
           
        let alert = alertService.alert(title: title, body: body, buttonTitle: buttonTitle) { [self] in
            signInButton.isEnabled = true
            signInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
            signInButton.hideLoading()
            
        }
        self.present(alert, animated: true)
    }
    
    func syncWeflowApiUser (token: String) {
        print("Device_token = ",UserDefaults.standard.string(forKey: "FCMToken") ?? "unknow")
        var params : Parameters?
        
        var weight = 76.0
        if weightTextField.text?.isEmpty == false {
            weight = Double(weightTextField.text!.replacingOccurrences(of: ",", with: "."))!
        }
        params = [
            "firstName" : firstNameTextField.text,
            "lastName" : lastNameTextField.text,
            "birthDate" : formatDate(date: birthDateTextField.text!),
            "gender" : gender,
            "weight":weight,
            "deviceToken" : UserDefaults.standard.string(forKey: "FCMToken") ?? "unknow",
            "source": "Transway",
            "client":"ios"
        ]
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + token,
            "content-type": "application/json"
        ]
        AF.request(K.Path.cleverApi + "v2/users?extended-info=true", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON : \(json)")
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
        
    }
    
    
    
    func syncTokenProfile (token : String) {
        let parameters = ["user":token]
        let parameters2 = [
            "update_doc":[
                "device_token":UserDefaults.standard.string(forKey: "FCMToken") ?? "unknow",
                "curr_platform":"ios",
                "curr_sync_interval":3600,
                "source":"Transway"
            ],
            "user":token
        ] as [String : Any]
        AF.request(K.Path.baseURL + K.Path.createProfile, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON {
            response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON : \(json)")
                AF.request(K.Path.baseURL + K.Path.updateProfile, method: .post, parameters: parameters2, encoding: JSONEncoding.default, headers: nil, interceptor: nil, requestModifier: nil).responseJSON { response in
                    switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        print("JSON : \(json)")
                    case .failure(let error):
                        SentrySDK.capture(error: error)
                        print(error)
                    }
                }
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
    }
    
    
    @IBAction func womenCheckBoxTapped(_ sender: Any) {
        womenCheckBoxImageView.image = checkedImage
        menCheckBoxImageView.image = uncheckedImage
        nonGenderedCheckBoxImageView.image = uncheckedImage
        gender = "female"
    }
    @IBAction func menCheckBoxTapped(_ sender: Any) {
        womenCheckBoxImageView.image = uncheckedImage
        menCheckBoxImageView.image = checkedImage
        nonGenderedCheckBoxImageView.image = uncheckedImage
        gender = "male"
    }
    @IBAction func nonGenderedCheckBoxTapped(_ sender: Any) {
        womenCheckBoxImageView.image = uncheckedImage
        menCheckBoxImageView.image = uncheckedImage
        nonGenderedCheckBoxImageView.image = checkedImage
        gender = "other"
    }
    @IBAction func tcuTapped(_ sender: Any) {
        if tcuIsConfirmed {
            tcuIsConfirmed = !tcuIsConfirmed
            tcuCheckBoxImageView.image = uncheckedImage
            signInButton.isEnabled = false
            signInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        } else {
            
            tcuIsConfirmed = !tcuIsConfirmed
            tcuCheckBoxImageView.image = checkedImage
            signInButton.isEnabled = true
            signInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        }
    }
    
    
    @IBAction func goBackToRootNav(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func isGenderChoosen () -> Bool {
        if gender == "" {
            return false
        } else {
            return true
        }
    }
    
    func setUpPicker() {
        
        birthDateTextField.tintColor = .clear
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            
        }
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
        datePicker.setDate(from: "1/1/2000", format: "dd/MM/yyyy", animated: true)
        datePicker.maximumDate = Date()
        birthDateTextField.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(self.dateChanged), for: .allEvents)
        let doneButton = UIBarButtonItem.init(title: "OK", style: .done, target: self, action: #selector(self.datePickerDone))
        doneButton.tintColor = UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00)
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        birthDateTextField.inputAccessoryView = toolBar
    }
    @objc func datePickerDone() {
        birthDateTextField.resignFirstResponder()
    }
    @objc func dateChanged() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        birthDateTextField.text = dateFormatter.string(from: datePicker.date)
    }

  
    
   
  
    
    
    func formatDate (date : String) -> String {
        var resultString = ""
        if birthDateTextField.text?.isEmpty == false {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd/MM/yyyy"
        let showDate = inputFormatter.date(from: date)
        inputFormatter.dateFormat = "yyyy-MM-dd"
        resultString = inputFormatter.string(from: showDate!)
        }
        return resultString
    }

    
  

}

extension UIDatePicker {

   func setDate(from string: String, format: String, animated: Bool = true) {

      let formater = DateFormatter()

      formater.dateFormat = format

      let date = formater.date(from: string) ?? Date()

      setDate(date, animated: animated)
   }
}
