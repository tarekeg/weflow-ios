//
//  OffreCollectionViewCell.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 01/10/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

class OffreCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var offreLabel: UILabel!
    @IBOutlet weak var offreImageView: UIImageView!
    @IBOutlet weak var offrePointLabel: UILabel!
    @IBOutlet weak var characterContainerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        characterContainerView.layer.cornerRadius = 11.2
        self.contentView.layer.cornerRadius = 10
        self.contentView.layer.borderWidth = 1.0

        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true

        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }

}
