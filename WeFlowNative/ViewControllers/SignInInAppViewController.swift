//
//  SignInInAppViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 03/08/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Alamofire
import AudioToolbox
import Sentry

class SignInInAppViewController: UIViewController {
    
    @IBOutlet weak var lastNameTextField: CustomTextField!
    @IBOutlet weak var firstNameTextField: CustomTextField!
    @IBOutlet weak var birthDateTextField: CustomTextField!
    @IBOutlet weak var mailTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var confirmPasswordTextField: CustomTextField!
    @IBOutlet weak var irebySignInButton: LoadingButton!
    @IBOutlet weak var companyCodeTextField: CustomTextField!
    @IBOutlet weak var irebyTcuTextView: UITextView!
    @IBOutlet weak var tcuCheckBoxImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var successDescriptionLabel: UILabel!
    @IBOutlet weak var tcuView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var passwordDescriptionLabel: UILabel!
    
    var datePicker : UIDatePicker!
    let alertService = AlertService()
    var params : Parameters?
    let RF = ReusableFunctions()
    let hyperlinks = [NSLocalizedString("tcu",comment : "") : K.Path.baseURLWeb + "/cgu",NSLocalizedString("experience-rules",comment : ""):K.Path.baseURLWeb + "/reglement-jeux"]
    let checkedImage = UIImage(named: "ic_check_box")
    let uncheckedImage = UIImage(named: "ic_check_box_outline_blank")
    var tcuIsConfirmed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpPicker()
        successDescriptionLabel.isHidden = true
        irebyTcuTextView.text = NSLocalizedString("tcu-ireby-text", comment: "")
        titleLabel.attributedText = attributedText(withString: NSLocalizedString("form-sign-in-description-text", comment: ""), boldString: NSLocalizedString("code", comment: ""), font: UIFont(name: "Avenir-Roman", size: 12.0)!)
        irebyTcuTextView.addHyperLinksToText(originalText: irebyTcuTextView.text!, hyperLinks: hyperlinks)
        navView.setShadow()
        navView.layer.shadowOffset = CGSize(width: 0, height: 2)
        navView.layer.shadowRadius = 1
        irebySignInButton.isEnabled = false
        irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        
        
    }
    func setUpPicker() {
        
        birthDateTextField.tintColor = .clear
        datePicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            
        }
        datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
        datePicker.setDate(from: "1/1/2000", format: "dd/MM/yyyy", animated: true)
        datePicker.maximumDate = Date()
        birthDateTextField.inputView = datePicker
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(self.dateChanged), for: .allEvents)
        let doneButton = UIBarButtonItem.init(title: "OK", style: .done, target: self, action: #selector(self.datePickerDone))
        doneButton.tintColor = UIColor(red: 0.21, green: 0.21, blue: 0.21, alpha: 1.00)
        let toolBar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 44))
        toolBar.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneButton], animated: true)
        birthDateTextField.inputAccessoryView = toolBar
    }
    @objc func datePickerDone() {
        birthDateTextField.resignFirstResponder()
    }
    @objc func dateChanged() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        birthDateTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    func attributedText(withString string: String, boldString: String, font: UIFont) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: string,
                                                         attributes: [NSAttributedString.Key.font: font])
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: font.pointSize)]
        let range = (string as NSString).range(of: boldString)
        attributedString.addAttributes(boldFontAttribute, range: range)
        return attributedString
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        if firstNameTextField.text?.isEmpty == true || lastNameTextField.text?.isEmpty == true {
            showAlert(title: NSLocalizedString("error-signup-firstname-title", comment: ""), body: NSLocalizedString("error-signup-firstname-body", comment: ""), ButtonTitle: NSLocalizedString("error-signup-firstname-button", comment: ""))
            return
        } else if RF.isValidEmail(mailTextField.text!) == false {
            showAlert(title: NSLocalizedString("error-signup-mail-title", comment: ""), body: NSLocalizedString("error-signup-mail-body", comment: ""), ButtonTitle: NSLocalizedString("error-signup-mail-button", comment: ""))
            return
        } else if RF.isPasswordValid(passwordTextField.text!) == false {
            showAlert(title: NSLocalizedString("error-signup-password-title", comment: ""), body: NSLocalizedString("error-signup-password-body", comment: ""), ButtonTitle: NSLocalizedString("error-signup-password-button", comment: ""))
            return
        } else if passwordTextField.text != confirmPasswordTextField.text {
            showAlert(title: NSLocalizedString("error-signup-confirmPassword-title", comment: ""), body: NSLocalizedString("error-signup-confirmPassword-body", comment: ""), ButtonTitle: NSLocalizedString("error-signup-confirmPassword-button", comment: ""))
            return
        } else {
            if irebySignInButton.title(for: .normal) == NSLocalizedString("lets_go", comment: "") {
                navigationController?.popViewController(animated: true)
            } else {
                createUserInIreby(companyCode: companyCodeTextField.text!)
            }
            
        }
    }
    @IBAction func tcuTapped(_ sender: Any) {
        if tcuIsConfirmed {
            tcuIsConfirmed = !tcuIsConfirmed
            tcuCheckBoxImageView.image = uncheckedImage
            irebySignInButton.isEnabled = false
            irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        } else {
            
            tcuIsConfirmed = !tcuIsConfirmed
            tcuCheckBoxImageView.image = checkedImage
            irebySignInButton.isEnabled = true
            irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        }
    }
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func createUserInIreby(companyCode : String) {
        var upperCode = companyCode.uppercased()
        if companyCode == "" {
            upperCode = "N64C6"
        }
        irebySignInButton.showLoading()
        irebySignInButton.isEnabled = false
        irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        params = [
            
            "firstname" : firstNameTextField.text,
            "lastname" : lastNameTextField.text,
            "company_code" : upperCode,
            "password" : passwordTextField.text,
            "email" : mailTextField.text
        ]
        let headers: HTTPHeaders = [
            "authorization": "Bearer " + K.Constant.irebyBearer,
            "token" : UserDefaults.standard.string(forKey: "ireby_access_token")!,
            "content-type": "application/json"
        ]
        
        AF.request(K.Path.baseURLWeb + "/api/v2/users/create", method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers, interceptor: nil, requestModifier: nil).responseJSON { [self]
            response in
            switch response.result {
            case .success(let value):
                if response.response?.statusCode == 201 {
                    print(value)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        lastNameTextField.isHidden = true
                        firstNameTextField.isHidden = true
                        birthDateTextField.isHidden = true
                        mailTextField.isHidden = true
                        passwordTextField.isHidden = true
                        confirmPasswordTextField.isHidden = true
                        companyCodeTextField.isHidden = true
                        tcuView.isHidden = true
                        descriptionLabel.isHidden = true
                        passwordDescriptionLabel.isHidden = true
                        successDescriptionLabel.isHidden = false
                        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
                        irebySignInButton.hideLoading()
                        irebySignInButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                        irebySignInButton.isEnabled = true
                        irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                    }
                    
                } else if response.response?.statusCode == 409 {
                    print(value)
                    showAlert(title: NSLocalizedString("popup-already-sync-title", comment: ""), body: NSLocalizedString("popup-already-sync-body", comment: ""), ButtonTitle: NSLocalizedString("popup-already-sync-button", comment: ""))
                    irebySignInButton.hideLoading()
                    irebySignInButton.setTitle(NSLocalizedString("lets_go", comment: ""), for: .normal)
                    irebySignInButton.isEnabled = true
                    irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                    
                } else {
                    showAlert(title: NSLocalizedString("error-popup-signIn-title", comment: ""), body: NSLocalizedString("error-popup-signIn-body", comment: ""), ButtonTitle: NSLocalizedString("error-popup-signIn-button", comment: ""))
                    irebySignInButton.hideLoading()
                    irebySignInButton.isEnabled = true
                    irebySignInButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
                }
            case .failure(let error):
                SentrySDK.capture(error: error)
                print(error)
            }
            
        }
        
        
    }
    
    func showAlert(title : String, body : String, ButtonTitle : String) {
        let alert = alertService.alert(title: title, body: body, buttonTitle: ButtonTitle) {
            
        }
        tabBarController!.present(alert, animated: true)
    }
    
    
}
