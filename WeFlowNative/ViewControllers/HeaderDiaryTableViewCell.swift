//
//  HeaderDiaryTableViewCell.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 18/10/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit



    class HeaderDiaryTableViewCell: UITableViewHeaderFooterView {

        @IBOutlet weak var headerLabel: UILabel!
        override func awakeFromNib() {
            super.awakeFromNib()
            self.isSkeletonable = true
        }
        
    }
