//
//  OnboardingDescriptionViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 09/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit

class OnboardingDescriptionViewController: UIViewController, UIGestureRecognizerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func presentSecondOnboardingDescription(_ sender: Any) {
        self.performSegue(withIdentifier: "toSecondOnboardingScreen", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let navVC = segue.destination as? UINavigationController
        
        let secondOnboardingVC = navVC?.viewControllers.first as! SecondOnboardingDescriptionViewController
        
        secondOnboardingVC.backButtonIsHidden = false
    }
    
}
