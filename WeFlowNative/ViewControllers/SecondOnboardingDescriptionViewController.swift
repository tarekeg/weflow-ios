//
//  SecondOnboardingDescriptionViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 09/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit

class SecondOnboardingDescriptionViewController: UIViewController {
    
    @IBOutlet weak var toLoginButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backButton: UIButton!
    
    var backButtonIsHidden = true
    
    
    let yourAttributes: [NSAttributedString.Key: Any] = [
        .font: UIFont(name: "Avenir-Heavy", size: 15)!,
        .foregroundColor: K.Color.grayThirtySix,
         .underlineStyle: NSUnderlineStyle.single.rawValue
     ]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDesign()
        if backButtonIsHidden {
            backButton.isHidden = true
        } else {
            backButton.isHidden = false
        }
    }
    
    func setDesign () {
      
        scrollView.contentSize.height = 812.0
        
        let attributeString = NSMutableAttributedString(
                string: NSLocalizedString("login", comment: ""),
                attributes: yourAttributes
             )
             toLoginButton.setAttributedTitle(attributeString, for: .normal)
    }
    
    override func viewDidLayoutSubviews() {
       
    }

    @IBAction func goBackToPreviousView(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            let vc = OnboardingDescriptionViewController()

            // 'self' refers to FirstController, but you have just dismissed
            //  FirstController! It's no longer in the view hierarchy!
            self.present(vc, animated: true, completion: nil)
        })
    }
    

}
