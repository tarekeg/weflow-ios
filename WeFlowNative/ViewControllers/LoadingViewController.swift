//
//  LoadingViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 17/06/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import SwiftyJSON
import Sentry

class LoadingViewController: UIViewController, URLSessionDelegate {
    
    @IBOutlet weak var customProgressBar: PlainHorizontalProgressBar!
    
    
    let RF = ReusableFunctions()
    var session:URLSession?
    private var observation: NSKeyValueObservation?
    var expectedContentLength : Int64 = 0
    var percentageDownloaded : Float = 0
    var buffer:NSMutableData = NSMutableData()
    var section : [String : Any] = [:]
    var sections : [[String : Any]] = []
    var tripDatas : [TripData] = []
    var formattedSections : [Section] = []
    var trips : [Trip] = []
    
    deinit {
        observation?.invalidate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let configuration = URLSessionConfiguration.default
        session = Foundation.URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        print(Date.changeDaysBy(days: -5).timeIntervalSince1970)
        NetworkManager.isReachable { [self]_ in
            percentageDownloaded = 0.0
            didStartProgress(progress: 0.1)
            getTrips()
        }
    }
    
    
    
    func didStartProgress(progress : CGFloat) {

        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            guard self.customProgressBar.progress < 1 else {
                timer.invalidate()
                return
            }
            self.customProgressBar.progress += progress
        }

    }
    func getTrips () {
        let params = [
            "user" : UserDefaults.standard.string(forKey: "Token")!,
            "key_list" : [
                "analysis/recreated_location",
                "inference/prediction"
            ],
            "start_time" : Date.changeDaysBy(days: -5).timeIntervalSince1970,
            "end_time" :Date.changeDaysBy(days: 1).timeIntervalSince1970
        ] as [String : Any]
        
        let url = URL(string: K.Path.baseURL +  "/datastreams/find_entries/timestamp")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let task = session!.dataTask(with: request as URLRequest,completionHandler:  {(data, response, error) -> Void in
            if let value = response?.expectedContentLength {
                self.expectedContentLength = value
                guard let data = data else {
                    return
                }
                
                self.buffer.append(data)
                self.percentageDownloaded = Float(self.buffer.length) / Float(self.expectedContentLength)
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String : Any]
                    let rawJson = JSON(json)
                    let rawData = rawJson["phone_data"].arrayValue
                    if !rawData.isEmpty {
                        let predictions = rawData.filter({ (json) -> Bool in
                                                            return json["metadata"]["key"].stringValue == "inference/prediction"; })
                        let recreatedLocations = rawData.filter({ (json) -> Bool in
                                                                    return json["metadata"]["key"].stringValue == "analysis/recreated_location"; })
                        let jsonRecreatedLocation = JSON(recreatedLocations)
                        let jsonPrediction = JSON(predictions)
                        DispatchQueue.main.async { [self] in
                            if let items = jsonRecreatedLocation.array {
                                for item in items {
                                    if let sectionId = item["data"]["section"]["$oid"].string {
                                        let tripData = TripData(sectionId: sectionId, speed: item["data"]["speed"].doubleValue, distance: item["data"]["distance"].doubleValue, location: Location(latitude: item["data"]["loc"]["coordinates"][1].doubleValue, longitude: item["data"]["loc"]["coordinates"][0].doubleValue))
                                        tripDatas.append(tripData)
                                    }
                                }
                            }
                            if let items = jsonPrediction.array {
                                for item in items {
                                    if let tripId = item["data"]["trip_id"]["$oid"].string {
                                        section["tripId"] = tripId
                                        section["sectionId"] = item["data"]["section_id"]["$oid"].stringValue
                                        section["end_ts"] = item["data"]["end_ts"].doubleValue
                                        section["start_ts"] = item["data"]["start_ts"].doubleValue
                                        section["mode"] = Array(item["data"]["predicted_mode_map"].dictionaryValue)[0].key
                                        sections.append(section)
                                    }
                                }
                            }
                            let groupedSection = Dictionary(grouping: tripDatas, by: { $0.sectionId })
                            for item in sections {
                                for (key,value) in groupedSection {
                                    if key == item["sectionId"] as! String {
                                        var speeds : [Double] = []
                                        var distances : [Double] = []
                                        for i in 0...value.count - 1 {
                                            speeds.append(value[i].speed)
                                            distances.append(value[i].distance)
                                        }
                                        let section = Section(tripId: item["tripId"] as! String, sectionId: item["sectionId"] as! String, mode: item["mode"] as! String, startTs: item["start_ts"] as! Double, endTs: item["end_ts"] as! Double, data: value,speeds: speeds,distances: distances)
                                        formattedSections.append(section)
                                    }
                                }
                            }
                            formattedSections.sort {
                                $0.startTs < $1.startTs
                            }
                                                        
                            let groupedTrip = Dictionary(grouping: formattedSections, by: {$0.tripId})
                            
                            for (key,value) in groupedTrip {
                                var totalDistance = 0.0
                                for i in 0...value.count - 1 {
                                    totalDistance += value[i].distances.reduce(0, +)
                                }
                                let trip = Trip(tripId: key, sections: value, totalDistance: totalDistance)
                                trips.append(trip)
                            }
                            trips.sort {
                                $0.sections[0].startTs < $1.sections[0].startTs
                            }
                        }
                        
                        
                    }
                    DispatchQueue.main.async { [self] in
                        let json = try! JSONEncoder().encode(trips)
                        UserTripData.userTrips = JSON(json)
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0, execute: {
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "tabBarController")
                        UIApplication.shared.windows.first?.rootViewController = vc
                        UIApplication.shared.windows.first?.makeKeyAndVisible()
                        self.present(vc, animated: true)
                    })
                }catch{
                    SentrySDK.capture(error: error)
                    print(error)
                    print("erroMsg")
                    
                }
                
            }
        })
        observation = task.progress.observe(\.fractionCompleted) { progress, _ in
            print("progress: ", progress.fractionCompleted)
            DispatchQueue.main.async {
                if progress.fractionCompleted == 1.0 {
                    self.customProgressBar.progress = 1.0
                }
            }
        }
        
        task.resume()
    }
}

extension Date {
  static func changeDaysBy(days : Int) -> Date {
    let currentDate = Date()
    var dateComponents = DateComponents()
    dateComponents.day = days
    return Calendar.current.date(byAdding: dateComponents, to: currentDate)!
  }
}
