//
//  AlertResetPasswordViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 27/05/2021.
//  Copyright © 2021 Transway. All rights reserved.
//

import UIKit
import Sentry

protocol AlertResetPasswordViewControllerDelegate {
    func isResetEmailSent(bool : Bool)
}

class AlertResetPasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var crossView: UIView!
    
    var delegate : AlertResetPasswordViewControllerDelegate?
    
    let RF = ReusableFunctions()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertView.layer.cornerRadius = 10.0
        crossView.layer.cornerRadius = 10.0
        
        emailTextField.addTarget(self, action: #selector(InAppLoginViewController.textFieldDidChange(_:)), for: .editingChanged)
        confirmButton.isEnabled = false
        confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        emailTextField.delegate = self
    }
    
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func confirmTapped(_ sender: Any) {
        
        resetPassword(email: emailTextField.text!) { success in
            if success {
                self.delegate?.isResetEmailSent(bool: true)
                DispatchQueue.main.async {
                    self.dismiss(animated: true)
                }
                
            } else {
                self.delegate?.isResetEmailSent(bool: false)
            }
        }
        
    }
    
    func resetPassword(email : String, completion: @escaping(_ success: Bool) -> Void)  {
        guard let clientInfo = plistValues(bundle: Bundle.main) else { return }
        
        let headers = ["content-type": "application/json"]
        let parameters = [
            "client_id": clientInfo.clientId,
            "email": email,
            "connection": "Username-Password-Authentication"
        ] as [String : Any]
        
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://" + clientInfo.domain + "/dbconnections/change_password")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch let error {
            print("error serialization", error)
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                SentrySDK.capture(error: error!)
                
            } else {
                completion(true)
            }
        })
        
        dataTask.resume()
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if RF.isValidEmail(emailTextField.text!) == true {
            confirmButton.isEnabled = true
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 1)
        } else {
            confirmButton.isEnabled = false
            confirmButton.backgroundColor = UIColor(red: 0.99, green: 0.74, blue: 0.22, alpha: 0.5)
        }
        
    }
    
}
