//
//  CalendarViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 03/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit
import FSCalendar

protocol CalendarViewControllerDelegate : AnyObject {
    func getDate(date : String)
    func getDate2(date : String)
}

class CalendarViewController: UIViewController, FSCalendarDelegate, FSCalendarDataSource, UIGestureRecognizerDelegate {

    @IBOutlet weak var fsCalendar: FSCalendar!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var inlineDatePicker: UIDatePicker!
    @IBOutlet weak var inlineDatePickerContainer: UIView!
    
    weak var delegate : CalendarViewControllerDelegate?
    var textFieldNumber : Int?
    var date : String?
    let customFont = UIFont(name: "Avenir-Medium", size: 15)
    var tap: UITapGestureRecognizer!
    let RF = ReusableFunctions()
    
    override func viewDidAppear(_ animated: Bool) {
        tap = UITapGestureRecognizer(target: self, action: #selector(onTap(sender:)))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        tap.cancelsTouchesInView = false
        tap.delegate = self
        self.view.window?.addGestureRecognizer(tap)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.layer.cornerRadius = 5.0
        view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        fsCalendar.clipsToBounds = true
        fsCalendar.layer.masksToBounds = true
        inlineDatePicker.layer.masksToBounds = true
        inlineDatePicker.clipsToBounds = true
        inlineDatePickerContainer.layer.cornerRadius = 10.0
        fsCalendar.appearance.headerTitleColor = .black
        fsCalendar.appearance.weekdayTextColor = .black
        fsCalendar.appearance.titleFont = customFont
        fsCalendar.appearance.weekdayFont = customFont
        fsCalendar.appearance.headerTitleFont = customFont
        fsCalendar.appearance.subtitleFont = customFont
        inlineDatePicker.maximumDate = Date()
        inlineDatePicker.datePickerMode = .date
        if(date == "") {
            inlineDatePicker.date = Date()
        } else {
            inlineDatePicker.date = RF.formatStringFromPopupToDate(strDate: date!)
        }
        
        if #available(iOS 14.0, *) {
            self.containerView.isHidden = true
        } else {
            self.inlineDatePickerContainer.isHidden = true
        }
        
        
    }
    
    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    internal func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if #available(iOS 14.0, *) {
            let location = touch.location(in: self.inlineDatePickerContainer)
            if self.inlineDatePickerContainer   .point(inside: location, with: nil) {
                return false
            }
            else {
                return true
            }
            
        } else {
            let location = touch.location(in: self.containerView)
            if self.containerView.point(inside: location, with: nil) {
                return false
            }
            else {
                return true
            }
        }
       
    }
    
    @IBAction func dateChanged(_ inlineDatePicker : UIDatePicker) {
        let selectedDate = inlineDatePicker.date
        print(selectedDate)
        self.dismiss(animated: true, completion: nil)
    }

    @objc private func onTap(sender: UITapGestureRecognizer) {

        self.view.window?.removeGestureRecognizer(sender)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("Date is ", date)
        if(textFieldNumber == 1) {
            delegate?.getDate(date: formatDate(date: date))
        } else if(textFieldNumber == 2){
            delegate?.getDate2(date: formatDate(date: date))
        }
        self.dismiss(animated: true, completion: nil)
    }
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func formatDate(date : Date) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd-MM-yyyy"

      let date = dateFormatterGet.date(from: "\(date)")
            return dateFormatterPrint.string(from: date!)
  
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if #available(iOS 14.0, *) {
            if(textFieldNumber == 1) {
                delegate?.getDate(date: formatDate(date: inlineDatePicker.date))
            } else if(textFieldNumber == 2){
                delegate?.getDate2(date: formatDate(date: inlineDatePicker.date))
            }
        }
    }

}
