//
//  AlertWithSecondButtonViewController.swift
//  WeFlowNative
//
//  Created by Tarek El Ghoul on 17/09/2020.
//  Copyright © 2020 Transway. All rights reserved.
//

import UIKit

class AlertWithSecondButtonViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var bodyTextView: UITextView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var crossView: UIView!
    
    let hyperlinks = [NSLocalizedString("tcu",comment : ""):K.Path.wefloWix + "mentions-légales".stringByAddingPercentEncodingForRFC3986(), NSLocalizedString("politique-confidentialité", comment: ""): K.Path.wefloWix + "politique-de-confidentialité".stringByAddingPercentEncodingForRFC3986()]

    
    var alertTitle = String()
    
    var alertBody = String()
    
    var alertActionDismissButtonTitle = String()
    
    var alertActionButtonTitle = String()
    
    var buttonAction: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        print(hyperlinks)
        bodyTextView.addHyperLinksToText(originalText: bodyTextView.text!, hyperLinks: hyperlinks)
        bodyTextView.font = UIFont(name: "Avenir-Medium", size: 12)
        bodyTextView.textColor = K.Color.graySeven
        bodyTextView.textAlignment = .left
        
    }
    

    
    func setupView () {
        crossView.layer.cornerRadius = 10.0
        
        alertView.layer.cornerRadius = 10.0
        
        titleLabel.text = alertTitle
        
        bodyTextView.text = alertBody
        
        actionButton.setTitle(alertActionButtonTitle, for: .normal)
        
        dismissButton.setTitle(alertActionDismissButtonTitle, for: .normal)
        
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        
        self.dismiss(animated: true)
        
    }
    @IBAction func actionButtonTapped(_ sender: Any) {
        dismiss(animated: true)
        
        buttonAction?()
    }
    
}

extension String {
  func stringByAddingPercentEncodingForRFC3986() -> String {
    let unreserved = "-._~/?"
    let allowed = NSMutableCharacterSet.alphanumeric()
    allowed.addCharacters(in: unreserved)
    return addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)!
  }
}
